﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Xunit;
using GameEx;
using GameEx.Controllers;
using System.Threading.Tasks;
using GameEx.Data;
using Microsoft.AspNetCore.Identity;
using GameEx.Models;
using Moq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Text.Encodings.Web;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using System.Linq;
using OpenQA.Selenium.Remote;
using Microsoft.AspNetCore.Http.Extensions;

namespace GameEx.Tests
{
    //CONTROLLER TESTS
    public class GameEx_Home
    {


        private List<User> _users = new List<User>
        {
            new User() { Id = "1", UserName = "JohnDoe" },
            new User() { Id = "2", UserName = "JohnDoeDoe" }
        };


        public Mock<UserManager<TUser>> MockUserManager<TUser>(List<TUser> ls) where TUser : class
        {
            var store = new Mock<IUserStore<TUser>>();
            var mgr = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<TUser>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

            mgr.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Callback<TUser, string>((x, y) => ls.Add(x));
            mgr.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);

            return mgr;
        }

        [Fact]
        public async Task TestGameSuggestionCreationWithCorrectData()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;

            var context = new ApplicationDbContext(options);
            Game game1 = new Game() { CategoryId = "1", Description = "prva igra", Designer = "luka aka bog svemira", Id = "1", Link = "/", Name = "vratar", PhotoUrl = "/", Publisher = "nitko", ThumbnailUrl = "/" };
            context.Game.Add(game1);
            Player player1 = new Player() { Id = "1" };
            context.Player.Add(player1);
            GameSuggestion gs1 = new GameSuggestion() { Id = "1", UserId = "1", Text = "somedesc", Title = "tit", URL = "url" };
            context.GameSuggestion.Add(gs1);
            context.SaveChanges();


            UserManager<User> _userManager = MockUserManager<User>(_users).Object;


            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));

            var gameSuggestionsController = new GameSuggestionsController(context, _userManager);
            gameSuggestionsController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            GameSuggestion gs2 = new GameSuggestion() { Text = "descript", Title = "titleeeeeee", URL = "url/url" };
            //KORISTI CREATE ZA TEST
            var actionResult = await gameSuggestionsController.Create(gs2);
            var allGameSuggestions = context.GameSuggestion.ToList();
            /*
            Debug.WriteLine(allGameSuggestions.Count());
            foreach (GameSuggestion gss in allGameSuggestions)
            {
                Debug.WriteLine(gss.Id);
            }*/
            if(allGameSuggestions.Count == 2)
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.False(false);
            }

        }
        
        [Fact]
        public async Task TestGameSuggestionDelete()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;
            var context = new ApplicationDbContext(options);
            UserManager<User> _userManager = MockUserManager<User>(_users).Object;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));


            Player player1 = new Player() { Id = "1" };
            context.Player.Add(player1);
            GameSuggestion gs1 = new GameSuggestion() { Id = "1", UserId = "1", Text = "somedesc", Title = "tit", URL = "url" };
            context.GameSuggestion.Add(gs1);
            context.SaveChanges();

            var gameSuggestionsController = new GameSuggestionsController(context, _userManager);
            gameSuggestionsController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            GameSuggestion gs2 = new GameSuggestion();

            var actionResult = await gameSuggestionsController.DeleteConfirmed("1");
            var allGameSuggestions = context.GameSuggestion.ToList();
            
            if (allGameSuggestions.Count == 0)
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.True(false);
            }
        }
        
        [Fact]
        public async Task TestGameSuggestionIndexBadPrivileges()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;
            var context = new ApplicationDbContext(options);
            UserManager<User> _userManager = MockUserManager<User>(_users).Object;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));

            User dbUser = new Models.User() { Id = "1", UserName = "JohnDoe" };

            context.User.Add(dbUser);

            GameSuggestion gs1 = new GameSuggestion() { Id = "1", User = dbUser, Text = "somedesc", Title = "tit", URL = "url", UserId = "1" };
            
            context.GameSuggestion.Add(gs1);

            context.SaveChanges();

            var gameSuggestionsController = new GameSuggestionsController(context, _userManager);
            gameSuggestionsController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };


            var actionResult = await gameSuggestionsController.Details("1");
            
            if(typeof(cloudscribe.Pagination.Models.PagedResult<GameSuggestion>) == actionResult.GetType())
            {
                context.Database.EnsureDeleted();
                Assert.True(false);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
        }
        [Fact]
        public async Task TestGameAddToCollection()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;
            var context = new ApplicationDbContext(options);
            UserManager<User> _userManager = MockUserManager<User>(_users).Object;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));


            User dbUser = new Models.User() { Id = "1", UserName = "JohnDoe" };
            Player player1 = new Player() { Id = "1", User = dbUser };
            context.User.Add(dbUser);
            context.Player.Add(player1);

            context.SaveChanges();

            var playerGamesController = new PlayerGamesController(context, _userManager);
            playerGamesController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            var actionResult = playerGamesController.Create("1", new PlayerGame() { GameId = "1" });

            var allGameThisPlayerGames = context.PlayerGame.Where(p => p.PlayerId == "1").ToList();

            if (allGameThisPlayerGames.Count == 1)
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.True(false);
            }
        }
        [Fact]
        public async Task TestGameRemoveFromCollection()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;
            var context = new ApplicationDbContext(options);
            UserManager<User> _userManager = MockUserManager<User>(_users).Object;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));


            User dbUser = new Models.User() { Id = "1", UserName = "JohnDoe" };
            Player player1 = new Player() { Id = "1", User = dbUser };
            context.User.Add(dbUser);
            context.Player.Add(player1);

            context.SaveChanges();

            var playerGamesController = new PlayerGamesController(context, _userManager);
            playerGamesController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            PlayerGame pg = new PlayerGame() { PlayerId = "1", GameId = "1" };
            context.PlayerGame.Add(pg);
            context.SaveChanges();
            //Debug.WriteLine(context.PlayerGame.Where(p => p.PlayerId == "1").ToList().Count());

            var actionResult = playerGamesController.DeleteConfirmed("1");
            var allGameThisPlayerGames = context.PlayerGame.Where(p => p.PlayerId == "1").ToList();

            if (allGameThisPlayerGames.Count == 0)
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.True(false);
            }
        }
        [Fact]
        public async Task TestAddToInventory()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GameEx")
                .Options;
            var context = new ApplicationDbContext(options);
            UserManager<User> _userManager = MockUserManager<User>(_users).Object;
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, "JohnDoe"),
                new Claim(ClaimTypes.NameIdentifier, "1"),
                new Claim("custom-claim", "example claim value"),
            }, "mock"));


            User dbUser = new Models.User() { Id = "1", UserName = "JohnDoe" };
            Renter renter1 = new Renter() { Id = "1", User = dbUser };
            context.User.Add(dbUser);
            context.Renter.Add(renter1);
            Game game1 = new Game() { CategoryId = "1", Description = "prva igra", Designer = "luka aka bog svemira", Id = "1", Link = "/", Name = "vratar", PhotoUrl = "/", Publisher = "nitko", ThumbnailUrl = "/" };
            context.Game.Add(game1);
            context.SaveChanges();

            var inventoryItemsController = new InventoryItemsController(context, _userManager);
            inventoryItemsController.ControllerContext = new Microsoft.AspNetCore.Mvc.ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };


            //Debug.WriteLine(context.PlayerGame.Where(p => p.PlayerId == "1").ToList().Count());

            var actionResult = inventoryItemsController.Create(new InventoryItem() { GameId = "1", Quantity = 10, Price = 5 }, "vratar");
            var allInventoryItems = context.InventoryItem.Where(p => p.RenterId == "1").ToList();

            if (allInventoryItems.Count == 1)
            {
                context.Database.EnsureDeleted();
                Assert.True(true);
            }
            else
            {
                context.Database.EnsureDeleted();
                Assert.True(false);
            }
        }
    }



    //DRIVER TESTS
    public class SeleniumTests : IDisposable
    {

        private readonly IWebDriver _driver;
        public SeleniumTests()
        {

            FirefoxOptions fo = new FirefoxOptions() { AcceptInsecureCertificates = true };
            _driver = new FirefoxDriver(fo);
        }
        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }

        //Player
        //Player1234
        //Renter
        //Renter1234
        //Moderator
        //Moderator1234
        //Admin
        //Admin1234

        private readonly string LoginURL = "https://gameex20210111220251.azurewebsites.net/Identity/Account/Login";
        private readonly string BaseURL = "https://gameex20210111220251.azurewebsites.net";

        [Fact]
        public void TestLoginDriver()
        {

            _driver.Navigate().GoToUrl(LoginURL);

            //ovdje staviti korisnika BILO KOJEG
            _driver.FindElement(By.Id("Input_Username")).SendKeys("Player");
            _driver.FindElement(By.Id("Input_Password")).SendKeys("Player1234");

            _driver.FindElement(By.XPath("//button[@class='btn btn-primary'][.='Log in']")).Click();
            //Debug.WriteLine(_driver.Url);

            if (_driver.Url == BaseURL + "/")
            {
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }
        
        [Fact]
        public void TestCreateGroupTab()
        {

            _driver.Navigate().GoToUrl(LoginURL);

            //ovdje staviti korisnika tipa samo PLAYER
            _driver.FindElement(By.Id("Input_Username")).SendKeys("Player");
            _driver.FindElement(By.Id("Input_Password")).SendKeys("Player1234");

            _driver.FindElement(By.XPath("//button[@class='btn btn-primary'][.='Log in']")).Click();

            _driver.FindElement(By.XPath("//a[@class='btn btn-success m-3'][contains(@href,\"/Groups/Create\")][.='Create a group']")).Click();

            if (_driver.Url == BaseURL + "/Groups/Create")
            {
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }

        [Fact]
        public void TestTestSearch()
        {
            _driver.Navigate().GoToUrl(LoginURL);

            //ovdje staviti korisnika tipa samo PLAYER
            _driver.FindElement(By.Id("Input_Username")).SendKeys("Player");
            _driver.FindElement(By.Id("Input_Password")).SendKeys("Player1234");

            _driver.FindElement(By.XPath("//button[@class='btn btn-primary'][.='Log in']")).Click();

            _driver.Navigate().GoToUrl(BaseURL + "/Game?query=13");

            if(_driver.FindElement(By.XPath("//a[contains(@href,\"/Game/r6Gno8lbpk\")]")) != null)
            {
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }

        [Fact]
        public void TestAddGameCollectionList()
        {
            _driver.Navigate().GoToUrl(LoginURL);

            //ovdje staviti korisnika tipa samo RENTER
            _driver.FindElement(By.Id("Input_Username")).SendKeys("Player");
            _driver.FindElement(By.Id("Input_Password")).SendKeys("Player1234");

            _driver.FindElement(By.XPath("//button[@class='btn btn-primary'][.='Log in']")).Click();

            _driver.Navigate().GoToUrl(BaseURL + "/PlayerGames/Create");

            _driver.FindElement(By.XPath("//input[@class='text-success fa fa-plus']")).Click();

            _driver.Navigate().GoToUrl(BaseURL + "/PlayerGames/Delete/r6Gno8lbpk");

            if (_driver.FindElement(By.XPath("//input[@class='btn btn-danger']")) != null)
            {
                _driver.FindElement(By.XPath("//input[@class='btn btn-danger']")).Click();
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }


        [Fact]
        public void TestAddGameToInventory()
        {
            _driver.Navigate().GoToUrl(LoginURL);

            //ovdje staviti korisnika tipa samo RENTER
            _driver.FindElement(By.Id("Input_Username")).SendKeys("Renter");
            _driver.FindElement(By.Id("Input_Password")).SendKeys("Renter1234");

            _driver.FindElement(By.XPath("//button[@class='btn btn-primary'][.='Log in']")).Click();

            _driver.Navigate().GoToUrl(BaseURL + "/InventoryItems/Create");

            _driver.FindElement(By.Id("GameAutocomplete")).SendKeys("Star Trek: Catan");
            _driver.FindElement(By.Id("Quantity")).SendKeys("10");
            _driver.FindElement(By.Id("Price")).SendKeys("20");

            _driver.FindElement(By.XPath("//input[@class='btn btn-success']")).Click();

            _driver.Navigate().GoToUrl(BaseURL + "/InventoryItems/Delete/l2i97dfuvD");

            if (_driver.FindElement(By.XPath("//input[@class='btn btn-danger']")) != null)
            {
                _driver.FindElement(By.XPath("//input[@class='btn btn-danger']")).Click();
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }
    }

}
