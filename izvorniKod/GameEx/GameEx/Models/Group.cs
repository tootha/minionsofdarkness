﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GameEx.Models
{
    public class Group
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Please enter a group name.")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter a group description.")]
        public string Description { get; set; }
        public double? LocationXCoordinate { get; set; }
        public double? LocationYCoordinate { get; set; }
        public double? LocationRadius { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? TimeOfPlaying { get; set; }
        [Required(ErrorMessage = "Please enter a capacity of your group")]
        [Range(1,1000)]
        public int Capacity { get; set; }

        public string GameId { get; set; }
        public Game Game { get; set; }

        public List<GroupInvitation> GroupInvitations { get; set; }
        public List<GroupMember> GroupMembers { get; set; }
        public List<Review> Reviews { get; set; }
        public List<GroupMessage> GroupMessages { get; set; }

        public List<PlayerPlayingDates> PlayingDates { get; set; }
        public List<GroupJoinRequest> GroupJoinRequests { get; set; }
        public GroupAdvert GroupAdvert { get; set; }
        public Group()
        {
            GroupInvitations = new List<GroupInvitation>();
        }
    }
}
