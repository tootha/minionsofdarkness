﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Player
    {
        public string Id { get; set; }

        public User User { get; set; }

        public List<Exchange> InitiatedExchanges { get; set; }
        public List<Exchange> TargetedExchanges { get; set; }
        public List<Rental> Rentals { get; set; }
        public List<PlayerGame> PlayerGames { get; set; }
        public List<GroupInvitation> GroupInvitations { get; set; }
        public List<GroupMessage> GroupMessages { get; set; }
        [JsonIgnore]
        public List<GroupMember> GroupMemberships { get; set; }
        public List<Review> ReceivedReviews { get; set; }
        public List<Review> WrittenReviews { get; set; }
        public List<PlayerPlayingDates> PlayingDates { get; set; }
        public List<GroupJoinRequest> GroupJoinRequests { get; set; }
    }
}
