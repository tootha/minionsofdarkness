﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Renter
    {
        public string Id { get; set; }
        [Display(Name = "Company Name")]
        [Required(ErrorMessage = "This field is required.")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }
        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "This field is required.")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Web address")]
        [Required(ErrorMessage = "This field is required.")]
        public string WebAddress { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [RegularExpression("[0-9]{11}", ErrorMessage = "Value can only contain numbers 0-9 and must be 11 characters long.")]
        public string OIB { get; set; }
        public bool ApprovedByAdmin { get; set; }

        public User User { get; set; }
        public List<InventoryItem> InventoryItems { get; set; }
    }
}
