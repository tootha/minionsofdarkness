﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class User:IdentityUser
    {
        public byte[] ProfileImage { get; set; }
        public Player Player { get; set; }
        public Renter Renter { get; set; }

        public List<Message> ReceivedMessages { get; set; }
        public List<Message> SentMessages { get; set; }
        public List<GameSuggestion> GameSuggestions { get; set; }
    }
}
