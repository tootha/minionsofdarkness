﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class PlayerPlayingDates
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public Group Group { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public GroupMember GroupMember { get; set; }
    }
}
