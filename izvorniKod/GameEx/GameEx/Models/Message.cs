﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Message
    {
        public string Id { get; set; }
        public User Sender { get; set; }
        [Required]
        public string SenderUserName { get; set; }

        public User Receiver { get; set; }
        [Required]
        public string ReceiverUserName { get; set; }

        [Required]
        public string Text { get; set; }
        public DateTime When { get; set; }

        public Message()
        {
            When = DateTime.Now;
        }
    }
}
