﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GameCategory
    {
        public string Id { get; set; }
        public string Name { get; set; }

        //za n:n vezu sa GameCategory se koristi GameCategoryGame kao sredisnja tablica koja
        //povezuje te 2 tablice
        public List<GameCategoryGame> GameCategoryGames { get; set; }
    }
}
