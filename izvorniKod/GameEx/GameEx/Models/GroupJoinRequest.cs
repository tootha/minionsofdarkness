﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GroupJoinRequest
    {
        public string Id { get; set; }
        public Player Player { get; set; }
        public string PlayerId { get; set; }
        public Group Group { get; set; }
        public string GroupId { get; set; }

    }
}
