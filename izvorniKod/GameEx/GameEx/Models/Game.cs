﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Game
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [MinLength(5)]
        public string PhotoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string CategoryId { get; set; }
        [Required]
        [MinLength(10)]
        public string Description { get; set; }
        [Required]
        [MinLength(5)]
        public string Link { get; set; }
        [Range(0.0,5.0)]
        public double? Rating { get; set; }
        [Range(1,100)]
        public int? MinPlayers { get; set; }
        [Range(1, 100)]
        public int? MaxPlayers { get; set; }
        [Range(0, 3600)]
        public int? MinPlayingTime { get; set; }
        [Range(1, 3600)]
        public int? MaxPlayingTime { get; set; }
        public string Designer { get; set; }
        public string Publisher { get; set; }
        [Range(1900,3000)]
        public int? Year { get; set; }
        [Range(0,100)]
        public int? MinAge { get; set; }

        //za n:n vezu sa GameCategory se koristi GameCategoryGame kao sredisnja tablica koja
        //povezuje te 2 tablice
        public List<GameCategoryGame> GameCategoryGames { get; set; }
        public List<Group> Groups { get; set; }
        public List<PlayerGame> PlayerGames { get; set; }
        public List<Exchange> OfferedGames { get; set; }
        public List<Exchange> WantedGames { get; set; }
        public List<InventoryItem> InventoryItems { get; set; }
    }
}
