﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GameCategoryGame
    {
        public string GameId { get; set; }
        public Game Game { get; set; }
        public string CategoryId { get; set; }
        public GameCategory GameCategory { get; set; }
    }
}
