﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Review
    {
        public string GroupId { get; set; }
        public Group Group { get; set; }
        public string ReviewerId { get; set; }
        public Player Reviewer { get; set; }
        public string ReviewedPlayerId { get; set; }
        public Player ReviewedPlayer { get; set; }
        public DateTime TimeWritten { get; set; }
        [Required]
        [Range(0,5)]
        public int Rating { get; set; }
        [Required]
        public string Text { get; set; }
       
    }
}
