﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GroupMessage
    {
        public string Id { get; set; }
        public string SenderId { get; set; }
        public string SenderUsername { get; set; }
        public Player Sender { get; set; }
        public string GroupId { get; set; }
        public Group Group { get; set; }
        public string Text { get; set; }
        public DateTime When { get; set; }
        public GroupMessage()
        {
            When = DateTime.Now;
        }
    }
}
