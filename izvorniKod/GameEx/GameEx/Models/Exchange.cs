﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Exchange
    {
        public string InitiatorId { get; set; }
        public Player Initiator { get; set; }
        public string TargetPlayerId { get; set; }
        public Player TargetPlayer { get; set; }
        public string WantedGameId { get; set; }
        public Game WantedGame { get; set; }
        public string OfferedGameId { get; set; }
        public Game OfferedGame { get; set; }

        
        public DateTime Created { get; set; }
        public string ExchangeStatusId { get; set; }
        public ExchangeStatus ExchangeStatus { get; set; }
    }
}
