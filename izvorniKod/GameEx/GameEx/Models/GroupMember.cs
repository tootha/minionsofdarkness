﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GroupMember
    {
        public string GroupId { get; set; }
        public Group Group { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
        public List<PlayerPlayingDates> PlayerPlayingDates { get; set; }

        public bool Owner { get; set; }
    }
}
