﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GroupInvitation
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public Group Group { get; set; }
        public string InvitedPlayerId { get; set; }
        public Player InvitedPlayer { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
