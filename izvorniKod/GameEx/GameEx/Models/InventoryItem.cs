﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class InventoryItem
    {
        public string GameId { get; set; }
        public Game Game { get; set; }
        public string RenterId { get; set; }
        public Renter Renter { get; set; }
        [Required]
        [Range(0,10000)]
        public int Quantity { get; set; }
        [Required]
        [Range(0,1000000)]
        public decimal Price { get; set; }
        public int QuantityReserved { get; set; }
        public List<Rental> Rentals { get; set; }
    }
}
