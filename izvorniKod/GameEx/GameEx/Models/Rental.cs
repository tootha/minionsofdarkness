﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class Rental
    {
        public string Id { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
        public string RenterId { get; set; }
        public string GameId { get; set; }
        public InventoryItem InventoryItem { get; set; }
        public bool IsReturned { get; set; }
        public DateTime DateOfRent { get; set; }
        public DateTime? DateOfReturn { get; set; }
    }
}
