﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    [NotMapped]
    public class CheckboxModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool isChecked { get; set; }
    }
}
