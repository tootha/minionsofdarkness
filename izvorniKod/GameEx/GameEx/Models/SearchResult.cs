﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    [NotMapped]
    public class SearchResult
    {
        public string Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public byte[] Photo { get; set; }
        public string PhotoUrl { get; set; }

    }
}
