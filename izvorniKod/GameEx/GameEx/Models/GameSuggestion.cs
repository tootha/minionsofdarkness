﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class GameSuggestion
    {
        public string Id { get; set; }
        
        public string UserId { get; set; }
        public User User { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Text { get; set; }
        public string URL { get; set; }
    }
}
