﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Models
{
    public class PlayerGame
    {
        public string GameId { get; set; }
        public Game Game { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
        public bool WillingToTrade { get; set; }
    }
}
