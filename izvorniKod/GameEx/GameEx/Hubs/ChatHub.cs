﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Hubs
{
    public class ChatHub:Hub
    {
        private readonly ApplicationDbContext _context;
        public ChatHub(ApplicationDbContext context)
        {
            _context = context;
        }
        public override Task OnConnectedAsync()
        {
            // Retrieve user.
            var user = _context.Users
                .SingleOrDefault(u => u.UserName == Context.User.Identity.Name);

            var groups = _context.GroupMember.Where(g => g.PlayerId == user.Id).ToList();

            // Add to each assigned group.
            foreach (var item in groups)
            {
                //Groups.Add(Context.ConnectionId, item.GroupId);
                Groups.AddToGroupAsync(Context.ConnectionId, item.GroupId);
            }
            
            return base.OnConnectedAsync();
        }

        public async Task SendMessage(Message message)
        {
            var receiver = message.ReceiverUserName;
            var sender = message.SenderUserName;
            var receiverId = _context.User.Where(u => u.UserName == receiver).Select(u => u.Id).FirstOrDefault();
            var senderId = _context.User.Where(u => u.UserName == sender).Select(u => u.Id).FirstOrDefault();

            await Clients.Users(receiverId,senderId).SendAsync("receiveMessage", message);
        }

        public async Task SendGroupMessage(GroupMessage message)
        {
            await Clients.Group(message.GroupId).SendAsync("recieveGroupMessage", message);
        }
    }
}
