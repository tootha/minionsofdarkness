﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.ViewComponents
{
    public class MessagesViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public MessagesViewComponent(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var groupMemberships = _context.GroupMember.Include(g => g.Group).Where(g => g.PlayerId == _userManager.GetUserId((System.Security.Claims.ClaimsPrincipal)User)).Select(g => new { Name = g.Group.Name, Id = g.GroupId }).Distinct().ToList();
            var messagesVc = new List<MessageVC>();
            var groupMessagesVc = new List<GroupMessageVC>();

            foreach (var membership in groupMemberships)
            {
                var groupMessage = new GroupMessageVC
                {
                    GroupId = membership.Id,
                    GroupName = membership.Name
                };

                var recentMessage = _context.GroupMessage.Where(m => m.GroupId == membership.Id).OrderByDescending(m => m.When).Select(m => new { m.Text, m.When }).FirstOrDefault();
                //odaberi najnoviju poruku
                if (recentMessage != null)
                {
                    groupMessage.Text = recentMessage.Text;
                    groupMessage.When = recentMessage.When;
                    groupMessagesVc.Add(groupMessage);
                }
            }

            var messagedUsers = _context.Message.Include(m => m.Receiver).Where(m => m.SenderUserName == User.Identity.Name)
                .Select(m => new { Name = m.ReceiverUserName, Id = m.Receiver.Id }).ToList();
            var messagedByUsers = _context.Message.Include(m => m.Sender).Where(m => m.ReceiverUserName == User.Identity.Name)
                .Select(m => new { Name = m.SenderUserName, Id = m.Sender.Id }).ToList();

            var userSet = messagedUsers;
            userSet.AddRange(messagedByUsers);
            userSet = userSet.Distinct().ToList();

            foreach (var user in userSet)
            {
                var userMessage = new MessageVC
                {
                    UserName = user.Name,
                    Id = user.Id
                };
                var recentMessage = _context.Message.Include(m => m.Sender)
                    .Where(m => m.SenderUserName == user.Name && m.ReceiverUserName == User.Identity.Name
                    || m.ReceiverUserName == user.Name && m.SenderUserName == User.Identity.Name)
                    .Select(m => new { Text = m.Text, When = m.When, Photo = m.Sender.ProfileImage }).OrderByDescending(m => m.When).FirstOrDefault();
                userMessage.Text = recentMessage.Text;
                userMessage.When = recentMessage.When;
                userMessage.Photo = recentMessage.Photo;
                messagesVc.Add(userMessage);
            }
            ViewData["GroupMessages"] = groupMessagesVc.OrderByDescending(g => g.When).ToList();
            ViewData["UserMessages"] = messagesVc.OrderByDescending(g => g.When).ToList();
            return View();
        }


    }

    public class MessageVC
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public DateTime When { get; set; }
        public string UserName { get; set; }
        public byte[] Photo { get; set; }
    }

    public class GroupMessageVC
    {
        public string Text { get; set; }
        public DateTime When { get; set; }
        public string GroupName { get; set; }
        public string GroupId { get; set; }

    }
}
