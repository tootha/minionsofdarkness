﻿using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.ViewComponents
{
    public class ModeratorMessagesViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public ModeratorMessagesViewComponent(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var groupMemberships = _context.GroupMember.Include(g => g.Group).Select(g => new { Name = g.Group.Name, Id = g.GroupId }).Distinct().ToList();
            var messagesVc = new List<MessageModeratorVC>();
            var groupMessagesVc = new List<GroupMessageVC>();

            foreach (var membership in groupMemberships)
            {
                var groupMessage = new GroupMessageVC
                {
                    GroupId = membership.Id,
                    GroupName = membership.Name
                };

                var recentMessage = _context.GroupMessage.Where(m => m.GroupId == membership.Id).OrderByDescending(m => m.When).Select(m => new { m.Text, m.When }).FirstOrDefault();
                //odaberi najnoviju poruku
                if (recentMessage != null)
                {
                    groupMessage.Text = recentMessage.Text;
                    groupMessage.When = recentMessage.When;
                    groupMessagesVc.Add(groupMessage);
                }
            }

            var messagedUsers = _context.Message.Include(m => m.Receiver).Include(m => m.Sender)
                .Select(m => new { SenderUserName = m.SenderUserName, ReceiverUserName = m.ReceiverUserName, SenderId = m.Sender.Id, ReceiverId = m.Receiver.Id }).ToList();



            var userSet = messagedUsers;
            userSet = userSet.Distinct().ToList();
            for(var i = 0; i < userSet.Count(); i++)
            {
                var check = userSet.Where(m => m.ReceiverUserName == userSet[i].SenderUserName && m.SenderUserName == userSet[i].ReceiverUserName).ToList();
                if (check.Count() != 0)
                {
                    userSet.Remove(userSet[i]);
                }
            }
            var userSetModified = userSet;
            foreach (var user in userSetModified)
            {
                var userMessage = new MessageModeratorVC
                {
                    SenderUserName = user.SenderUserName,
                    SenderId = user.SenderId,
                    ReceiverUserName = user.ReceiverUserName,
                    ReceiverId = user.ReceiverId
                };
                var recentMessage = _context.Message.Include(m => m.Sender)
                    .Where(m => m.SenderUserName == user.SenderUserName && m.ReceiverUserName == user.ReceiverUserName)
                    .Select(m => new { Text = m.Text, When = m.When, SenederPhoto = m.Sender.ProfileImage, ReceiverPhoto = m.Receiver.ProfileImage })
                    .OrderByDescending(m => m.When).FirstOrDefault();
                userMessage.Text = recentMessage.Text;
                userMessage.When = recentMessage.When;
                userMessage.SenderPhoto = recentMessage.SenederPhoto;
                userMessage.ReceiverPhoto = recentMessage.ReceiverPhoto;
                messagesVc.Add(userMessage);
            }
            ViewData["GroupMessages"] = groupMessagesVc.OrderByDescending(g => g.When).ToList();
            ViewData["UserMessages"] = messagesVc.OrderByDescending(g => g.When).ToList();
            return View();
        }
    }

    public class MessageModeratorVC
    {
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public string Text { get; set; }
        public DateTime When { get; set; }
        public string SenderUserName { get; set; }
        public string ReceiverUserName { get; set; }
        public byte[] SenderPhoto { get; set; }
        public byte[] ReceiverPhoto { get; set; }
    }
}
