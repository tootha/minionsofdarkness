﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace GameEx.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly ApplicationDbContext _context;

        public RegisterModel(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _context = context;
        }

        [BindProperty]
        public PlayerRegistration Input { get; set; }
        [BindProperty]
        public RenterRegistration AdditionalInput { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class PlayerRegistration
        {
            [Required]
            [Display(Name = "Profile picture")]
            public IFormFile ProfilePicture { get; set; }

            [Required]
            [StringLength(20, MinimumLength = 5)]
            [Display(Name = "Username")]
            public string UserName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public class RenterRegistration
        {
            [Required]
            [StringLength(30, MinimumLength = 3)]
            [Display(Name = "Company name")]
            public string CompanyName { get; set; }

            [Required]
            [StringLength(30, MinimumLength = 3)]
            [Display(Name = "Address")]
            public string Address { get; set; }

            [Required]
            [StringLength(30, MinimumLength = 3)]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [Required]
            [StringLength(40, MinimumLength = 3)]
            [Display(Name = "Web adress")]
            public string WebAddress { get; set; }

            [Required]
            [StringLength(11, MinimumLength = 11)]
            [Display(Name = "OIB")]
            public string OIB { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            ViewData["SubmittedForm"] = "none";
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null, string userType = null)
        {
            ModelState.Clear();
            if (userType == "player")
            {
                TryValidateModel(Input, nameof(Input));
            } else if (userType == "renter")
            {
                TryValidateModel(Input, nameof(Input));
                TryValidateModel(AdditionalInput, nameof(AdditionalInput));
            }
            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            if (ModelState.IsValid)
            {
                byte[] fileBytes = null;
                if (Input.ProfilePicture.Length > 0)
                {
                    var ms = new MemoryStream();
                    Input.ProfilePicture.CopyTo(ms);
                    fileBytes = ms.ToArray();
                    //string s = Convert.ToBase64String(fileBytes);
                    // act on the Base64 data

                }

                var user = new User { UserName = Input.UserName, Email = Input.Email, ProfileImage = fileBytes };
                var result = await _userManager.CreateAsync(user, Input.Password);

                if (result.Succeeded)
                {

                    if (userType == "player")
                    {
                        var igrac = new Player { Id = user.Id };
                        await _userManager.AddToRoleAsync(user, "Player");
                        _context.Player.Add(igrac);
                        _context.SaveChanges();
                    }
                    else if (userType == "renter")
                    {

                        var iznajmljivac = new Renter
                        {
                            Id = user.Id,
                            Address = AdditionalInput.Address,
                            CompanyName = AdditionalInput.CompanyName,
                            OIB = AdditionalInput.OIB,
                            PhoneNumber = AdditionalInput.PhoneNumber,
                            WebAddress = AdditionalInput.WebAddress
                        };
                        await _userManager.AddToRoleAsync(user, "Renter");
                        _context.Renter.Add(iznajmljivac);
                        _context.SaveChanges();
                    }
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            if(userType == "player")
            {
                ViewData["SubmittedForm"] = "player";
            }else if(userType == "renter")
            {
                ViewData["SubmittedForm"] = "renter";
            }
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
