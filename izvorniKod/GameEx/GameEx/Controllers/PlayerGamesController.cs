﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;

namespace GameEx.Controllers
{
    [Authorize(Roles = "Player,Moderator,Administrator")]
    public class PlayerGamesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public PlayerGamesController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: PlayerGames
        public async Task<IActionResult> IndexAsync(string id, int page_number = 1, int page_size = 30)
        {
            
            string userId;
            try
            {
                userId = _userManager.GetUserId(User);
            }
            catch
            {
                return NotFound();
            }
            try
            {
                var appctx = _context.Player.Where(p => p.Id == userId).ToList();
                if(appctx.Count() != 1)
                {
                    return NotFound();
                }
            }
            catch
            {

            }
            var skipNum = (page_number - 1) * page_size;
            var applicationDbContext = _context.PlayerGame.Include(p => p.Game).Include(p => p.Player).Where(p => p.PlayerId == id)
                .Include(pg => pg.Player).ThenInclude(p => p.User).OrderBy(p => p.Game.Name)
                .Skip(skipNum).Take(page_size);

            var result = new PagedResult<PlayerGame>
            {
                Data = applicationDbContext.AsNoTracking().ToList(),
                TotalItems = _context.PlayerGame.Count(),
                PageNumber = page_number,
                PageSize = page_size
            };
            ViewData["User"] = await _userManager.FindByIdAsync(id) ;
            return View(result);
        }

        
        public async Task<IActionResult> ToggleTrade(string id)
        {
            var playerId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (id == null)
            {
                return NotFound();
            }
            
            try
            {
                var playergame = await _context.PlayerGame.FindAsync(id, playerId);
                if (playergame == null)
                {
                    return NotFound();
                }
                
                playergame.WillingToTrade = playergame.WillingToTrade == true ? false : true;
                _context.Update(playergame);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "PlayerGames", new { id = id });
            }
            catch
            {
                throw;
            }
        }

        // GET: PlayerGames/Create
        public IActionResult Create(string id, int page_number = 1, int page_size = 20)
        {
            var skipNum = (page_number - 1) * page_size;
            var playerrId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var playerrgames = _context.PlayerGame.Where(p => p.PlayerId == playerrId).Select(p =>p.GameId).ToList();
            var applicationDbContext = _context.Game
                .Include(g => g.GameCategoryGames).ThenInclude(g => g.GameCategory)
                .Include(g => g.PlayerGames).Where(g => !playerrgames.Contains(g.Id))
                .OrderBy(g => g.Name)
                .Skip(skipNum).Take(page_size);

            var result = new PagedResult<Game>
            {
                Data = applicationDbContext.AsNoTracking().ToList(),
                TotalItems = _context.Game.Count() - _context.PlayerGame.Where(p=>p.PlayerId == id).Count(),
                PageNumber = page_number,
                PageSize = page_size
        };

            ViewData["WillingToTrade"] = false;
            ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id");
            //ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id");
            return View(result);
        }

        // POST: PlayerGames/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string id, [Bind("GameId")] PlayerGame playerGame)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound();
            }
            try
            {
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (appctx.Count() != 1)
                {
                    return NotFound();
                }
            }
            catch
            {

            }
            playerGame.PlayerId = userId;
            playerGame.GameId = id;
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Add(playerGame);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index","PlayerGames",new { id = userId});
                }
                catch
                {
                    return NotFound();
                }
            }
            ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id", playerGame.GameId);
            ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id", playerGame.PlayerId);
            return View(playerGame);
        }

        // GET: PlayerGames/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound();
            }
            try
            {
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (appctx.Count() != 1)
                {
                    return NotFound();
                }
            }
            catch
            {

            }

            var playerGame = await _context.PlayerGame.FindAsync(id, userId);
            var playerrrGame = await _context.PlayerGame
                .Include(i => i.Game)
                .FirstOrDefaultAsync(i => i.GameId == id && i.PlayerId == userId);

            if (playerGame == null)
            {
                return NotFound();
            }

            return View(playerrrGame);
        }

        // POST: PlayerGames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound();
            }
            try
            {
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (appctx.Count() != 1)
                {
                    return NotFound();
                }
            }
            catch
            {
                return NotFound();
            }

            var playerGame = await _context.PlayerGame.FindAsync(id, userId);
            _context.PlayerGame.Remove(playerGame);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(IndexAsync));
        }

        private bool PlayerGameExists(string id)
        {
            return _context.PlayerGame.Any(e => e.GameId == id);
        }
    }
}
