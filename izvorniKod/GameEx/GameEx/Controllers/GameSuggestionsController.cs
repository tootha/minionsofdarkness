﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace GameEx.Controllers
{
    
    public class GameSuggestionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public GameSuggestionsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: GameSuggestions
        [Authorize(Roles="Moderator,Administrator")]
        public IActionResult Index(int page_number = 1, int page_size = 30)
        {
            var skipNum = (page_number - 1) * page_size;
            var applicationDbContext = _context.GameSuggestion
                .Include(g => g.User)
                .Skip(skipNum).Take(page_size);
            var result = new PagedResult<GameSuggestion>
            {
                Data = applicationDbContext.AsNoTracking().ToList(),
                TotalItems = _context.GameSuggestion.Count(),
                PageNumber = page_number,
                PageSize = page_size
            };
            
            return View(result);
        }

        [Authorize]
        // GET: GameSuggestions/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameSuggestion = await _context.GameSuggestion
                .Include(g => g.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gameSuggestion == null)
            {
                return NotFound();
            }
            var isAdmin = await _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Administrator");
            var isModerator = await _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User), "Moderator");
            if (User.Identity.Name == gameSuggestion.User.UserName
                || isAdmin
                || isModerator)
            {
                return View(gameSuggestion);
            }
            else
            {
                return NotFound();
            }

            
        }

        // GET: GameSuggestions/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: GameSuggestions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Text,URL")] GameSuggestion gameSuggestion)
        {
            if (ModelState.IsValid)
            {
                gameSuggestion.Id = Guid.NewGuid().ToString();
                gameSuggestion.UserId = _userManager.GetUserId(User);
                _context.Add(gameSuggestion);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Home");
            }
            return View(gameSuggestion);
        }

        // GET: GameSuggestions/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameSuggestion = await _context.GameSuggestion.FindAsync(id);
            if (gameSuggestion == null)
            {
                return NotFound();
            }
            ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id", gameSuggestion.UserId);
            return View(gameSuggestion);
        }

        // POST: GameSuggestions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,PlayerId,Title,Text, URL")] GameSuggestion gameSuggestion)
        {
            if (id != gameSuggestion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gameSuggestion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameSuggestionExists(gameSuggestion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id", gameSuggestion.UserId);
            return View(gameSuggestion);
        }

        // GET: GameSuggestions/Delete/5
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameSuggestion = await _context.GameSuggestion
                .Include(g => g.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gameSuggestion == null)
            {
                return NotFound();
            }

            return View(gameSuggestion);
        }

        // POST: GameSuggestions/Delete/5
        [Authorize(Roles ="Moderator,Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var gameSuggestion = await _context.GameSuggestion.FindAsync(id);
            _context.GameSuggestion.Remove(gameSuggestion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GameSuggestionExists(string id)
        {
            return _context.GameSuggestion.Any(e => e.Id == id);
        }

        [Authorize(Roles ="Administrator,Moderator")]
        public IActionResult Approve()
        {
            return RedirectToAction("Create", "Game");
        }
    }
}
