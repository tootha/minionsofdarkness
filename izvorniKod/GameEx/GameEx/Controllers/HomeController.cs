﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GameEx.Models;
using GameEx.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace GameEx.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public HomeController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        [Route("Welcome")]
        public async Task<IActionResult> Welcome()
        {
            var topGames = await _context.Game.Include(g => g.GameCategoryGames)
                .ThenInclude(g => g.GameCategory).OrderByDescending(g => g.Rating).ToListAsync();
            ViewData["TopGames"] = topGames.Take(10);

            //if loged in
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var groupInvitations = await _context.GroupInvitation.Include(g => g.Group)
                .Include(p => p.InvitedPlayer).ThenInclude(u => u.User)
                .Where(i => i.InvitedPlayerId == userId).ToListAsync();
            ViewData["groupInvitations"] = groupInvitations;

            var groupAdverts = await _context.GroupAdvert.Include(g => g.Group).ToListAsync();
            ViewData["groupAdverts"] = groupAdverts.Take(5);

            ViewData["userId"] = userId;
            ViewData["groupInvitationsCount"] = groupInvitations.Count();
            ViewData["groupAdvertsCount"] = groupAdverts.Count();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> IndexAsync()
        {
            if (User.Identity.IsAuthenticated)
            {

                var user = await _userManager.GetUserAsync(User);
                var isPlayer = await _userManager.IsInRoleAsync(user, "Player");
                var isRenter = await _userManager.IsInRoleAsync(user, "Renter");
                

                ViewData["TopRatedGames"] = _context.Game.OrderByDescending(g => g.Rating).Take(5).ToList();

                if (isPlayer)
                {
                    var groupAdverts = _context.GroupAdvert.Include(g => g.Group).ThenInclude(g => g.Game)
                        .ToList();
                    groupAdverts.Reverse();
                    ViewData["GroupAdverts"] = groupAdverts.Take(5).ToList();
                    var groupInvitations = _context.GroupInvitation.Include(g => g.Group).ThenInclude(g => g.Game).Where(g => g.InvitedPlayerId == _userManager.GetUserId(User)).ToList();
                    groupInvitations.Reverse();
                    ViewData["GroupInvitations"] = groupInvitations.Take(5).ToList();

                    var groups = _context.GroupMember.Include(g => g.Group).ThenInclude(g => g.Game).Where(g => g.PlayerId == _userManager.GetUserId(User))
                        .Select(g => g.Group).ToList();
                    groups.Reverse();
                    ViewData["Groups"] = groups.Take(5).ToList();
                    ViewData["PlayingSoon"] = _context.GroupMember.Include(g => g.Group)
                        .ThenInclude(g => g.Game).Where(g => g.Group.TimeOfPlaying >= DateTime.Now && g.PlayerId == _userManager.GetUserId(User))
                        .OrderBy(g => g.Group.TimeOfPlaying)
                        .Select(g => g.Group).Distinct().ToList();

                    var rentals = _context.Rental.Include(r => r.InventoryItem).ThenInclude(i => i.Game)
                             .Include(r => r.InventoryItem).ThenInclude(i => i.Renter).ThenInclude(i => i.User)
                             .Where(r => r.PlayerId == _userManager.GetUserId(User)).ToList();
                    rentals.Reverse();
                    ViewData["Rentals"] = rentals.Take(5).ToList();
                }
                if (isRenter)
                {
                    var rentals = _context.Rental.Include(r => r.InventoryItem).ThenInclude(i => i.Game)
                                                 .Include(r => r.InventoryItem).ThenInclude(i => i.Renter).ThenInclude(i => i.User)
                        .Where(r => r.RenterId == _userManager.GetUserId(User)).ToList();
                    rentals.Reverse();
                    ViewData["Rentals"] = rentals.Take(5).ToList();
                    var inventoryItems = _context.InventoryItem.Include(i => i.Game).Where(ii => ii.RenterId == _userManager.GetUserId(User)).ToList();
                    inventoryItems.Reverse();
                    ViewData["InventoryItems"] = inventoryItems.Take(5).ToList();
                }

                var renters = _context.Renter.Include(r => r.User).ToList();
                renters.Reverse();
                ViewData["Renters"] = renters.Take(5).ToList();


                var players = _context.Player.Include(p => p.User).ToList();
                players.Reverse();
                ViewData["Players"] = players.Take(5).ToList();
                
                return View();
            }
            else
            {
                return RedirectToAction("Welcome");
            }

        }

        [Authorize]
        public IActionResult Profile(string id)
        {
            var renter = _context.Renter.Where(r => r.Id == id).FirstOrDefault();
            var player = _context.Player.Where(p => p.Id == id).FirstOrDefault();

            if (player != null)
            {
                return RedirectToAction("Details", "Players", new { id = id });
            }
            else if (renter != null)
            {
                return RedirectToAction("Details", "Renter", new { id = id });
            }
            else
            {
                return NotFound();
            }
        }
    }
}
