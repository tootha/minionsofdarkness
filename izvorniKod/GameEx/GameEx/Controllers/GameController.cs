﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace GameEx.Controllers
{
    public class GameController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public GameController(ApplicationDbContext context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: Game
        public async Task<IActionResult> Index(int page_number = 1,int page_size=30, string query = "")
        {
            var skipNum = (page_number - 1) * page_size;
            var applicationDbContext = _context.Game
                .Include(g => g.GameCategoryGames).ThenInclude(g => g.GameCategory)
                .Where(g => g.Name.ToUpper().Contains(query.ToUpper()))
                .OrderBy(g => g.Name)
                .Skip(skipNum).Take(page_size);
            var result = new PagedResult<Game>
            {
                Data = applicationDbContext.AsNoTracking().ToList(),
                TotalItems = _context.Game
                            .Where(g => g.Name.ToUpper().Contains(query.ToUpper())).Count(),
                PageNumber = page_number,
                PageSize = page_size
            };

            var user = await _userManager.GetUserAsync(User);
            IList<string> roles = null;
            if(user != null)
            {
                roles = await _userManager.GetRolesAsync(user);
            }
            if(roles != null)
            {
                ViewData["Roles"] = roles;
            }
            
            return View(result);
        }

        // GET: Game/Details/5
        [Route("Game/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game
                .Include(g => g.GameCategoryGames).ThenInclude(g => g.GameCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (game == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);
            IList<string> roles = null;
            if (user != null)
            {
                roles = await _userManager.GetRolesAsync(user);
            }
            if (roles != null)
            {
                ViewData["Roles"] = roles;
            }

            return View(game);
        }

        // GET: Game/Create
        [Authorize(Roles= "Moderator,Administrator")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Game/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> Create([Bind("Id,Name,PhotoUrl,ThumbnailUrl,Description,Link,MinPlayers,MaxPlayers,MinPlayingTime,MaxPlayingTime,Designer,Publisher,Year,MinAge")] Game game)
        {
            ModelState.Clear();

            game.Id = Guid.NewGuid().ToString();
            TryValidateModel(game);
            if (ModelState.IsValid)
            {
                _context.Add(game);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(game);
        }

        // GET: Game/Edit/5
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game.Include(g => g.GameCategoryGames)
                    .ThenInclude(gcg => gcg.GameCategory)
                .Where(g => g.Id == id).FirstOrDefaultAsync();
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }

        // POST: Game/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,PhotoUrl,ThumbnailUrl,CategoryId,Description,Link,MinPlayers,MaxPlayers,MinPlayingTime,MaxPlayingTime,Designer,Publisher,Year,MinAge")] Game game)
        {
            if (id != game.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var gameDb = _context.Game.AsNoTracking().Where(g => g.Id == id).FirstOrDefault();
                    if(gameDb != null)
                    {
                        game.Rating = gameDb.Rating;
                    }
                    _context.Update(game);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameExists(game.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(game);
        }

        // GET: Game/Delete/5
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Game
                .Include(g => g.GameCategoryGames).ThenInclude(g => g.GameCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // POST: Game/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Moderator,Administrator")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var game = await _context.Game.FindAsync(id);
            _context.Game.Remove(game);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public List<Game> Api(string term)
        {
            return _context.Game.Where(g => g.Name.ToUpper().Contains(term.ToUpper())).Take(10).ToList();
        }

        private bool GameExists(string id)
        {
            return _context.Game.Any(e => e.Id == id);
        }
    }
}
