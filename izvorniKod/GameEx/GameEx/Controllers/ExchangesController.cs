﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using GameEx.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using cloudscribe.Pagination.Models;
using Microsoft.Extensions.Options;

namespace GameEx.Controllers
{
    public class ExchangesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        

        public ExchangesController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
            
        }

        // GET: Exchanges
        public async Task<IActionResult> Index(int page_size = 10, int page_number1 = 1, int page_number2 = 1)
        {

            var skipNum1 = (page_number1 - 1) * page_size;
            var skipNum2 = (page_number2 - 1) * page_size;

            string userId;


            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound("Not Found\nSomething went wrong :(");

            }
            
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                var Initiated = _context.Exchange.Include(e => e.ExchangeStatus)
                                            .Include(e => e.Initiator).ThenInclude(m => m.User)
                                            .Include(e => e.OfferedGame)
                                            .Include(e => e.TargetPlayer).ThenInclude(m => m.User)
                                            .Include(e => e.WantedGame)
                                            .OrderByDescending(a => a.Created)
                                            .Where(a => a.InitiatorId == userId)
                                            .Skip(skipNum1).Take(page_size);


                var Targeted = _context.Exchange.Include(e => e.ExchangeStatus)
                                            .Include(e => e.Initiator).ThenInclude(m => m.User)
                                            .Include(e => e.OfferedGame)
                                            .Include(e => e.TargetPlayer).ThenInclude(m => m.User)
                                            .Include(e => e.WantedGame)
                                            .OrderByDescending(a => a.Created)
                                            .Where(a => a.TargetPlayerId == userId)
                                            .Skip(skipNum2).Take(page_size);

                


                var result1 = new PagedResult<Exchange>
                {
                    Data = await Initiated.AsNoTracking().ToListAsync(),
                    TotalItems = _context.Exchange.Where(a => a.InitiatorId == userId).Count(),
                    PageNumber = page_number1,
                    PageSize = page_size
                };

                var result2 = new PagedResult<Exchange>
                {
                    Data = await Targeted.AsNoTracking().ToListAsync(),
                    TotalItems = _context.Exchange.Where(a => a.TargetPlayerId == userId).Count(),
                    PageNumber = page_number2,
                    PageSize = page_size
                };

                List<bool> inExch = new List<bool>();
                foreach (var em in result2.Data) 
                {
                    var yes = await _context.Exchange.AsNoTracking()
                    .Where(m => m.ExchangeStatusId == "02" && ((m.InitiatorId == em.InitiatorId && m.OfferedGameId == em.OfferedGameId) 
                                                            || (m.TargetPlayerId == em.InitiatorId && m.WantedGameId == em.OfferedGameId)
                                                            || (m.InitiatorId == em.TargetPlayerId && m.OfferedGameId == em.WantedGameId) 
                                                            || (m.TargetPlayerId == em.TargetPlayerId && m.WantedGameId == em.WantedGameId)))
                    .ToListAsync();
                    if (yes.Count > 0)
                    {
                        inExch.Add(true);
                    }
                    else
                    {
                        inExch.Add(false);
                    }
                };

                    var tables = new ExchangesViewModel
                {
                    Initiated = result1,
                    Targeted = result2,
                    inExchange = inExch
                };
                ViewData["User"] = await _userManager.GetUserAsync(User);
                return View(tables);
            }
            catch
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
}

        // GET: Exchanges/Details/5
        public async Task<IActionResult> Details(string initId, string targID, string wantId, string offerId, string created)
        {
            if (initId == null )
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
            DateTime datum = DateTime.Parse(created);

            var exchange = await _context.Exchange.AsNoTracking()
                .Include(e => e.ExchangeStatus)
                .Include(e => e.Initiator).ThenInclude(m => m.User)
                .Include(e => e.OfferedGame)
                .Include(e => e.TargetPlayer).ThenInclude(m => m.User)
                .Include(e => e.WantedGame)
                .Where(m => m.InitiatorId == initId && m.TargetPlayerId == targID && m.WantedGameId == wantId && m.OfferedGameId == offerId && m.Created == datum).SingleOrDefaultAsync();

            var inexch = await _context.Exchange.AsNoTracking()
                    .Where(m => m.ExchangeStatusId == "02" && ((m.InitiatorId == initId && m.OfferedGameId == offerId) || (m.TargetPlayerId == initId && m.WantedGameId == offerId)
                                                            || (m.InitiatorId == targID && m.OfferedGameId == wantId) || (m.TargetPlayerId == targID && m.WantedGameId == wantId)))
                    .ToListAsync();

            ViewBag.inexchange = inexch.Count;

            //Console.WriteLine(inexch.Count);

            if (exchange == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            string userId;
            userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            ViewBag.currentId = userId;

            return View(exchange);
        }

        // GET: Exchanges/Create
        public async Task<IActionResult> Create( string targId, string wantId, string offerId )
        {
            if (targId == null || wantId ==null || offerId == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            string userId;
            userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            try
            {
                var exchang = new PotentialExchangeViewModel
                {
                    InitiatorId = userId,
                    Initiator = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == userId).SingleOrDefaultAsync(),
                    TargetPlayerId = targId,
                    TargetPlayer = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == targId).SingleOrDefaultAsync(),
                    OfferedGameId = offerId,
                    OfferedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == offerId && a.PlayerId == userId).SingleOrDefaultAsync(),
                    WantedGameId = wantId,
                    WantedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == wantId && a.PlayerId == targId).SingleOrDefaultAsync()
                };

                if (exchang.Initiator == null || exchang.TargetPlayer == null || exchang.WantedGame == null || exchang.OfferedGame == null) 
                {
                    return NotFound("Not Found\nSomething went wrong :(");
                } 
                return View(exchang);

            }
            catch (Exception exc)
            {
                return NotFound("Something went wrong :(");
            }

            
        }



        // POST: Exchanges/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TargetPlayerId,WantedGameId,OfferedGameId")] Exchange exchange, string targId, string wantId, string offerId)
        {
            string userId;
            userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (ModelState.IsValid)
            {
                exchange.TargetPlayerId = targId;
                exchange.OfferedGameId = offerId;
                exchange.WantedGameId = wantId;
                exchange.InitiatorId = userId;
                var date = DateTime.Now;
                date = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Kind);
                exchange.Created = date;
                exchange.ExchangeStatusId = "00";

                try 
                {

                    var Initiator = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == userId).SingleOrDefaultAsync();

                    var TargetPlayer = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == targId).SingleOrDefaultAsync();

                    var OfferedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == offerId && a.PlayerId == userId).SingleOrDefaultAsync();

                    var WantedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == wantId && a.PlayerId == targId).SingleOrDefaultAsync();

                    if (Initiator == null || TargetPlayer == null || OfferedGame == null || WantedGame == null) 
                    {
                        return NotFound("Not Found\nSomething went wrong :(");
                    }


                    _context.Add(exchange);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Details", "Players", new { id = targId });
                }
                catch (Exception exc)
                {
                    return NotFound("Something went wrong :(" + exc);
                }
            }


            return NotFound("Not Found\nSomething went wrong :(");
        }

       

        private bool ExchangeExists(string id)
        {
            return _context.Exchange.Any(e => e.InitiatorId == id);
        }

        // POST: Exchanges/Accept/5
        [HttpPost, ActionName("Accept")]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Accept(string initId, string targID, string wantId, string offerId, string created, string where)
        {
            //Console.WriteLine(created);
            //Console.WriteLine(targID);
            DateTime datum = DateTime.Parse(created);
            if (initId == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            var exchange = await _context.Exchange.AsNoTracking()
                .Where(m => m.InitiatorId == initId && m.TargetPlayerId == targID && m.WantedGameId == wantId && m.OfferedGameId == offerId && m.Created==datum ).SingleOrDefaultAsync();
          
            if (exchange == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            var yes = await _context.Exchange.AsNoTracking()
                    .Where(m => m.ExchangeStatusId == "02" && ((m.InitiatorId == initId && m.OfferedGameId == offerId) 
                                                            || (m.TargetPlayerId == initId && m.WantedGameId == offerId)
                                                            || (m.InitiatorId == targID && m.OfferedGameId == wantId) 
                                                            || (m.TargetPlayerId == targID && m.WantedGameId == wantId)))
                    .ToListAsync();

            if (yes.Count > 0)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            try
            {
                exchange.ExchangeStatusId = "02";
                _context.Update(exchange);
                await _context.SaveChangesAsync();
                if (where == "index")
                {
                    return RedirectToAction(nameof(Index));
                }
                else if (where == "details") 
                {
                    return RedirectToAction(nameof(Details), new { initId = exchange.InitiatorId, targID = exchange.TargetPlayerId, wantId = exchange.WantedGameId, offerId = exchange.OfferedGameId, created = exchange.Created.ToString() });
                }
                return RedirectToAction(nameof(Index));

            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
           
        }

        // POST: Exchanges/Reject/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Reject(string initId, string targID, string wantId, string offerId, string created)
        {
            Console.WriteLine(created);
            Console.WriteLine(targID);
            DateTime datum = DateTime.Parse(created);
            if (initId == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            var exchange = await _context.Exchange.AsNoTracking()
                .Where(m => m.InitiatorId == initId && m.TargetPlayerId == targID && m.WantedGameId == wantId && m.OfferedGameId == offerId && m.Created == datum).SingleOrDefaultAsync();
            
            if (exchange == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            try
            {
                _context.Exchange.Remove(exchange);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
            
        }

        // POST: Exchanges/ReturnGame/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ReturnGame(string initId, string targID, string wantId, string offerId, string created)
        {
            Console.WriteLine(created);
            Console.WriteLine(targID);
            DateTime datum = DateTime.Parse(created);
            if (initId == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            var exchange = await _context.Exchange.AsNoTracking()
                .Where(m => m.InitiatorId == initId && m.TargetPlayerId == targID && m.WantedGameId == wantId && m.OfferedGameId == offerId && m.Created == datum).SingleOrDefaultAsync();

            if (exchange == null)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }

            try
            {
                _context.Exchange.Remove(exchange);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));

            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
            
        }

        public RedirectToActionResult CancelExchange(string targId) 
        {
            return RedirectToAction("Details", "Players", new { id = targId });
        }


        //GET: Exchange/Offer
        public async Task<IActionResult> Offer(string targId, string wantId, int page_size = 10, int page_number=1)
        {
            if (targId == null || wantId == null)
            {
                return NotFound("Something went wrong :(\nPlease go back and try again");
            }
            var skipNum = (page_number - 1) * page_size;
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound("Not Found\nSomething went wrong :(");
            }
            try
            {
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (appctx.Count() != 1)
                {
                    return NotFound("Not Found\nSomething went wrong :(");
                }
            }
            catch
            {

            }
            ViewBag.targetId = targId;
            ViewBag.wantedId = wantId;

            Console.WriteLine(targId);

            var applicationDbContext = _context.PlayerGame
                                        .Include(p => p.Game)
                                        .Include(p => p.Player)
                                        .Where(p => p.PlayerId == userId)
                                        .OrderBy(g => g.Game.Name)
                                        .Skip(skipNum).Take(page_size);

            var result = new PagedResult<PlayerGame>
            {
                Data = await applicationDbContext.AsNoTracking().ToListAsync(),
                TotalItems = _context.PlayerGame.Where(a => a.PlayerId == userId).Count(),
                PageNumber = page_number,
                PageSize = page_size
            };


            return View(result);
        }

        // POST: Exchanges/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Offer(string targId, string wantId, string offerId)
        {
            string userId;
            userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                

            try
            {

                var Initiator = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == userId).SingleOrDefaultAsync();

                var TargetPlayer = await _context.Player.AsNoTracking().Include(a => a.User).Where(a => a.Id == targId).SingleOrDefaultAsync();

                var OfferedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == offerId && a.PlayerId == userId).SingleOrDefaultAsync();

                var WantedGame = await _context.PlayerGame.AsNoTracking().Include(a => a.Game).Where(a => a.Game.Id == wantId && a.PlayerId == targId).SingleOrDefaultAsync();

                if (Initiator == null || TargetPlayer == null || OfferedGame == null || WantedGame == null)
                {
                    return NotFound("Not Found\nSomething went wrong :(");
                }


                
                return RedirectToAction("Create", "Exchanges", new { targId=targId, wantId=wantId, offerId=offerId });
            }
            catch (Exception exc)
            {
                return NotFound("Something went wrong :(" + exc);
            }



            //return NotFound("Not Found\nSomething went wrong :(");
        }

    }
}
