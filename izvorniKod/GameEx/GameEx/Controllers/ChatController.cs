﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        public readonly ApplicationDbContext _context;
        public readonly UserManager<User> _userManager;
        public ChatController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult AllMessages()
        {
            return View();
        }

        [Authorize(Roles ="Moderator")]
        public IActionResult AllMessagesModerator()
        {
            return View();
        }


        public async Task<IActionResult> Create(Message message)
        {
            if (ModelState.IsValid)
            {
                message.Id = Guid.NewGuid().ToString();
                message.SenderUserName = User.Identity.Name;
                var sender = await _userManager.GetUserAsync(User);
                await _context.Message.AddAsync(message);
                await _context.SaveChangesAsync();
                return Ok();
            }
            return Error();
        }


        public async Task<IActionResult> CreateGroupMessage(GroupMessage message)
        {
            if (ModelState.IsValid)
            {
                message.Id = Guid.NewGuid().ToString();
                //message.UserName = User.Identity.Name;
                
                var sender = await _userManager.GetUserAsync(User);

                message.SenderId = sender.Id;
                await _context.GroupMessage.AddAsync(message);
                await _context.SaveChangesAsync();
                return Ok();
            }
            return Error();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("Chat/User/{id}")]
        public IActionResult MessageUser(string id)
        {
            var messages = _context.Message.Where(m => m.Receiver.Id == _userManager.GetUserId(User)
                                                        && m.Sender.Id == id
                                                        || m.Receiver.Id == id
                                                        && m.Sender.Id == _userManager.GetUserId(User)
                ).OrderByDescending(m => m.When).ToList();

            ViewData["Messages"] = messages.OrderByDescending(m => m.When);
            ViewData["UserId"] = id;
            ViewData["UserName"] = _context.User.Where(u => u.Id == id).FirstOrDefault();
            return View(messages);
        }
        [Route("Chat/Group/{id}")]
        public IActionResult MessageGroup(string id)
        {
            var membership = _context.GroupMember.Where(g => g.GroupId == id && g.PlayerId == _userManager.GetUserId(User)).FirstOrDefault();
            if(membership == null)
            {
                return NotFound();
            }
            var messages = _context.GroupMessage.Where(m => m.GroupId == id).OrderByDescending(m => m.When).ToList();

            ViewData["Messages"] = messages.OrderByDescending(m => m.When);
            ViewData["UserId"] = id;
            ViewData["UserName"] = _context.User.Where(u => u.Id == id).FirstOrDefault();
            ViewData["GroupId"] = id;
            return View(messages);
        }




        private const int BATCH_SIZE = 10;
        [HttpPost]
        public IActionResult _MessagePartial(string id, string sortOrder, string searchString, int firstItem = 0)
        {
            var user = _context.User.Where(u => u.Id == id).FirstOrDefault();
            var messages = new List<Message>();
            if(user != null)
            {
                messages = _context.Message.Include(m => m.Receiver)
                                                .Include(m => m.Sender)
                                                .Where(m => (m.ReceiverUserName == User.Identity.Name
                                                            && m.SenderUserName == user.UserName)
                                                            || (m.ReceiverUserName == user.UserName
                                                            && m.SenderUserName == User.Identity.Name)
                    ).OrderByDescending(m => m.When).ToList();
            }
            var query = messages;
            // Extract a portion of data
            var model = query.Skip(firstItem).Take(BATCH_SIZE).Reverse().ToList();
            if (model.Count() == 0) return StatusCode(204);  // 204 := "No Content"
            return PartialView(model);
        }

        [HttpPost]
        public IActionResult _ModeratorUserMessagePartial(string SenderId, string ReceiverId, string sortOrder, string searchString, int firstItem = 0)
        {
            var messages = new List<Message>();
            messages = _context.Message.Include(m => m.Receiver)
                                            .Include(m => m.Sender)
                                            .Where(m => (m.Sender.Id == SenderId
                                                        && m.Receiver.Id == ReceiverId
                                                        || m.Sender.Id == ReceiverId
                                                        && m.Receiver.Id == SenderId)
                ).OrderByDescending(m => m.When).ToList(); var query = messages;
            // Extract a portion of data
            var model = query.Skip(firstItem).Take(BATCH_SIZE).Reverse().ToList();
            if (model.Count() == 0) return StatusCode(204);  // 204 := "No Content"
            return PartialView(model);
        }


        [HttpPost]
        public IActionResult _ModeratorGroupMessagePartial(string id, string sortOrder, string searchString, int firstItem = 0)
        {
            var messages = new List<GroupMessage>();
            messages = _context.GroupMessage.Include(m => m.Sender)
                                            .Where(m => m.GroupId == id)
                                            .OrderByDescending(m => m.When).ToList(); var query = messages;
            // Extract a portion of data
            var model = query.Skip(firstItem).Take(BATCH_SIZE).Reverse().ToList();
            if (model.Count() == 0) return StatusCode(204);  // 204 := "No Content"
            return PartialView(model);
        }


        public IActionResult MessageModeratorUser(string SenderId, string ReceiverId)
        {
            var messages = _context.Message.Include(m => m.Sender).Include(m => m.Receiver)
                .Where(m => m.Sender.Id == SenderId
                                            && m.Receiver.Id == ReceiverId
            ).OrderByDescending(m => m.When).ToList();

            ViewData["Messages"] = messages.OrderByDescending(m => m.When);
            return View(messages);
        }

        public IActionResult MessageModeratorGroup(string id)
        {
            var messages = _context.GroupMessage.Include(m => m.Sender)
                    .Where(m => m.GroupId == id).OrderByDescending(m => m.When).ToList();

            ViewData["Messages"] = messages.OrderByDescending(m => m.When);
            //ViewData["UserId"] = id;
            ViewData["GroupId"] = id;
            return View(messages);
        }

        public IActionResult DeleteUserMessage(string id)
        {
            if(id == null)
            {
                return NotFound();
            }

            var message = _context.Message.Include(m => m.Sender).Include(m => m.Receiver)
                .Where(m => m.Id == id).FirstOrDefault();
            if(message == null)
            {
                return NotFound();
            }

            try
            {
                _context.Remove(message);
                _context.SaveChanges();
            }catch(Exception exc)
            {
                return NotFound();
            }
            return RedirectToAction("MessageModeratorUser","Chat",new {SenderId = message.Sender.Id, ReceiverId = message.Receiver.Id});
        }

        public IActionResult DeleteGroupMessage(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var message = _context.GroupMessage.Include(m => m.Sender)
                .Where(m => m.Id == id).FirstOrDefault();
            if (message == null)
            {
                return NotFound();
            }

            try
            {
                _context.Remove(message);
                _context.SaveChanges();
            }
            catch (Exception exc)
            {
                return NotFound();
            }
            return RedirectToAction("MessageModeratorGroup", "Chat", new { Id = message.GroupId });
        }
    }
}

