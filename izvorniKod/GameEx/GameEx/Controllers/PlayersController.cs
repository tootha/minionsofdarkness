﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace GameEx.Controllers
{
    [Authorize]
    public class PlayersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public PlayersController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Players
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Player.Include(p => p.User);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Players/Details/5
        public async Task<IActionResult> Details(string id)
        {
            string userId;
            if (id == null)
            {
                return NotFound();
            }

            try
            {
                userId = _userManager.GetUserId(User);
                try
                {
                    var appctx = _context.User.Where(p => p.Id == userId);
                    if (appctx.Count() != 1)
                    {
                        return NotFound();
                    }
                }
                catch
                {
                    return NotFound();
                }
            }
            catch
            {
                return NotFound();
            }

            ViewData["Reviews"] = await _context.Review.Include(p => p.ReviewedPlayer).ThenInclude(u => u.User)
                .Include(r => r.Reviewer).ThenInclude(u => u.User)
                .Include(g => g.Group)
                .Where(p => p.ReviewedPlayerId == id).ToListAsync();

            var gcdb = await _context.PlayerGame.Include(pg => pg.Game).ThenInclude(g => g.GameCategoryGames)
                .ThenInclude(g => g.GameCategory)
                .Where(pg => pg.PlayerId == id).ToListAsync();
            ViewData["GameCollection"] = gcdb.Take(5);
            ViewData["GameCollectionCount"] = gcdb.Count();

            ViewData["userId"] = userId;
            var player = _context.Player.Include(p => p.User)
                .Include(u => u.PlayerGames).ThenInclude(pg => pg.Game).ThenInclude(i => i.GameCategoryGames).ThenInclude(u => u.GameCategory)
                .Include(u => u.GroupMemberships).ThenInclude(u => u.Group).ThenInclude(g => g.Game)
                .Include(u => u.InitiatedExchanges)
                .Include(u => u.TargetedExchanges).Include(u => u.Rentals)
                .Include(u => u.TargetedExchanges).ThenInclude(z => z.ExchangeStatus)
                .Include(u => u.TargetedExchanges).ThenInclude(z => z.WantedGame)
                .Include(u => u.TargetedExchanges).ThenInclude(z => z.OfferedGame)
                .Include(u => u.TargetedExchanges).ThenInclude(u => u.TargetPlayer).ThenInclude(t => t.User)
                .Include(u => u.InitiatedExchanges).ThenInclude(z => z.ExchangeStatus)
                .Include(u => u.InitiatedExchanges).ThenInclude(z => z.WantedGame)
                .Include(u => u.InitiatedExchanges).ThenInclude(z => z.OfferedGame)
                .Include(u => u.InitiatedExchanges).ThenInclude(u => u.TargetPlayer).ThenInclude(t => t.User)
                .Where(p => p.Id == id)
                .FirstOrDefault();
            
            if (player == null)
            {
                return NotFound();
            }

            return View(player);
        }


        // GET: Players/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (id == null || appctx.Count() != 1)
                {
                    return NotFound();
                }

                var player = await _context.Player.Include(p => p.User)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (player == null)
                {
                    return NotFound();
                }

                ViewData["userId"] = userId;
                ViewData["Id"] = new SelectList(_context.Set<User>(), "Id", "Id", player.Id);
                return View(player);
            } catch (Exception)
            {
                return RedirectToAction(nameof(Edit));
            }
            
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id")] Player player)
        {
            if (id != player.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(player);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlayerExists(player.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Details));
            }
            ViewData["Id"] = new SelectList(_context.Set<User>(), "Id", "Id", player.Id);
            return View(player);
        }

        // GET: Players/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var player = await _context.Player
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (player == null)
            {
                return NotFound();
            }

            return View(player);
        }

        // POST: Players/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var player = await _context.Player.FindAsync(id);
            _context.Player.Remove(player);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlayerExists(string id)
        {
            return _context.Player.Any(e => e.Id == id);
        }

        public async Task<IActionResult> Rentals(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var appctx = _context.Player.Where(p => p.Id == userId);
                if (id == null || appctx.Count() != 1)
                {
                    return NotFound();
                }

                var rentals = await _context.Player.Include(p => p.Rentals).ThenInclude(r => r.InventoryItem).ThenInclude(ii => ii.Game)
                .FirstOrDefaultAsync(m => m.Id == id);
                if (rentals == null)
                {
                    return NotFound();
                }

                ViewData["userId"] = userId;
                return View(rentals);
            }
            catch
            {
                return NotFound();
            }
        }

        public List<Player> Api(string term)
        {
            return _context.Player.Include(p => p.User)
                .Where(p => p.User.NormalizedUserName.Contains(term.ToUpper().Trim()))
                .Select(p => new Player
                {
                    Id = p.Id,
                    User = new Models.User
                    { UserName = p.User.UserName }
                })
                .Take(10).ToList();
        }
    }
}
