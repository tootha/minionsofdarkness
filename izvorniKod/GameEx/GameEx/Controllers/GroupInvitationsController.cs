﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Controllers
{
    [Authorize(Roles = "Player,Moderator,Administrator")]
    public class GroupInvitationsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public GroupInvitationsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var invitations = _context.GroupInvitation.Include(g => g.Group).ThenInclude(g => g.Game)
                .Include(g => g.Group).ThenInclude(g => g.GroupMembers).ThenInclude(g => g.Player).ThenInclude(p => p.User)
                .Where(gi => gi.InvitedPlayerId == user.Id).ToList();
            foreach(var inv in invitations)
            {
                inv.Group.GroupMembers = inv.Group.GroupMembers.Where(g => g.Owner).ToList();
            }
            ViewData["User"] = await _userManager.GetUserAsync(User);
            return View(invitations);
        }

        public async Task<IActionResult> AcceptInvite(string id)
        {
            var invitation = _context.GroupInvitation.Where(i => i.Id == id).FirstOrDefault();
            if(invitation == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetUserAsync(User);
            var membership = new GroupMember
            {
                PlayerId = user.Id,
                GroupId = invitation.GroupId
            };

            _context.Add(membership);
            _context.Remove(invitation);
            _context.SaveChanges();
            return RedirectToAction("Details", "Groups", new { id = invitation.GroupId});
        }

        public async Task<IActionResult> RejectInvite(string id)
        {
            var invitation = _context.GroupInvitation.Where(i => i.Id == id).FirstOrDefault();
            if (invitation == null)
            {
                return NotFound();
            }
            var user = await _userManager.GetUserAsync(User);
            if(User.Identity.Name == user.UserName)
            {
                _context.Remove(invitation);
                _context.SaveChanges();
                return RedirectToAction("Index", "GroupInvitations");
            }
            else
            {
                return Unauthorized();
            }

        }
    }
}
