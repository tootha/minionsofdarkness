﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace GameEx.Controllers
{
    [Authorize(Roles = "Player,Moderator,Administrator")]
    public class GroupAdvertsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public GroupAdvertsController(ApplicationDbContext context,UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        // GET: GroupAdverts
        public async Task<IActionResult> Index(int page_number = 1, int page_size = 30, string query = "")
        {
            var skip = (page_number - 1) * page_size;
            var applicationDbContext = _context.GroupAdvert.Include(g => g.Group).ThenInclude(g => g.Game).ToList();
            var data = applicationDbContext.Where(g => g.Text.ToUpper().Contains(query.ToUpper())
                                || g.Title.ToUpper().Contains(query.ToUpper())
                                || g.Group.Game.Name.ToUpper().Contains(query.ToUpper()))
                .Skip(skip).Take(page_size).ToList();
            var result = new PagedResult<GroupAdvert>
            {
                Data = data,
                PageNumber = page_number,
                PageSize = page_size,
                TotalItems = applicationDbContext.Count()
            };
            return View(result);
        }

        // GET: GroupAdverts/Details/5
        public async Task<IActionResult> Details(string GroupId)
        {
            if (GroupId == null)
            {
                return NotFound();
            }

            var groupAdvert = await _context.GroupAdvert
                .Include(g => g.Group).ThenInclude(g => g.Game)
                .Where(g => g.GroupId == GroupId)
                .FirstOrDefaultAsync();
            if (groupAdvert == null)
            {
                return NotFound();
            }

            return View(groupAdvert);
        }

        // GET: GroupAdverts/Create
        public async Task<IActionResult> Create(string GroupId)
        {
            ViewData["GroupId"] = GroupId;
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == GroupId)
                .FirstOrDefaultAsync();
            if(membership == null || !membership.Owner)
            {
                return NotFound();
            }
            return View();
        }

        // POST: GroupAdverts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Text,Title,GroupId")] GroupAdvert groupAdvert)
        {
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == groupAdvert.GroupId)
                .FirstOrDefaultAsync();
            if(membership == null || !membership.Owner)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                groupAdvert.Id = Guid.NewGuid().ToString();
                _context.Add(groupAdvert);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", groupAdvert.GroupId);
            return View(groupAdvert);
        }

        // GET: GroupAdverts/Edit/5
        public async Task<IActionResult> Edit(string GroupId)
        {
            if (GroupId == null)
            {
                return NotFound();
            }
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == GroupId)
                .FirstOrDefaultAsync();
            if (membership == null || !membership.Owner)
            {
                return NotFound();
            }
            var groupAdvert = await _context.GroupAdvert.Where(g => g.GroupId == GroupId).FirstOrDefaultAsync();
            if (groupAdvert == null)
            {
                return NotFound();
            }
            ViewData["GroupId"] = GroupId;
            return View(groupAdvert);
        }

        // POST: GroupAdverts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Text,Title,GroupId")] GroupAdvert groupAdvert)
        {
            if (id != groupAdvert.Id)
            {
                return NotFound();
            }
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == groupAdvert.GroupId)
                .FirstOrDefaultAsync();
            if (membership == null || !membership.Owner)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(groupAdvert);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupAdvertExists(groupAdvert.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = groupAdvert.GroupId;
            return View(groupAdvert);
        }

        // GET: GroupAdverts/Delete/5
        public async Task<IActionResult> Delete(string GroupId)
        {
            if (GroupId == null)
            {
                return NotFound();
            }
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == GroupId)
                .FirstOrDefaultAsync();
            if (membership == null || !membership.Owner)
            {
                return NotFound();
            }

            var groupAdvert = await _context.GroupAdvert
                .Include(g => g.Group).ThenInclude(g => g.Game)
                .Where(m => m.GroupId == GroupId).FirstOrDefaultAsync();
            if (groupAdvert == null)
            {
                return NotFound();
            }

            return View(groupAdvert);
        }

        // POST: GroupAdverts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {

            var groupAdvert = await _context.GroupAdvert.FindAsync(id);
            var groupId = groupAdvert.GroupId;
            var membership = await _context.GroupMember
                .Where(g => g.PlayerId == _userManager.GetUserId(User) && g.GroupId == groupId)
                .FirstOrDefaultAsync();
            if (membership == null || !membership.Owner)
            {
                return NotFound();
            }
            _context.GroupAdvert.Remove(groupAdvert);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details","Groups", new { Id = groupId});
        }

        private bool GroupAdvertExists(string id)
        {
            return _context.GroupAdvert.Any(e => e.Id == id);
        }
    }
}
