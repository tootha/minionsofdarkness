﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using cloudscribe.Pagination.Models;

namespace GameEx.Controllers
{
    [Authorize]
    public class InventoryItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public InventoryItemsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: InventoryItems
        [Authorize(Roles = "Renter")]
        public IActionResult Index(int page_number = 1, int page_size = 20)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch
            {
                return NotFound();

            }
            try
            {
                var skipNum = (page_number - 1) * page_size;
                var applicationDbContext = _context.InventoryItem
                    .Include(i => i.Game).Include(i => i.Renter)
                    .Where(r => r.Renter.Id == userId)
                    .OrderBy(g => g.Game.Name)
                    .Skip(skipNum).Take(page_size);
                int inventoryCount = _context.InventoryItem
                    .Include(i => i.Game).Include(i => i.Renter)
                    .Where(r => r.Renter.Id == userId).Count();

                var result = new PagedResult<InventoryItem>
                {
                    Data = applicationDbContext.AsNoTracking().ToList(),
                    TotalItems = inventoryCount,
                    PageNumber = page_number,
                    PageSize = page_size
                };
                return View(result);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
        }

        // GET: InventoryItems/Details/5
        [Authorize(Roles = "Player,Renter")]
        public async Task<IActionResult> Details(string id, string renterId)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var inventoryItem = await _context.InventoryItem
                    .Include(i => i.Game).Include(i => i.Renter)
                    .FirstOrDefaultAsync(i => i.GameId == id && i.RenterId == renterId);

                var taken = _context.Rental
                .Where(i => i.GameId == id && i.RenterId == renterId).Count();

                if (inventoryItem == null)
                {
                    return NotFound();
                }

                ViewData["availableStatus"] = inventoryItem.Quantity - taken;
                return View(inventoryItem);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }

        }

        // GET: InventoryItems/Create
        [Authorize(Roles = "Renter")]
        public IActionResult Create()
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return RedirectToAction("Index", "HomeController");
            }
            ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id");
            ViewData["RenterId"] = new SelectList(_context.Renter, "Id", "Id");
            return View();
        }

        // POST: InventoryItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Renter")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GameId,Quantity,Price")] InventoryItem inventoryItem, [Bind("GameName")] string GameName)
        {
            if (inventoryItem.GameId == null && GameName != null)
            {
                var gameId = _context.Game.Where(g => g.Name == GameName).Select(g => g.Id).ToList();
                if (gameId.Count() > 1 || gameId.Count < 1)
                {
                    ModelState.AddModelError("GameId", "There are multiple games with the same name or the game doesn't exist.");
                }
                else
                {
                    inventoryItem.GameId = gameId[0];
                }
            }
            else if (inventoryItem.GameId != null && GameName != null)
            {
                var game = _context.Game.Where(g => g.Id == inventoryItem.GameId).ToList();
                if (game.Count() > 1 || game.Count < 1)
                {
                    ModelState.AddModelError("GameId", "Entered game doesn't exist.");
                }
                else
                {
                    if (game[0].Name == GameName)
                    {
                        inventoryItem.GameId = game[0].Id;
                    }
                    else
                    {
                        var gameId = _context.Game.Where(g => g.Name == GameName).Select(g => g.Id).ToList();
                        if (gameId.Count() > 1 || gameId.Count < 1)
                        {
                            ModelState.AddModelError("GameId", "There are multiple games with the same name or the game doesn't exist.");
                        }
                        else
                        {
                            inventoryItem.GameId = gameId[0];
                        }
                    }

                }
            }
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return RedirectToAction("Index", "HomeController");
            }
            inventoryItem.RenterId = userId;
            if (ModelState.IsValid)
            {
                try
                {
                    var check = _context.InventoryItem.Where(i => i.RenterId == inventoryItem.RenterId && i.GameId == inventoryItem.GameId).FirstOrDefault();
                    if(check != null)
                    {
                        ModelState.AddModelError("GameId", "You already have this item in your inventory!");
                    }
                    if (ModelState.IsValid)
                    {
                        _context.Add(inventoryItem);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }
                    return View(inventoryItem);
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                    return NotFound();
                }
            }
            ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id", inventoryItem.GameId);
            ViewData["RenterId"] = new SelectList(_context.Renter, "Id", "Id", inventoryItem.RenterId);
            return View(inventoryItem);
        }

        // GET: InventoryItems/Edit/5
        [Authorize(Roles = "Renter")]
        public async Task<IActionResult> Edit(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
            if (id == null)
            {
                return NotFound();
                
            }
            try
            {
                var inventoryItem = await _context.InventoryItem.Include(g => g.Game)
                .Where(ii => ii.RenterId == userId && ii.GameId == id).FirstOrDefaultAsync();
                if (inventoryItem == null)
                {
                    return NotFound();
                }
                ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id", inventoryItem.GameId);
                ViewData["RenterId"] = new SelectList(_context.Renter, "Id", "Id", inventoryItem.RenterId);
                return View(inventoryItem);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
        }

        // POST: InventoryItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        [Authorize(Roles = "Renter")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("GameId,Quantity,Price")] InventoryItem inventoryItem)
        {
            if (id == null)
            {
                return NotFound();

            }
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        inventoryItem.RenterId = userId;
                        _context.Update(inventoryItem);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!InventoryItemExists(inventoryItem.RenterId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["GameId"] = new SelectList(_context.Set<Game>(), "Id", "Id", inventoryItem.GameId);
                ViewData["RenterId"] = new SelectList(_context.Renter, "Id", "Id", inventoryItem.RenterId);
                return View(inventoryItem);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
        }

        // GET: InventoryItems/Delete/5
        [Authorize(Roles = "Renter")]
        public async Task<IActionResult> Delete(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                return NotFound();
            }
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var inventoryItem = await _context.InventoryItem.Include(g => g.Game)
                    .Where(ii => ii.RenterId == userId && ii.GameId == id).FirstOrDefaultAsync();
                var rentals = _context.Rental.Where(r => r.RenterId == userId && r.GameId == id).ToList();
                ViewData["ActiveRentalsCount"] = rentals.Count();
                if (inventoryItem == null)
                {
                    return NotFound();
                }
                return View(inventoryItem);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
        }

        // POST: InventoryItems/Delete/5
        [Authorize(Roles = "Renter")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            string userId;
            try
            {
                userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
            try
            {
                var inventoryItem = await _context.InventoryItem.FindAsync(userId, id);
                var rentals = await _context.Rental.Where(r => r.RenterId == userId && r.GameId == id).ToListAsync();
                _context.Rental.RemoveRange(rentals);
                _context.InventoryItem.Remove(inventoryItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return NotFound();
            }
        }

        private bool InventoryItemExists(string id)
        {
            return _context.InventoryItem.Any(e => e.RenterId == id);
        }
    }
}
