﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using GameEx.Helpers;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

using PayPalCheckoutSdk.Orders;
using GameEx.Paypal;
using PayPalHttp;

namespace GameEx.Controllers
{
    //[Authorize]
    public class CartController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public CartController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Game
        [Authorize(Roles = "Player")]
        public IActionResult Index()
        {
            List<int> wantedQuantityList = new List<int>();
            var cart = new List<(string, string, int)>();
            try
            {
                cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
                if (cart == null)
                {
                    cart = new List<(string, string, int)>();
                    SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                }
            }catch(Exception exc)
            {
                Console.WriteLine(exc);
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                return RedirectToAction("Index", "Cart");
            }
            List<InventoryItem> items = new List<InventoryItem>();
            decimal total = 0;
            try
            {
                for (int i = 0; i < cart.Count(); i++)
                {
                    InventoryItem itemsInCart = _context.InventoryItem.AsNoTracking()
                        .Include(g => g.Game)
                        .Include(g => g.Renter).ThenInclude(g => g.User)
                        .Where(g => g.GameId == cart[i].Item1 && g.RenterId == cart[i].Item2).SingleOrDefault();
                    items.Add(itemsInCart);
                    total += itemsInCart.Price * cart[i].Item3;
                    wantedQuantityList.Add(cart[i].Item3);

                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                return RedirectToAction("Index", "Cart");
            }
            ViewBag.total = total;
            ViewBag.wantedQuantityList = wantedQuantityList;
            ViewBag.lastVisitedRenter = SessionHelper.GetObjectFromJson<string>(HttpContext.Session, "lastVisitedRenter");
            //Console.WriteLine(ViewBag.lastVisitedRenter);
            return View(items);
        }

        [Authorize(Roles = "Player")]
        public IActionResult AddToCart(string id, string renterId)
        {
            try
            {
                string lastVisitedRenter = renterId;
                SessionHelper.SetObjectAsJson(HttpContext.Session, "lastVisitedRenter", lastVisitedRenter);
                ViewBag.lastVisitedRenter = lastVisitedRenter;

                if (SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart") == null)
                {
                    List<(string, string, int)> cart = new List<(string, string, int)>();
                    cart.Add((id, renterId, 1));

                    SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);

                }
                else
                {
                    List<(string, string, int)> cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
                    bool hadItem = false;
                    for (int i = 0; i < cart.Count(); i++)
                    {
                        if (cart[i].Item1 == id && cart[i].Item2 == renterId)
                        {
                            hadItem = true;
                            cart[i] = (cart[i].Item1, cart[i].Item2, cart[i].Item3 + 1);
                            break;
                        }
                    }
                    if (hadItem == false)
                    {
                        cart.Add((id, renterId, 1));
                    }
                    SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);

                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                List<(string, string, int)> cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }

            return RedirectToAction(nameof(Index));
        }
        [Authorize(Roles = "Player")]
        public IActionResult RemoveFromCart(string id, string renterId)
        {
            try
            {
                if (SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart") == null)
                {

                }
                else
                {
                    List<(string, string, int)> cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
                    for (int i = 0; i < cart.Count(); i++)
                    {
                        if (cart[i].Item1 == id && cart[i].Item2 == renterId)
                        {
                            cart.Remove((cart[i].Item1, cart[i].Item2, cart[i].Item3));
                            break;
                        }
                    }
                    SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                List<(string, string, int)> cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            return RedirectToAction(nameof(Index));
        }
        [Authorize(Roles = "Player")]
        public IActionResult CancelOrder(string renterId)
        {
            //Console.WriteLine(renterId);
            List<(string, string, int)> cart = new List<(string, string, int)>();
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Details", "Renter", new { Id = renterId });
        }
        [Authorize(Roles = "Player")]
        public IActionResult Checkout()
        {
            //uzmi session elemente, izbriši ono što nema više
            //stavi da su rentane sve stvari u košari dok se ne dovrši transakcija

            List<int> wantedQuantityList = new List<int>();
            var cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
            List<InventoryItem> items = new List<InventoryItem>();
            var cartModified = new List<(string, string, int)>();

            string userId;
            userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            List<string> itemsRemoveNames = new List<string>();
            ViewBag.itemsRemovedNames = itemsRemoveNames;
            decimal total = 0;
            try
            {
                for (int i = 0; i < cart.Count(); i++)
                {
                    InventoryItem itemsInCart = _context.InventoryItem
                        .Include(g => g.Rentals)
                        .Include(g => g.Game)
                        .Include(g => g.Renter).ThenInclude(g => g.User)
                        .Where(g => g.GameId == cart[i].Item1 && g.RenterId == cart[i].Item2).SingleOrDefault();

                    //ovdje dodati provjeru ako je netko zauzeo mjesto proizvoda
                    if (itemsInCart.Quantity - itemsInCart.Rentals.Count() >= cart[i].Item3)
                    {
                        cartModified.Add((cart[i].Item1, cart[i].Item2, cart[i].Item3));
                        items.Add(itemsInCart);
                        total += itemsInCart.Price * cart[i].Item3;
                        wantedQuantityList.Add(cart[i].Item3);
                    }
                    else
                    {
                        itemsRemoveNames.Add(itemsInCart.Game.Name);
                    }
                }
                ViewBag.itemsRemovedNames = itemsRemoveNames;
            }
            catch (Exception exc)
            {
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                Console.WriteLine(exc);
                return RedirectToAction(nameof(Index));
            }
            if (items.Count() == 0)
            {
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                return RedirectToAction(nameof(Index));
            }
            //ako je spremi konačni cart u session kako ne bi bilo razlike između servera i sessiona
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cartModified);



            ViewBag.total = total;
            ViewBag.wantedQuantityList = wantedQuantityList;
            ViewBag.lastVisitedRenter = SessionHelper.GetObjectFromJson<string>(HttpContext.Session, "lastVisitedRenter");

            return View(items);
        }

        
        [Authorize(Roles = "Player")]
        [Route("Cart/CreateOrder")]
        [HttpPost]
        public async static Task<HttpResponse> CreateOrder(bool debug = false, List<(InventoryItem, int)> itemAndQuantity = null, string currencyName = "USD")
        {
            var request = new OrdersCreateRequest();
            request.Headers.Add("prefer", "return=representation");
            request.RequestBody(BuildRequestBody(itemAndQuantity, currencyName));
            var response = await PayPalClient.client().Execute(request);

            if (debug)
            {
                var result = response.Result<Order>();
                Console.WriteLine("Status: {0}", result.Status);
                Console.WriteLine("Order Id: {0}", result.Id);
                Console.WriteLine("Intent: {0}", result.CheckoutPaymentIntent);
                Console.WriteLine("Links:");
                foreach (LinkDescription link in result.Links)
                {
                    Console.WriteLine("\t{0}: {1}\tCall Type: {2}", link.Rel, link.Href, link.Method);
                }
                AmountWithBreakdown amount = result.PurchaseUnits[0].AmountWithBreakdown;
                Console.WriteLine("Total Amount: {0} {1}", amount.CurrencyCode, amount.Value);
                Console.WriteLine("Response JSON: \n {0}", PayPalClient.ObjectToJSONString(result));
            }

            return response;
        }
        [Authorize(Roles = "Player")]
        private static OrderRequest BuildRequestBody(List<(InventoryItem, int)> itemAndQuantity, string currencyName)
        {
            List<Item> items = new List<Item>();
            decimal total = 0;
            for (int i = 0; i < itemAndQuantity.Count(); i++)
            {
                Item item = new Item
                {
                    Name = itemAndQuantity[i].Item1.Game.Name,
                    Description = "Game from: " + itemAndQuantity[i].Item1.Renter.CompanyName,
                    Sku = "sku01",
                    UnitAmount = new Money
                    {
                        CurrencyCode = currencyName,
                        Value = itemAndQuantity[i].Item1.Price.ToString()
                    },
                    Tax = new Money
                    {
                        CurrencyCode = currencyName,
                        Value = "0.00"
                    },
                    Quantity = itemAndQuantity[i].Item2.ToString(),
                    Category = "PHYSICAL_GOODS"
                };
                items.Add(item);
                decimal cost = itemAndQuantity[i].Item1.Price * itemAndQuantity[i].Item2;
                total += cost;
            }
            Console.WriteLine(total.ToString("0.00"));
            OrderRequest orderRequest = new OrderRequest()
            {
                CheckoutPaymentIntent = "CAPTURE",

                ApplicationContext = new ApplicationContext
                {
                    BrandName = "GameEx INC",
                    LandingPage = "BILLING",
                    CancelUrl = "https://localhost:5001/Cart/Index",
                    ReturnUrl = "https://localhost:5001/Cart/Index",
                    UserAction = "CONTINUE",
                    ShippingPreference = "SET_PROVIDED_ADDRESS"
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                    new PurchaseUnitRequest{
                        ReferenceId =  "PUHF",
                        Description = "Games pack",
                        CustomId = "Some quality games",
                        SoftDescriptor = "HighQualityGames",
                        AmountWithBreakdown = new AmountWithBreakdown
                        {
                            CurrencyCode = currencyName,
                            Value = total.ToString("0.00"),
                            AmountBreakdown = new AmountBreakdown
                            {
                                ItemTotal = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = total.ToString("0.00")
                                },
                                Shipping = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                Handling = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                TaxTotal = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                ShippingDiscount = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                }
                            }
                        },
                        Items = items,
                        ShippingDetail = new ShippingDetail
                        {
                            Name = new Name
                            {
                                FullName = "John Doe"
                            },
                            AddressPortable = new AddressPortable
                            {
                                AddressLine1 = "123 Townsend St",
                                AddressLine2 = "Floor 6",
                                AdminArea2 = "San Francisco",
                                AdminArea1 = "CA",
                                PostalCode = "94107",
                                CountryCode = "US"
                            }
                        }
                    }
                }
            };

            return orderRequest;
        }
        [Authorize(Roles = "Player")]
        public List<(InventoryItem,int)> GetCartItems()
        {
            var cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
            if (cart == null)
            {
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            List<(InventoryItem, int)> items = new List<(InventoryItem, int)>();

            //Dohvaćanje podataka o stavkama iz baze podataka na temelju podataka iz sessiona
            try
            {

                for (int i = 0; i < cart.Count(); i++)
                {
                    InventoryItem itemsInCart = _context.InventoryItem.AsNoTracking()
                        .Include(g => g.Game)
                        .Include(g => g.Renter).ThenInclude(g => g.User)
                        .Where(g => g.GameId == cart[i].Item1 && g.RenterId == cart[i].Item2).SingleOrDefault();
                    items.Add((itemsInCart, cart[i].Item3));
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
            return items;
        }
        [Authorize(Roles = "Player")]
        public IActionResult SuccessPage()
        {
            var cart = new List<(string, string, int)>();
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return View();
        }
    }
}
