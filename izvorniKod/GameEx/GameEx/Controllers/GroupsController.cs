﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using GameEx.ViewModels;
using cloudscribe.Pagination.Models;

namespace GameEx.Controllers
{
    [Authorize(Roles ="Player,Moderator,Administrator")]
    public class GroupsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public GroupsController(ApplicationDbContext context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index(string id,int page_number = 1, int page_size = 30, string query = "")
        {
            var skip = (page_number - 1) * page_size;
            var applicationDbContext = await _context.Group.Include(g => g.GroupMembers)
                .ThenInclude(gm => gm.Player)
                .ThenInclude(o => o.User)
                .Include(g => g.Game)
                .Where(g => g.Name.ToUpper().Contains(query.ToUpper())
                            || g.Game.Name.ToUpper().Contains(query.ToUpper())).ToListAsync();
            var data = applicationDbContext.Skip(skip).Take(page_size).ToList();
            ViewData["User"] = null;
            if (id != null && id == _userManager.GetUserId(User))
            {
                data = data.Where(g => g.GroupMembers.Any(g => g.PlayerId == id)).ToList();
                ViewData["User"] = await _userManager.GetUserAsync(User);
            }
            var result = new PagedResult<Group>
            {
                Data = data,
                PageNumber = page_number,
                PageSize = page_size,
                TotalItems = applicationDbContext.Count()
            };
            
            return View(result);
        }

        //Prikazuje individualnu grupu
        [Route("Group/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

           
            var group = await _context.Group.Include(g => g.GroupInvitations).ThenInclude(gi => gi.InvitedPlayer).ThenInclude(p => p.User)
                                                .Include(g => g.GroupMembers).ThenInclude(gm => gm.Player).ThenInclude(p => p.User)
                                                .Include(g => g.GroupMessages).ThenInclude(m => m.Sender)
                                                .Include(g => g.GroupJoinRequests).ThenInclude(gj => gj.Player).ThenInclude(p => p.User)
                                                .Include(g => g.Game)
                                                .Include(g => g.GroupAdvert)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (group == null)
            {
                return NotFound();
            }
            
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                    && g.GroupId == id).FirstOrDefaultAsync();
            var isMember = true;
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                group.GroupMessages.Clear();
                //group.GroupJoinRequests.Clear();
                group.GroupInvitations.Clear();
                isMember = false;
            }


            var currentUser = await _userManager.GetUserAsync(User);
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUserId = currentUser.Id;
                ViewBag.CurrentUserName = currentUser.UserName;
                ViewBag.GroupId = group.Id;
            }
            ViewData["IsMember"] = isMember;
            ViewData["MemberCount"] = _context.GroupMember.Where(g => g.GroupId == id).Count();
            ViewData["InvitedCount"] = _context.GroupInvitation.Where(g => g.GroupId == id).Count();
            return View(group);
        }


        //Stranica za stvaranje grupe
        public IActionResult Create()
        {
            return View();
        }

        //Validacija i spremanje novostvorene grupe u bazu podataka
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,GameId,Capacity,Description,LocationXCoordinate,LocationYCoordinate,LocationRadius")] Group group, [Bind("GameName")] string GameName)
        {
            if (group.GameId == null && GameName != null)
            {
                var gameId = _context.Game.Where(g => g.Name == GameName).Select(g => g.Id).ToList();
                if (gameId.Count() > 1 || gameId.Count < 1)
                {
                    ModelState.AddModelError("GameId", "There are multiple games with the same name or the game doesn't exist.");
                }
                else
                {
                    group.GameId = gameId[0];
                }
            }
            else if (group.GameId != null && GameName != null)
            {
                var game = _context.Game.Where(g => g.Id == group.GameId).ToList();
                if (game.Count() > 1 || game.Count < 1)
                {
                    ModelState.AddModelError("GameId", "Entered game doesn't exist.");
                }
                else
                {
                    if (game[0].Name == GameName)
                    {
                        group.GameId = game[0].Id;
                    }
                    else
                    {
                        var gameId = _context.Game.Where(g => g.Name == GameName).Select(g => g.Id).ToList();
                        if (gameId.Count() > 1 || gameId.Count < 1)
                        {
                            ModelState.AddModelError("GameId", "There are multiple games with the same name or the game doesn't exist.");
                        }
                        else
                        {
                            group.GameId = gameId[0];
                        }
                    }

                }
            }

            if (ModelState.IsValid)
            {
                group.Id = Guid.NewGuid().ToString();
                var userId = _userManager.GetUserAsync(User).Result.Id;
                _context.Add(group);
                _context.GroupMember.Add(new GroupMember
                {
                    GroupId = group.Id,
                    PlayerId = userId,
                    Owner = true
                });
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(AddMembers),new { id = group.Id});
            }
            return View(group);
        }

        //Uredivanje grupe
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                                    && g.Owner
                                                                    && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }

            var @group = await _context.Group.FindAsync(id);
            if (@group == null)
            {
                return NotFound();
            }
            return View(@group);
        }

        //Spremanje i validacija promijena nakon uređivanja grupe
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,GameId,Description,Name,TimeOfPlaying,Capacity,LocationXCoordinate,LocationYCoordinate,LocationRadius")] Group @group)
        {
            if (id != @group.Id)
            {
                return NotFound();
            }

            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                    && g.Owner    
                                                    && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //@group.OwnerId = _context.Group.Find(id).OwnerId;
                    _context.Update(@group);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupExists(@group.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details","Groups", new { Id = id});
            }
            return View(@group);
        }

        //Dohvat stranice za potvrdivanja brisanja grupe
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                                && g.Owner                                                        
                                                                && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return Unauthorized();
            }

            var @group = await _context.Group.Include(g => g.GroupMembers).ThenInclude(g => g.Player).ThenInclude(g => g.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@group == null)
            {
                return NotFound();
            }

            return View(@group);
        }

        //Brisanje grupe i povezanih podataka iz baze podataka
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {

            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                        && g.Owner
                                                        && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }
            var @group = await _context.Group.FindAsync(id);
            _context.Group.Remove(@group);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //Provjera postoji li uopce grupa prije brisanja grupe
        private bool GroupExists(string id)
        {
            return _context.Group.Any(e => e.Id == id);
        }

        //Stranica za dodavanje clanova grupe
        [Route("Group/{id}/AddMembers")]
        public async Task<IActionResult> AddMembersAsync(string id)
        {
            var user = await _userManager.GetUserAsync(User);


            ViewData["GroupId"] = id;
            var group = _context.Group.Include(g => g.GroupMembers)
                .Include(g => g.GroupInvitations).Where(g => g.Id == id).FirstOrDefault();

            bool found = false;
            foreach(var member in group.GroupMembers)
            {
                if(member.PlayerId == user.Id && member.Owner)
                {
                    found = true;
                }
            }

            if(group.Capacity == 1)
            {
                return RedirectToAction("Details", "Groups", new { Id = group.Id });
            }
            if (found)
            {
                return View(group);
            }
            else
            {
                return NotFound();
            }
        }

        //Stranica za spremanje i validaciju novododanih članova
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Group/{id}/AddMembers")]
        public async Task<IActionResult> AddMembers(List<GroupInvitation> groupInvitations, string id)
        {
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                        && g.Owner
                                                        && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }
            var group = _context.Group.Where(g => g.Id == id).FirstOrDefault();
            if (ModelState.IsValid)
            {

                foreach (var invitation in groupInvitations)
                {
                    if(invitation.InvitedPlayerId != null)
                    {
                        invitation.Id = Guid.NewGuid().ToString();
                        invitation.GroupId = id;
                        invitation.InvitedPlayerId = (await _userManager.FindByNameAsync(invitation.InvitedPlayerId)).Id;
                    }
                    else
                    {
                        ModelState.AddModelError("GroupInvitations[" + groupInvitations.IndexOf(invitation) + "].InvitedPlayerId", "Please enter username");
                    }
                }
                if (ModelState.IsValid)
                {
                    var results = groupInvitations.Where(i => i.InvitedPlayerId != null).ToHashSet();
                    _context.GroupInvitation.AddRange(results);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Details), new { id = group.Id });
                }
            }
            return View(group);
        }

        //Funkcija koja vraća partial s novim input formom za dodavanje igraca u grupu
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMember([Bind("Id,GroupInvitations,Capacity")]Group group)
        {
            var members = _context.GroupMember.Where(g => g.GroupId == group.Id).Count();
            if (group.Capacity > members)
            {
                group.GroupInvitations.Add(new GroupInvitation());
            }
            return PartialView("GroupInvitations", group);
        }

        private const int BATCH_SIZE = 10;
        public async Task<IActionResult> Chat(string id)
        {
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                        && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }
            var group = _context.Group.Where(g => g.Id == id).FirstOrDefault();
            if (group == null)
            {
                return NotFound();
            }
            var currentUser = await _userManager.GetUserAsync(User);
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.CurrentUserId = currentUser.Id;
                ViewBag.CurrentUserName = currentUser.UserName;
                ViewBag.GroupId = group.Id;
            }
            return View();
        }

        [HttpPost]
        public IActionResult _MessagePartial(string id, string sortOrder, string searchString, int firstItem = 0)
        {
            List<GroupMessage> data = _context.GroupMessage.Include(gm => gm.Sender).ThenInclude(p => p.User).OrderByDescending(p => p.When)
                .Where(g => g.GroupId == id).ToList();

            var query = data;
            

            // Extract a portion of data
            var model = query.Skip(firstItem).Take(BATCH_SIZE).Reverse().ToList();
            if (model.Count() == 0) return StatusCode(204);  // 204 := "No Content"
            return PartialView(model);
        }


        public async Task<IActionResult> PlayingDates(string id)
        {
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                        && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }
            if (id == null)
            {
                return NotFound();
            }
            var group = _context.Group.Where(g => g.Id == id).FirstOrDefault();
            if (group == null)
            {
                return NotFound();
            }
            ViewData["GroupId"] = group.Id;
            
            return View();
        }


        [Route("Group/{id}/AddReviews")]
        public async Task<IActionResult> AddReviews(string id)
        {
            var membership = await _context.GroupMember.Where(g => g.PlayerId == _userManager.GetUserId(User)
                                                        && g.GroupId == id).FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(await _userManager.GetUserAsync(User));
            if (membership == null && !roles.Contains("Adminstrator") && !roles.Contains("Moderator"))
            {
                return NotFound();
            }
            ViewData["GroupId"] = id;
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var groupMembers = _context.GroupMember
                .Include(i => i.Player).ThenInclude(i => i.User)
                .Where(i => i.GroupId == id).ToList();

            var curGroupReviewsByPlayer = _context.Review
                .Include(i => i.ReviewedPlayer)
                .Where(i => i.GroupId == id && i.ReviewerId == userId).ToList();

            List<string> reviewedPlayerIds = new List<string>();
            foreach(Review r in curGroupReviewsByPlayer)
            {
                reviewedPlayerIds.Add(r.ReviewedPlayerId);
            }

            List<ReviewViewModel> newReviews = new List<ReviewViewModel>();
            for (int i = 0; i < groupMembers.Count(); i++)
            {
                ReviewViewModel rvm = new ReviewViewModel();
                //provjeri ako je to trenutačni igrač i preskoči
                if (groupMembers[i].PlayerId == userId) continue;
                if (!reviewedPlayerIds.Contains(groupMembers[i].PlayerId))
                {

                    var getPlayerAndReview = _context.Player
                        .Include(d => d.User)
                        .Where(d => d.Id == groupMembers[i].PlayerId).SingleOrDefault();

                    rvm.review = new Review
                    {
                        GroupId = id,
                        ReviewerId = userId,
                        ReviewedPlayerId = groupMembers[i].PlayerId,
                        ReviewedPlayer = getPlayerAndReview
                    };
                    rvm.canEdit = true;
                }
                else
                {
                    for (int j = 0; j < curGroupReviewsByPlayer.Count(); j++)
                    {
                        if(curGroupReviewsByPlayer[j].ReviewedPlayerId == groupMembers[i].PlayerId)
                        {
                            rvm.review = curGroupReviewsByPlayer[j];
                            rvm.canEdit = false;
                            break;
                        }
                    }
                }
                newReviews.Add(rvm);
            }
            ReviewViewModelList rvml = new ReviewViewModelList();
            rvml.reviewViewModels = newReviews;

            return View(rvml);
        }

        //Stranica za spremanje i validaciju novododanih članova
        [HttpPost]
        [Route("Group/{id}/AddReviews")]
        public void AddReviews([FromBody] ReviewPostModel reviewPostModel)
        {
            try
            {
                string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                /*
                Console.WriteLine("cur user " + userId);
                Console.WriteLine("groupId " + reviewPostModel.groupId);
                Console.WriteLine("reviewedPlayerId" + reviewPostModel.reviewedPlayerId);
                Console.WriteLine("rating" + reviewPostModel.rating);
                Console.WriteLine("reviewText " + reviewPostModel.reviewText);*/

                string id = reviewPostModel.groupId;
                var group = _context.Group.Where(g => g.Id == id).FirstOrDefault();

                var date = DateTime.Now;

                Review newReview = new Review
                {
                    GroupId = reviewPostModel.groupId,
                    ReviewerId = userId,
                    ReviewedPlayerId = reviewPostModel.reviewedPlayerId,
                    TimeWritten = date,
                    Rating = reviewPostModel.rating,
                    Text = reviewPostModel.reviewText
                };

                _context.Review.Add(newReview);
                _context.SaveChanges();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
            }
        }

        public async Task<IActionResult> Leave(string id)
        {
            var user = await _userManager.GetUserAsync(User);
            var membership = _context.GroupMember.Where(m => m.PlayerId == user.Id && m.GroupId == id).FirstOrDefault();
            var members = _context.GroupMember.Where(m => m.GroupId == id && !m.Owner).ToList();
            var group = _context.Group.Where(g => g.Id == id).FirstOrDefault();
            if(membership != null && group != null)
            {
                if (membership.Owner)
                {
                    if(members.Count() == 0)
                    {
                        _context.Remove(membership);
                        var invitations = _context.GroupInvitation.Where(i => i.GroupId == id).ToList();
                        var messages = _context.GroupMessage.Where(m => m.GroupId == id).ToList();
                        var groupAdverts = _context.GroupAdvert.Where(m => m.GroupId == id).ToList();
                        var joinRequests = _context.GroupJoinRequest.Where(g => g.GroupId == id).ToList();
                        _context.RemoveRange(joinRequests);
                        _context.RemoveRange(groupAdverts);
                        _context.RemoveRange(invitations);
                        _context.RemoveRange(messages);
                        _context.Remove(group);
                    }
                    else
                    {
                        members[0].Owner = true;
                        _context.Update(members[0]);
                        _context.Remove(membership);
                    }
                }
                
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Kick(string id, string playerId)
        {
            var user = await _userManager.GetUserAsync(User);
            var membership = _context.GroupMember.Where(m => m.PlayerId == user.Id && m.GroupId == id).FirstOrDefault();
            if(membership == null || !membership.Owner)
            {
                return NotFound();
            }
            var player = _context.GroupMember.Where(m => m.PlayerId == playerId && m.GroupId == id).FirstOrDefault();
            if(player == null)
            {
                return NotFound();
            }
            _context.Remove(player);
            _context.SaveChanges();

            return RedirectToAction("Details", "Groups", new {Id = id});
        }

        [HttpPost]
        public IActionResult SetPlayingDate(string Id, DateTime TimeOfPlaying)
        {
            var membership = _context.GroupMember
                .Where(g => g.GroupId == Id && g.PlayerId == _userManager.GetUserId(User)).FirstOrDefault();
            if (membership == null || !membership.Owner)
            {
                return NotFound();
            }
            var group = _context.Group.Where(g => g.Id == Id).FirstOrDefault();
            if (group == null)
            {
                return NotFound();
            }
            group.TimeOfPlaying = TimeOfPlaying;
            _context.Group.Update(group);
            _context.SaveChanges();
            return RedirectToAction("Details", "Groups", new { Id = Id });
        }
    }
}
