﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Session;
using GameEx.ViewModels;
using cloudscribe.Pagination.Models;

namespace GameEx.Controllers
{
    [Authorize]
    public class RenterController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public RenterController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Renter
        public async Task<IActionResult> Index(int pageNumber = 1, int pageSize = 15)
        {
            int exclude = (pageNumber - 1) * pageSize;

            var renters = await  _context.Renter.Include(r => r.User).Where(r => r.ApprovedByAdmin).Skip(exclude).Take(pageSize).ToListAsync();
            var model = new List<RenterViewModel>();

            foreach(var r in renters)
            {
                var renter = new RenterViewModel
                {
                    Id = r.Id,
                    CompanyName = r.CompanyName,
                    WebAddress = r.WebAddress,
                    Address = r.Address,
                    PhoneNumber = r.PhoneNumber,
                    OIB = r.OIB,
                    User = r.User
                };

                model.Add(renter);

            }

            var result = new PagedResult<RenterViewModel>
            {
                Data = model,
                TotalItems = _context.Renter.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize,
            };
            return View(result);
        }

        // GET: Renter/Details/5
        public async Task<IActionResult> Details(string id, int pageNumber = 1, int pageSize = 20)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["userId"] = _userManager.GetUserId(User);

            var renter = await _context.Renter
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            var inventoryDb = _context.InventoryItem.Include(i => i.Game).Include(i => i.Rentals);

            ViewBag.TotalItems = inventoryDb.Count();
            ViewBag.PageSize = pageSize;
            ViewBag.PageNumber = pageNumber;

            var skip = (pageNumber - 1) * pageSize;

            renter.InventoryItems = inventoryDb.Where(ii => ii.RenterId == id)
                .Skip(skip)
                .Take(pageSize).ToList();

            if (renter == null)
            {
                return NotFound();
            }

            return View(renter);
        }

        // GET: Renter/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            String currentUserId = _userManager.GetUserAsync(User).Result.Id;
            if (id != currentUserId)
            {
                return new ForbidResult();
            }

            if (id == null)
            {
                return NotFound();
            }

            var renter = await _context.Renter.FindAsync(id);
            if (renter == null)
            {
                return NotFound();
            }
            ViewData["Id"] = new SelectList(_context.Set<User>(), "Id", "Id", renter.Id);
            ViewBag.prevPage = Request.Headers["Referer"].ToString();
            return View(renter);
        }

        // POST: Renter/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,CompanyName,Address,PhoneNumber,WebAddress,OIB,ApprovedByAdmin")] Renter renter)
        {
            if (id != renter.Id)
            {
                return new ForbidResult();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(renter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RenterExists(renter.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id }); ;
            }
            ViewData["Id"] = new SelectList(_context.Set<User>(), "Id", "Id", renter.Id);
            return View(renter);
        }

        // GET: Renter/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            String currentUserId = _userManager.GetUserAsync(User).Result.Id;
            var user = await _userManager.GetUserAsync(User);
            ViewData["userId"] = _userManager.GetUserId(User);
            ViewBag.prevPage = Request.Headers["Referer"].ToString();

            if ((id != currentUserId) && !(await _userManager.IsInRoleAsync(user, "Administrator")) && !(await _userManager.IsInRoleAsync(user, "Moderator")))
            {
                return new ForbidResult();
            }

            if (id == null)
            {
                return NotFound();
            }

            var renter = await _context.Renter
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (renter == null)
            {
                return NotFound();
            }

            return View(renter);
        }

        // POST: Renter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var renter = await _context.Renter.FindAsync(id);
            var user = await _userManager.FindByIdAsync(id);
            var rentals = _context.Rental.Where(r => r.RenterId == id);
            String currentUserId = _userManager.GetUserAsync(User).Result.Id;

            _context.Rental.RemoveRange(rentals);
            _context.Renter.Remove(renter);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            if (id == currentUserId) // ako korisnik briše svoj profil, potrebno ga je odlogirati
            {
                SignOut();
                if (HttpContext.Request.Cookies.Count > 0)
                {
                    var siteCookies = HttpContext.Request.Cookies.Where(c => c.Key.Contains(".AspNetCore.") || c.Key.Contains("Microsoft.Authentication"));
                    foreach (var cookie in siteCookies)
                    {
                        Response.Cookies.Delete(cookie.Key);
                    }
                }
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return RedirectToAction(nameof(Index));
        }

        private bool RenterExists(string id)
        {
            return _context.Renter.Any(e => e.Id == id);
        }

    }
}
