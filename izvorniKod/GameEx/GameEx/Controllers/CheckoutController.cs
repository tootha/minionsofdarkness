﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

using PayPalCheckoutSdk.Orders;
using GameEx.PayPal;
using GameEx.Models;
using System.Collections.Generic;
using GameEx.Helpers;
using System.Linq;
using System;
using GameEx.Data;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace GameEx.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public CheckoutController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _userManager = userManager;
            _context = context;
        }
        /// <summary>
        /// This action is called when the user clicks on the PayPal button.
        /// </summary>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Player")]
        [Route("api/paypal/checkout/order/create")]
        public async Task<PayPal.SmartButtonHttpResponse> Create()
        {
            var cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
            if (cart == null)
            {
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            List<(InventoryItem, int)> items = new List<(InventoryItem, int)>();
            
            //Dohvaćanje podataka o stavkama iz baze podataka na temelju podataka iz sessiona
            try
            {

                for (int i = 0; i < cart.Count(); i++)
                {
                    InventoryItem itemsInCart = _context.InventoryItem.AsNoTracking()
                        .Include(g => g.Game)
                        .Include(g => g.Renter).ThenInclude(g => g.User)
                        .Where(g => g.GameId == cart[i].Item1 && g.RenterId == cart[i].Item2).SingleOrDefault();
                    items.Add((itemsInCart, cart[i].Item3));
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
            var request = new PayPalCheckoutSdk.Orders.OrdersCreateRequest();
            
            request.Prefer("return=representation");
            request.RequestBody(OrderBuilder.Build(items,"USD"));

            // Call PayPal to set up a transaction
            var response = await GameEx.Paypal.PayPalClient.client().Execute(request);

            // Create a response, with an order id.
            var result = response.Result<PayPalCheckoutSdk.Orders.Order>();
            var payPalHttpResponse = new PayPal.SmartButtonHttpResponse(response)
            {
                orderID = result.Id
            };
            return payPalHttpResponse;
        }

        /// <summary>
        /// This action is called once the PayPal transaction is approved
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Player")]
        [Route("api/paypal/checkout/order/approved/{orderId}")]
        public IActionResult Approved(string orderId)
        {
            Console.WriteLine("approved checkout");
            return Ok();
        }

        /// <summary>
        /// This action is called once the PayPal transaction is complete
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Player")]
        [Route("api/paypal/checkout/order/complete/{orderId}")]
        public IActionResult Complete(string orderId)
        {
            // 1. Update the database.
            // 2. Complete the order process. Create and send invoices etc.
            // 3. Complete the shipping process.
            //game id, renter id, quantity
            var cart = SessionHelper.GetObjectFromJson<List<(string, string, int)>>(HttpContext.Session, "cart");
            if (cart == null)
            {
                cart = new List<(string, string, int)>();
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            List<(InventoryItem, int)> items = new List<(InventoryItem, int)>();

            //Dohvaćanje podataka o stavkama iz baze podataka na temelju podataka iz sessiona
            try
            {

                for (int i = 0; i < cart.Count(); i++)
                {
                    InventoryItem itemsInCart = _context.InventoryItem.AsNoTracking()
                        .Include(g => g.Game)
                        .Include(g => g.Renter).ThenInclude(g => g.User)
                        .Where(g => g.GameId == cart[i].Item1 && g.RenterId == cart[i].Item2).SingleOrDefault();
                    items.Add((itemsInCart, cart[i].Item3));
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
            try
            {
                for (int i = 0; i < items.Count(); i++)
                {
                    for (int j = 0; j < items[i].Item2; j++)
                    {
                        _context.Add(new Rental
                        {
                            DateOfRent = DateTime.Now,
                            GameId = items[i].Item1.GameId,
                            Id = Guid.NewGuid().ToString(),
                            PlayerId = _userManager.GetUserId(User),
                            IsReturned = false,
                            RenterId = items[i].Item1.RenterId,

                        });

                    }
                }
                _context.SaveChanges();
                return RedirectToAction("SuccessPage", "Cart");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
            return RedirectToAction("SuccessPage", "Cart");
        }

        /// <summary>
        /// This action is called once the PayPal transaction is complete
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Player")]
        [Route("api/paypal/checkout/order/cancel/{orderId}")]
        public IActionResult Cancel(string orderId)
        {
            Console.WriteLine("cancel checkout");
            // 1. Remove the orderId from the database.
            return Ok();
        }

        /// <summary>
        /// This action is called once the PayPal transaction is complete
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Player")]
        [Route("api/paypal/checkout/order/error/{orderId}/{error}")]
        public IActionResult Error(string orderId, string error)
        {
            Console.WriteLine("error checkout");

            // Log the error.
            // Notify the user.
            return NoContent();
        }


    }
}