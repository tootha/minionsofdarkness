﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Controllers
{
    public class UnconfirmedRentersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        public UnconfirmedRentersController(ApplicationDbContext context, RoleManager<IdentityRole> roleManager,UserManager<User> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Index(int page_size = 30, int page_num = 1, string query = "")
        {
            var skip = (page_num - 1) * page_size;
            List<Renter> unconfirmedRenters = _context.Renter.Include(r => r.User).Where(r => r.ApprovedByAdmin == false).ToList();
            var result = new PagedResult<Renter>
            {
                Data = unconfirmedRenters.Where(r => r.User.UserName.ToUpper().Contains(query.ToUpper()))
                    .Skip(skip).Take(page_size).ToList(),
                PageSize = page_size,
                PageNumber = page_num,
                TotalItems = unconfirmedRenters.Count()
            };
            return View(result);
        }

        [Authorize(Roles ="Administrator")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                User user = await _userManager.FindByIdAsync(id);
                await _userManager.DeleteAsync(user);
            } catch(Exception exc)
            {
                ViewData["ExceptionMessage"] = exc.Message;
            }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Approve(string id)
        {
            try
            {
                Renter user = _context.Renter.Find(id);
                if(user != null)
                {
                    user.ApprovedByAdmin = true;
                    _context.Renter.Update(user);
                    _context.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                ViewData["ExceptionMessage"] = exc.Message;
            }
            return RedirectToAction("Index");
        }
    }
}
