﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using GameEx.ViewModels;
using Microsoft.AspNetCore.Authorization;
using cloudscribe.Pagination.Models;

namespace GameEx.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserController(ApplicationDbContext context, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index(int pageNumber=1, int pageSize=10)
        {
            var userCurrent = await _userManager.GetUserAsync(User);
            if (userCurrent == null)
            {
                return NotFound();
            }
            var userRole = await _userManager.GetRolesAsync(userCurrent);

            var role = await _roleManager.FindByNameAsync("Administrator");
            ViewData["adminCount"] = _context.UserRoles.Where(x => x.RoleId == role.Id).Count();
            ViewData["userId"] = userCurrent.Id;
            ViewData["userRole"] = userRole[0];

            int exclude = (pageNumber - 1) * pageSize;

            var users = await _userManager.Users.Skip(exclude).Take(pageSize).ToListAsync();
            var model = new List<UserViewModel>();

            foreach(var u in users)
            {
                var user = new UserViewModel
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email,
                    EmailConfirmed = u.EmailConfirmed,
                    PhoneNumber = u.PhoneNumber,
                    PhoneConfirmed = u.PhoneNumberConfirmed,
                    Role = await _userManager.GetRolesAsync(u),
                };

                model.Add(user);
            }

            var result = new PagedResult<UserViewModel>
            {
                Data = model,
                TotalItems = _userManager.Users.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize,
            };
            return View(result);
        }

        [HttpGet]
        public async Task<IActionResult> Role(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var model = new List<UserRoleViewModel>();
            var roleList = await _roleManager.Roles.ToListAsync();
            foreach(var role in roleList)
            {
                var userRole = new UserRoleViewModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };

                if(await _userManager.IsInRoleAsync(user, role.Name))
                {
                    userRole.IsSelected = true;
                }
                else
                {
                    userRole.IsSelected = false;
                }

                model.Add(userRole);
            }

            ViewBag.userName = user.UserName;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Role(List<UserRoleViewModel> model, string id)
        {
            
            var user = await _userManager.FindByIdAsync(id);
            if(user == null)
            {
                return NotFound();
            }

            var roles = await _userManager.GetRolesAsync(user);
            var result = await _userManager.RemoveFromRolesAsync(user, roles);

            if(!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot remove user's existing roles.");
                return View(model);
            }

            result = await _userManager.AddToRolesAsync(user, model.Where(x => x.IsSelected).Select(n => n.RoleName));

            if(!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot add selected roles to user.");
                return View(model);
            }

            return RedirectToAction("Edit","User",new { id = id});
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var getUser = await _userManager.FindByIdAsync(Id);
            
            if(getUser == null)
            {
                return NotFound();
            }

            var user = new UserViewModel
            {
                Id = getUser.Id,
                UserName = getUser.UserName,
                Email = getUser.Email,
                EmailConfirmed = getUser.EmailConfirmed,
                PhoneNumber = getUser.PhoneNumber,
                PhoneConfirmed = getUser.PhoneNumberConfirmed,
                Role = await _userManager.GetRolesAsync(getUser),
            };

            var roleViewModel = new List<UserRoleViewModel>();
            var roleList = await _roleManager.Roles.ToListAsync();
            foreach (var role in roleList)
            {
                var userRole = new UserRoleViewModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };

                if (await _userManager.IsInRoleAsync(getUser, role.Name))
                {
                    userRole.IsSelected = true;
                }
                else
                {
                    userRole.IsSelected = false;
                }

                roleViewModel.Add(userRole);
            }
            ViewBag.RoleData = roleViewModel;
            ViewBag.userName = user.UserName;

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserViewModel user)
        {
            var getUser = await _userManager.FindByIdAsync(user.Id);

            if (getUser == null)
            {
                return NotFound();
            }

            getUser.UserName = user.UserName;
            getUser.NormalizedEmail = user.Email;
            getUser.EmailConfirmed = user.EmailConfirmed;
            getUser.PhoneNumber = user.PhoneNumber;
            getUser.PhoneNumberConfirmed = user.PhoneConfirmed;

            try
            {
                await _userManager.UpdateAsync(getUser);
                return RedirectToAction("Edit", "User", new { id = user.Id });
            }
            catch(Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                return View(user);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string Id)
        {
            var getUser = await _userManager.FindByIdAsync(Id);


            if (getUser == null)
            {
                return NotFound();
            }

            var user = new UserViewModel
            {
                Id = getUser.Id,
                UserName = getUser.UserName,
                Email = getUser.Email,
                EmailConfirmed = getUser.EmailConfirmed,
                PhoneNumber = getUser.PhoneNumber,
                PhoneConfirmed = getUser.PhoneNumberConfirmed,
                Role = await _userManager.GetRolesAsync(getUser),
            };

            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string Id)
        {
            var getUser = await _userManager.FindByIdAsync(Id);

            if (getUser == null)
            {
                return NotFound();
            }

            var userRole = await _userManager.GetRolesAsync(getUser);

            try
            {
                if(userRole[0] == "Player")
                {
                    var player = await _context.Player.FindAsync(Id);
                    var rentals = _context.Rental.Where(r => r.PlayerId == Id);
                    _context.Rental.RemoveRange(rentals);
                    _context.Player.Remove(player);
                } 
                else if (userRole[0] == "Renter")
                {
                    var renter = await _context.Renter.FindAsync(Id);
                    var rentals = _context.Rental.Where(r => r.RenterId == Id);
                    _context.Rental.RemoveRange(rentals);
                    _context.Renter.Remove(renter);
                }

                await _context.SaveChangesAsync();
                await _userManager.DeleteAsync(getUser);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
