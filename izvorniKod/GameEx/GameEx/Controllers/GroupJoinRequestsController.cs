﻿using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Controllers
{
    [Authorize(Roles="Moderator,Administrator,Player")]
    public class GroupJoinRequestsController:Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public GroupJoinRequestsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Create(string id, string playerId)
        {
            var membership = _context.GroupMember.Where(g => g.GroupId == id && g.PlayerId == playerId).FirstOrDefault();
            if(membership == null)
            {
                var request = _context.GroupJoinRequest.Where(g => g.PlayerId == playerId && g.GroupId == id).FirstOrDefault();
                if (request == null)
                {
                    _context.GroupJoinRequest.Add(new GroupJoinRequest
                    {
                        GroupId = id,
                        PlayerId = playerId,
                        Id = Guid.NewGuid().ToString()
                    });
                    _context.SaveChanges();
                }
            }

            return RedirectToAction("Details", "Groups", new { Id = id });
        }

        public IActionResult Accept(string id, string playerId)
        {
            var user = _userManager.GetUserId(User);
            var membership = _context.GroupMember.Where(g => g.PlayerId == user).FirstOrDefault();

            var request = _context.GroupJoinRequest.Where(r => r.GroupId == id && r.PlayerId == playerId).FirstOrDefault();
            var invitations = _context.GroupInvitation.Where(i => i.GroupId == id && i.InvitedPlayerId == playerId).ToList();

            if (request != null && membership != null && membership.Owner)
            {
                _context.GroupMember.Add(new GroupMember
                {
                    GroupId = id,
                    PlayerId = playerId,
                    Owner = false,
                    
                });
                if(invitations != null)
                {
                    _context.RemoveRange(invitations);
                }
                _context.GroupJoinRequest.Remove(request);
                _context.SaveChanges();
                return RedirectToAction("Details", "Groups", new { Id = id });
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Decline(string id, string playerId)
        {
            var user = _userManager.GetUserId(User);
            var membership = _context.GroupMember.Where(g => g.PlayerId == user).FirstOrDefault();

            var request = _context.GroupJoinRequest.Where(r => r.GroupId == id && r.PlayerId == playerId).FirstOrDefault();
            if(request != null && membership != null && membership.Owner)
            {
                _context.GroupJoinRequest.Remove(request);
                _context.SaveChanges();
                return RedirectToAction("Details", "Groups", new { Id = id });
            }
            else
            {
                return NotFound();
            }
            
        }
    }
}
