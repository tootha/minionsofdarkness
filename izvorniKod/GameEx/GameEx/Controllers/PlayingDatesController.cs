﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Controllers
{
    public class PlayingDatesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public PlayingDatesController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        public List<Event> GetGroupDates(string id)
        {
            var data = _context.PlayingDates
                .Include(p => p.Player)
                .ThenInclude(p => p.User)
                .Where(pd => pd.GroupId == id)
            .Select(pd => new Event {
                id = pd.GroupId,
                start = pd.Start,
                end = pd.End,
                title = pd.Player.User.UserName
            }).ToList();
            return data;
        }
        
        [HttpPost]
        public IActionResult AddEvent(Event ev)
        {

            var evdb = new PlayerPlayingDates
            {
                Id = Guid.NewGuid().ToString(),

                Start = ev.start,
                GroupId = ev.id,
                PlayerId = _userManager.GetUserId(User),
                End = new DateTime(0)
            };  
            _context.PlayingDates.Add(evdb);
            _context.SaveChanges();
            return Ok();
        }
        
        [HttpPost]
        public IActionResult UpdateEvent(Event ev)
        {
            var evdb = _context.PlayingDates.Where(p => p.GroupId == ev.idOld
            && p.Start == ev.startOld
            && p.End == ev.endOld
            && p.PlayerId == _userManager.GetUserId(User))
                .FirstOrDefault();
            if (evdb != null)
            {
                evdb.Start = ev.start;
                if (ev.end != null)
                {
                    evdb.End = ev.end;
                }
                _context.Update(evdb);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpPost]
        public IActionResult DeleteEvent(Event ev)
        {
            var evdb = _context.PlayingDates.Where(p => p.GroupId == ev.id && p.Start == ev.start && p.End == ev.end && p.PlayerId == _userManager.GetUserId(User)).FirstOrDefault();
            _context.PlayingDates.Remove(evdb);
            _context.SaveChanges();
            return Ok();
        }
    }

    public class Event
    {
        public string id { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string title { get; set; }
        public string idOld { get; set; }
        public DateTime startOld { get; set; }
        public DateTime endOld { get; set; }
        public string titleOld { get; set; }
    }

}
