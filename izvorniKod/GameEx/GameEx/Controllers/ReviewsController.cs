﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;

namespace GameEx.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReviewsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Reviews/Edit/5
        public async Task<IActionResult> Edit(string ReviewedPlayerId, string ReviewerId, string GroupId, string TimeWritten)
        {
            DateTime time = DateTime.ParseExact(TimeWritten, "dd.MM.yyyy hh:mm:ss",null);
            if (GroupId == null ||ReviewedPlayerId == null || ReviewerId == null || TimeWritten == null)
            {
                return NotFound();
            }

            var reviews = await _context.Review.Where(r => r.ReviewedPlayerId == ReviewedPlayerId && r.ReviewerId == ReviewerId
                                                && r.GroupId == GroupId).ToListAsync();
            Review review = null;
            foreach(var rev in reviews)
            {
                var timeRev = DateTime.ParseExact(rev.TimeWritten.ToString(), "dd.MM.yyyy hh:mm:ss", null);
                if (DateTime.Compare(timeRev, time) == 0)
                {
                    review = rev;
                    break;
                }
            }
            if (reviews == null)
            {
                return NotFound();
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", review.GroupId);
            ViewData["ReviewedPlayerId"] = new SelectList(_context.Player, "Id", "Id", review.ReviewedPlayerId);
            ViewData["ReviewerId"] = new SelectList(_context.Player, "Id", "Id", review.ReviewerId);
            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("GroupId,ReviewerId,ReviewedPlayerId,TimeWritten,Rating,Text")] Review review)
        {
            if (id != review.GroupId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(review);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReviewExists(review.GroupId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupId"] = new SelectList(_context.Group, "Id", "Id", review.GroupId);
            ViewData["ReviewedPlayerId"] = new SelectList(_context.Player, "Id", "Id", review.ReviewedPlayerId);
            ViewData["ReviewerId"] = new SelectList(_context.Player, "Id", "Id", review.ReviewerId);
            return View(review);
        }

        // GET: Reviews/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review
                .Include(r => r.Group)
                .Include(r => r.ReviewedPlayer)
                .Include(r => r.Reviewer)
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var review = await _context.Review.FindAsync(id);
            _context.Review.Remove(review);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReviewExists(string id)
        {
            return _context.Review.Any(e => e.GroupId == id);
        }
    }
}
