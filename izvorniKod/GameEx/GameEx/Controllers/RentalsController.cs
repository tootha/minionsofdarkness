﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GameEx.Data;
using GameEx.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using GameEx.ViewModels;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;

namespace GameEx.Controllers
{
    [Authorize]
    public class RentalsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public RentalsController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Rentals
        public async Task<IActionResult> Index(int pageNumber=1, int pageSize=20)
        {
            int exclude = (pageNumber - 1) * pageSize;
            var user = await _userManager.GetUserAsync(User);

            var rent = await _context.Rental.Include(g => g.InventoryItem).ThenInclude(g => g.Game).OrderByDescending(o => o.DateOfRent)
               .Where(p => (p.PlayerId == user.Id || p.RenterId == user.Id) && p.IsReturned == false)
               .Skip(exclude).Take(pageSize).ToListAsync();

            var model = new List<RentalViewModel>();
            foreach (var r in rent)
            {
                var rentModel = new RentalViewModel
                {
                    GameId = r.GameId,
                    InventoryItem = r.InventoryItem,
                    IsReturned = r.IsReturned,
                    DateOfRent = r.DateOfRent,
                    DateOfReturn = r.DateOfReturn,
                    Player = r.Player,
                    PlayerId = r.PlayerId,
                    RenterId = r.RenterId,
                    Id = r.Id,
                };

                model.Add(rentModel);
            }
            var result = new PagedResult<RentalViewModel>
            {
                Data = model,
                TotalItems = _context.Rental.Include(g => g.InventoryItem).ThenInclude(g => g.Game)
                            .Where(p => (p.PlayerId == user.Id || p.RenterId == user.Id) && p.IsReturned == false).Count(),
                PageNumber = pageNumber,
                PageSize = pageSize,
            };
            ViewData["User"] = await _userManager.GetUserAsync(User);
            if (await _userManager.IsInRoleAsync(user, "Player"))
            {
                ViewData["Role"] = "Player";
            }
            else if (await _userManager.IsInRoleAsync(user, "Renter"))
            {
                ViewData["Role"] = "Renter";
            }
            return View(result);
        }

        public async Task<IActionResult> History(int pageNumber=1, int pageSize=20)
        {
            int exclude = (pageNumber - 1) * pageSize;
            var user = await _userManager.GetUserAsync(User);

            var rent = await _context.Rental.Include(g => g.InventoryItem).ThenInclude(g => g.Game).OrderByDescending(o => o.DateOfRent)
               .Where(p => p.PlayerId == user.Id || p.RenterId == user.Id)
               .Skip(exclude).Take(pageSize).ToListAsync();

            var model = new List<RentalViewModel>();
            foreach (var r in rent)
            {
                var rentModel = new RentalViewModel
                {
                    GameId = r.GameId,
                    InventoryItem = r.InventoryItem,
                    IsReturned = r.IsReturned,
                    DateOfRent = r.DateOfRent,
                    DateOfReturn = r.DateOfReturn,
                    Player = r.Player,
                    PlayerId = r.PlayerId,
                    RenterId = r.RenterId,
                    Id = r.Id,
                };

                model.Add(rentModel);
            }
            var result = new PagedResult<RentalViewModel>
            {
                Data = model,
                TotalItems = _context.Rental.Include(g => g.InventoryItem).ThenInclude(g => g.Game)
                            .Where(p => p.PlayerId == user.Id || p.RenterId == user.Id).Count(),
                PageNumber = pageNumber,
                PageSize = pageSize,
            };
            return View(result);
        }

        // GET: Rentals/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewBag.prevPage = Request.Headers["Referer"].ToString();

            var rental = await _context.Rental
                .Include(r => r.InventoryItem).ThenInclude(g => g.Game)
                .Include(r => r.Player)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (rental == null || _userManager.GetUserId(User) != rental.InventoryItem.RenterId 
                && await _userManager.IsInRoleAsync(await _userManager.GetUserAsync(User),"Renter"))
            {
                return NotFound();
            }

            var model = new RentalDetailsViewModel
            {
                Id = rental.Id,
                Player = await _userManager.FindByIdAsync(rental.PlayerId),
                Renter = await _context.Renter.FirstOrDefaultAsync(m => m.Id == rental.RenterId),
                InventoryItem = rental.InventoryItem,
                IsReturned = rental.IsReturned,
                DateOfRent = rental.DateOfRent,
                DateOfReturn = rental.DateOfReturn
            };


            return View(model);
        }

        // GET: Rentals/Create
        public IActionResult Create()
        {
            ViewData["GameId"] = new SelectList(_context.Set<InventoryItem>(), "RenterId", "RenterId");
            ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id");
            return View();
        }

        // POST: Rentals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlayerId,RenterId,GameId,IsReturned,DateOfRent,DateOfReturn")] Rental rental)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rental);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GameId"] = new SelectList(_context.Set<InventoryItem>(), "RenterId", "RenterId", rental.GameId);
            ViewData["PlayerId"] = new SelectList(_context.Player, "Id", "Id", rental.PlayerId);
            return View(rental);
        }
        // GET: Rentals/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            var rental = await _context.Rental
                .Include(r => r.InventoryItem).ThenInclude(g => g.Game)
                .Include(r => r.Player)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (rental == null)
            {
                return NotFound();
            }

            var model = new RentalDetailsViewModel
            {
                Id = rental.Id,
                Player = await _userManager.FindByIdAsync(rental.PlayerId),
                Renter = await _context.Renter.FirstOrDefaultAsync(m => m.Id == rental.RenterId),
                InventoryItem = rental.InventoryItem,
                IsReturned = rental.IsReturned,
                DateOfRent = rental.DateOfRent,
                DateOfReturn = rental.DateOfReturn
            };


            return View(model);
        }

        // POST: Rentals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var rental = await _context.Rental.FindAsync(id);
            _context.Rental.Remove(rental);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(History));
        }

        private bool RentalExists(string id)
        {
            return _context.Rental.Any(e => e.RenterId == id);
        }

        public async Task<IActionResult> Return(string id)
        {
            if(id == null)
            {
                return NotFound();
            }
            var rental = _context.Rental.Include(r => r.InventoryItem)
                .FirstOrDefault(r => r.Id == id);
            if(rental == null)
            {
                return NotFound();
            }

            rental.IsReturned = true;
            rental.DateOfReturn = DateTime.Now;
            rental.InventoryItem.Quantity += 1;
            _context.Update(rental);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { id });
        }
    }
}
