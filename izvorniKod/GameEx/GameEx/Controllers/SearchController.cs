﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Data;
using GameEx.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GameEx.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        public ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public SearchController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string query)
        {
            if(query == null)
            {
                query = "";
            }
            var players = _context.Player.Include(p => p.User)
                .Where(p => p.User.UserName.ToUpper().Contains(query.ToUpper().Trim()));
            ViewData["Players"] = players.Take(5).ToList();
            ViewData["PlayerCount"] = players.Count();

            var renters = _context.Renter.Include(p => p.User)
                .Where(r => r.CompanyName.ToUpper().Contains(query.ToUpper().Trim()));
            ViewData["Renters"] = renters.Take(5).ToList();
            ViewData["RenterCount"] = renters.Count();

            var games = _context.Game
                .Where(g => g.Name.ToUpper().Contains(query.ToUpper().Trim()));
            ViewData["Games"] = games.Take(5).ToList();
            ViewData["GameCount"] = games.Count();

            var groups = _context.Group.Include(g => g.GroupMembers).ThenInclude(g => g.Player)
                .Include(g => g.Game)
                .Where(g => g.Name.ToUpper().Contains(query.ToUpper().Trim()));
            ViewData["Groups"] = groups.Take(5).ToList();
            ViewData["GroupCount"] = groups.Count();

            var user = await _userManager.GetUserAsync(User);
            IList<string> roles = null;
            if (user != null)
            {
                roles = await _userManager.GetRolesAsync(user);
            }
            if (roles != null)
            {
                ViewData["Roles"] = roles;
            }

            return View();
        }

        [Route("Search/Api")]
        public List<SearchResult> Api(string term)
        {
            List<SearchResult> data = new List<SearchResult>();
            var players = _context.Player.Include(p => p.User).Include(p => p.User)
                .Where(p => p.User.UserName.ToUpper().Contains(term.ToUpper().Trim()))
                .Select(p => new SearchResult { Category = "Player", Id = p.Id, Name = p.User.UserName, Photo = p.User.ProfileImage })
                .Take(5).ToList();
            var renters = _context.Renter
                .Where(r => r.CompanyName.ToUpper().Contains(term.ToUpper().Trim()) && r.User.UserName.ToUpper().Contains(term.ToUpper().Trim())
                && r.ApprovedByAdmin)
                .Select(r => new SearchResult { Category = "Renter", Id = r.Id, Name = r.CompanyName, Photo = r.User.ProfileImage })
                .Take(5).ToList();
            var games =   _context.Game
                .Where(g => g.Name.ToUpper().Contains(term.ToUpper().Trim()))
                .Select(g => new SearchResult { Category = "Game", Id = g.Id, Name = g.Name, PhotoUrl = g.ThumbnailUrl }).Take(5).ToList();
            var groups =  _context.Group
                .Where(g => g.Name.ToUpper().Contains(term.ToUpper().Trim()))
                .Select(g => new SearchResult { Category = "Group", Id = g.Id, Name = g.Name }).Take(5).ToList();

            data.AddRange(players);
            data.AddRange(renters);
            data.AddRange(games);
            data.AddRange(groups);
            
            return data;
        }
    }
}
