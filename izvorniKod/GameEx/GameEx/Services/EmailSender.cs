﻿using MimeKit;
using MailKit.Net.Smtp;
using MimeKit.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using GameEx.Settings;
using Microsoft.Extensions.Options;

namespace GameEx.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly MailSettings _mailSettings;
        public EmailSender(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }
        public async Task SendEmailAsync(string to, string subject, string htmlMessage)
        {
            //create To and From email adresses
            //MailboxAddress fromAdress = MailboxAddress.Parse(_configuration.GetSection);
            MailboxAddress toAdress = MailboxAddress.Parse(to);

            //create the email message itself
            MimeMessage email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(toAdress);
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = htmlMessage };

            //send email message
            SmtpClient client = new SmtpClient();
            await client.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
            await client.AuthenticateAsync(_mailSettings.Mail, _mailSettings.Password);
            await client.SendAsync(email);
            await client.DisconnectAsync(true);
        }
    }
}
