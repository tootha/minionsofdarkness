﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;

namespace GameEx.ViewModels
{
    public class ReviewViewModel
    {
        public Review review { get; set; }
        public bool canEdit { get; set; }

    }
}
