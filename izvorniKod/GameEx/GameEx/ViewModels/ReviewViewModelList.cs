﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;

namespace GameEx.ViewModels
{
    public class ReviewViewModelList
    {
        public List<ReviewViewModel> reviewViewModels { get; set; }

    }
}
