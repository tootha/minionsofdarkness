﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.ViewModels
{
    public class ReviewPostModel
    {
        public string groupId { get; set; }
        public string reviewedPlayerId { get; set; }
        public int rating { get; set; }
        public string reviewText { get; set; }

    }
}
