﻿using GameEx.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.ViewModels
{
    public class RenterViewModel
    {
        public string Id { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string WebAddress { get; set; }
        [RegularExpression("[0-9]{11}")]
        [Required]
        public string OIB { get; set; }

        public User User { get; set; }

        public List<InventoryItem> InventoryItems { get; set; }
    }
}
