﻿using GameEx.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.ViewModels
{
    public class RentalDetailsViewModel
    {
        public string Id { get; set; }
        public Renter Renter { get; set; }
        public User Player { get; set; }
        public InventoryItem InventoryItem { get; set; }
        public bool IsReturned { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date of renting")]
        public DateTime DateOfRent { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date of return")]
        public DateTime? DateOfReturn { get; set; }
    }
}
