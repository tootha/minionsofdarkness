﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;
using cloudscribe.Pagination.Models;

namespace GameEx.ViewModels
{
    public class ExchangesViewModel
    {
        public PagedResult<Exchange> Initiated { get; set; }
        public PagedResult<Exchange> Targeted { get; set; }

        public List<bool> inExchange { get; set; }

    }
}
