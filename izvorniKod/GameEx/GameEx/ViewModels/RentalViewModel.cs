﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;

namespace GameEx.ViewModels
{
    public class RentalViewModel
    {
        public string Id { get; set; }
        public string PlayerId { get; set; }
        public Player Player { get; set; }
        public string RenterId { get; set; }
        public string GameId { get; set; }
        public InventoryItem InventoryItem { get; set; }
        public bool IsReturned { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfRent { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfReturn { get; set; }
    }
}
