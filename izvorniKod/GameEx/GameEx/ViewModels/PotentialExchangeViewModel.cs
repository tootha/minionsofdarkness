﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;


namespace GameEx.ViewModels
{
    public class PotentialExchangeViewModel
    {
        public string InitiatorId { get; set; }
        public Player Initiator { get; set; }
        public string TargetPlayerId { get; set; }
        public Player TargetPlayer { get; set; }
        public string WantedGameId { get; set; }
        public PlayerGame WantedGame { get; set; }
        public string OfferedGameId { get; set; }
        public PlayerGame OfferedGame { get; set; }
    }
}
