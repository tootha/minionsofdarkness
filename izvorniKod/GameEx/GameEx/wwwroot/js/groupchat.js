﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
document.getElementById("submitButton").disabled = true;

connection.on('recieveGroupMessage', addMessageToChat);

function sendMessageToHub(message) {
    connection.invoke('sendGroupMessage', message);
}


connection.start().then(function () {
    document.getElementById("submitButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});


class GroupMessage {
    constructor(userId, username, text, groupid, when) {
        this.senderId = userId;
        this.senderUsername = username;
        this.text = text;
        this.groupid = groupid;
        this.when = when;
    }
}

// userName is declared in razor page.
const username = userName;
console.log("Username:" + username)
const groupid = groupId;
const textInput = document.getElementById('messageText');
const whenInput = document.getElementById('when');
const chat = document.getElementById('chat');
const messagesQueue = [];



function clearInputField() {
    messagesQueue.push(textInput.value);
    textInput.value = "";
}

function sendGroupMessage() {
    let text = messagesQueue.shift() || "";
    if (text.trim() === "") return;

    let when = new Date();
    let message = new GroupMessage(userId, username, text, groupid,when);
    sendMessageToHub(message);
}

function addMessageToChat(message) {
    let isCurrentUserMessage = message.senderId === userId;

    console.log(message);
    console.log(message.groupId);
    console.log(groupid);    

    if (isCurrentUserMessage && message.groupId == groupid) {
        createOutgoingMessage(message);
    } else if (message.groupId == groupid) {
        createIncomingMessage(message);
    }
}


function createOutgoingMessage(message) {
    let mainDiv = document.createElement('div');
    mainDiv.className = "outgoing_msg";

    let usernamehead = document.createElement('div');
    usernamehead.className = "username_head";

    let sender = document.createElement('p');
    let senderBold = document.createElement('b');
    senderBold.innerHTML = message.senderUsername;

    let sentmsg = document.createElement('div');
    sentmsg.className = "sent_msg";

    let text = document.createElement('p');
    text.innerHTML = message.text;
    let time = document.createElement('span');
    //time.innerHTML = message.when;
    var currentdate = new Date();
    time.innerHTML =
        (currentdate.getDate() + "."
            + (currentdate.getMonth() + 1) + "."
            + currentdate.getFullYear() + ". "
            + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second:'numeric', hour12: false }));

    time.className = "time_date";

    sender.appendChild(senderBold);
    usernamehead.appendChild(sender);
    sentmsg.appendChild(text);
    sentmsg.appendChild(time);
    mainDiv.appendChild(usernamehead);
    mainDiv.appendChild(sentmsg);
    chat.appendChild(mainDiv);

    //scroll to the bottom of the div
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
}

function createIncomingMessage(message) {
    let mainDiv = document.createElement('div');
    mainDiv.className = "incoming_msg";

    let recievedmsg = document.createElement('div');
    recievedmsg.className = "received_msg";

    let recievedmsgChild = document.createElement('div');
    recievedmsgChild.className = "received_withd_msg";

    let usernamehead = document.createElement('div');
    usernamehead.className = "username_head_inc";

    let sender = document.createElement('p');
    let senderBold = document.createElement('b');
    senderBold.innerHTML = message.senderUsername;

    let text = document.createElement('p');
    text.innerHTML = message.text;

    let time = document.createElement('span');
    time.innerHTML = message.when;
    time.className = "time_date";

    sender.appendChild(senderBold)
    usernamehead.appendChild(sender);
    recievedmsgChild.appendChild(text);
    recievedmsgChild.appendChild(time);
    recievedmsg.appendChild(usernamehead);
    recievedmsg.appendChild(recievedmsgChild);
    
    mainDiv.appendChild(recievedmsg);
    chat.appendChild(mainDiv);

    //scroll to the bottom of the div
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
}

/*
outgoing_msg
                        <div class="username_head">
                            <p><b>@message.Sender.User.UserName</b></p>
                        </div>
                        <div class="sent_msg">
                            <p>@message.Text</p>
                            <span class="time_date">@message.When</span>
                        </div>
                    </div>


incoming_msg
                        
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <p>@message.Text</p>
                                <span class="time_date">@message.When</span>
                            </div>
                        </div>
                    </div>
 */