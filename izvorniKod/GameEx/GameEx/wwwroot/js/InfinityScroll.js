﻿function InfinitiySroll(iTable, iAction, iParams) {
    this.table = iTable;        // Reference to the table where data should be added
    this.action = iAction;      // Name of the conrtoller action
    this.params = iParams;      // Additional parameters to pass to the controller
    this.loading = false;       // true if asynchronous loading is in process
    let divsHeight = 0;
    this.AddTableLines = function (firstItem) {
        this.loading = true;
        this.params.firstItem = firstItem;
        // $("#footer").css("display", "block"); // show loading info
        $.ajax({
            type: 'POST',
            url: self.action,
            data: self.params,
            dataType: "html"
        })
            .done(function (result) {
                if (result) {
                    var numOfDivs = $("#" + self.table)[0].childElementCount;
                    $("#" + self.table).prepend(result);
                    
                    
                    if (numOfDivs == 0) {
                        $("#chat").scrollTop($("#chat")[0].scrollHeight);
                        divsHeight = $("#" + self.table).height();
                    } else {
                        console.log(divsHeight);
                        $("#chat").scrollTop(divsHeight);
                    }
                    var parseRes = $.parseHTML(result);
                    self.loading = false;
                }
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log("Error in AddTableLines:", thrownError);
            })
            .always(function () {
                // $("#footer").css("display", "none"); // hide loading info
            });
    }

    var self = this;
    $('#chat').scroll(function (ev) {
        
        if ($('#chat').scrollTop() < 10) {
            //User is currently at the bottom of the page
            if (!self.loading) {
                var itemCount = $('#' + self.table).children().length;
                self.AddTableLines(itemCount);
            }
        }
    });
    this.AddTableLines(0);
}