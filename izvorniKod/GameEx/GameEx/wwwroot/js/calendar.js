﻿document.addEventListener('DOMContentLoaded', function () {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendar.Draggable;

    var containerEl = document.getElementById('external-events');
    var calendarEl = document.getElementById('calendar');
    var checkbox = document.getElementById('drop-remove');

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
        itemSelector: '.fc-event',
        eventData: function (eventEl) {
            return {
                title: eventEl.innerText
            };
        }
    });


    

    // initialize the calendar
    // -----------------------------------------------------------------

    var calendar = new Calendar(calendarEl, {
        locale: 'en-UK',
        events: "/PlayingDates/GetGroupDates/" + groupId,
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        eventClick: function (info) {
            // change the border color just for fun
            //
            console.log(info);
            info.el.style.borderColor = 'red';
            info.el.style.backgroundColor = 'red';
        },
        editable: true,
        firstDay: 1,
        droppable: true, // this allows things to be dropped onto the calendar
        eventReceive: function (info) {
            console.log("Event recieve")
            console.log(info.relatedEvents);
            start = moment(info.event.start).format();
            var obj = {
                id: groupId,
                start: start,
                end: null,
                title: info.event.title
            };
            console.log(obj);
            $.ajax({
                url: "/PlayingDates/AddEvent",
                data: obj,
                method: "POST"
            });
        },

        eventDrop: function (eventResizeInfo) {
            console.log("Event drop")
            var start = moment(eventResizeInfo.event.start).format();


            var startOld = moment(eventResizeInfo.oldEvent.start).format();
            var endOld = moment(eventResizeInfo.oldEvent.end).format();
            var ev = {
                id: groupId,
                start: start,
                title: eventResizeInfo.event.title,
                idOld: groupId,
                startOld: startOld,
                endOld: endOld,
                titleOld: eventResizeInfo.oldEvent.title
            };
            if (eventResizeInfo.event.end != null) {
                var end = moment(eventResizeInfo.event.end).format();
                ev.end = end;
            }
            console.log(ev)
            $.ajax({
                url: "/PlayingDates/UpdateEvent",
                data: ev,
                method: "POST"
            });
        },
        eventResize: function (eventResizeInfo) {
            console.log("Event drop")
            var start = moment(eventResizeInfo.event.start).format();


            var startOld = moment(eventResizeInfo.oldEvent.start).format();
            var endOld = moment(eventResizeInfo.oldEvent.end).format();
            var ev = {
                id: groupId,
                start: start,
                title: eventResizeInfo.event.title,
                idOld: groupId,
                startOld: startOld,
                endOld: endOld,
                titleOld: eventResizeInfo.oldEvent.title
            };
            if (eventResizeInfo.event.end != null) {
                var end = moment(eventResizeInfo.event.end).format();
                ev.end = end;
            }
            console.log(ev)
            $.ajax({
                url: "/PlayingDates/UpdateEvent",
                data: ev,
                method: "POST"
            });
        },
        drop: function(){
            console.log("drop called");
        },
        eventLeave: function (ev) {
            console.log("Event leave");
        },
        eventDragStop: function (info) {
            console.log("Event drag stop called");
            var trashEl = jQuery('#calendarTrash');
            var ofs = trashEl.offset();

            var x1 = ofs.left;
            var x2 = ofs.left + trashEl.outerWidth(true);
            var y1 = ofs.top;
            var y2 = ofs.top + trashEl.outerHeight(true);

            if (info.jsEvent.pageX >= x1 && info.jsEvent.pageX <= x2 &&
                info.jsEvent.pageY >= y1 && info.jsEvent.pageY <= y2) {

                var start = moment(info.event.start).format();
                var end = moment(info.event.end).format();
                var ev = {
                    id: groupId,
                    start: start,
                    title: info.event.title,
                    end: end
                };
                console.log(ev);
                info.event.remove();
                $.ajax({
                    url: "/PlayingDates/DeleteEvent",
                    data: ev,
                    method: "POST"
                })
            }
        }
    });


    calendar.render();

});
