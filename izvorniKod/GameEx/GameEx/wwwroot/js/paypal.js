﻿// This is stored just in case the user cancels the other 
// or there is an error in the other process.
var orderId;
// Render the PayPal smart button into #paypal-button-container
paypal.Buttons({

    // Set up the transaction
    createOrder: function (data, actions) {
        orderId = data.orderID;
        return fetch('/api/paypal/checkout/order/create/', {
            method: 'post'
        }).then(function (res) {
            return res.json();
        }).then(function (data) {
            console.log(data);
            return data.orderID;
        });
    },

    // Finalise the transaction
    onApprove: function (data, actions) {
        return fetch('/api/paypal/checkout/order/approved/' + data.orderID, {
            method: 'post'
        }).then(function (res) {
            return actions.order.capture();
        }).then(function (details) {

            // (Preferred) Notify the server that the transaction id complete 
            // and have an option to display an order completed screen.
            window.location.replace('/api/paypal/checkout/order/complete/' +
                data.orderID);

            // OR
            // Notify the server that the transaction id complete
            //httpGet('/api/paypal/checkout/order/complete/' + data.orderID);

            // Show a success message to the buyer
            alert('Transaction completed by ' + details.payer.name.given_name + '!');
        });
    },

    // Buyer cancelled the payment
    onCancel: function (data, actions) {
        httpGet('/api/paypal/checkout/order/cancel/' + data.orderID);
    },

    // An error occurred during the transaction
    onError: function (err) {
        httpGet('/api/paypal/checkout/order/error/' + orderId + '/' +
            encodeURIComponent(err));
    }

}).render('#paypal-div');

function httpGet(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, false);
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

