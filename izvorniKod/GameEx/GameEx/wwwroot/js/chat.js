﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
document.getElementById("submitButton").disabled = true;

connection.on('receiveMessage', addMessageToChat);

function sendMessageToHub(message) {
    connection.invoke('sendMessage', message);
}


connection.start().then(function () {
    document.getElementById("submitButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});


class Message {
    constructor(username, receiver, text, when) {
        this.receiverUserName = receiver;
        this.senderUserName = username;
        this.text = text;
        this.when = when;
    }
}

// userName is declared in razor page.
const username = userName;
const textInput = document.getElementById('messageText');
const whenInput = document.getElementById('when');
const chat = document.getElementById('chat');
const messagesQueue = [];



function clearInputField() {
    messagesQueue.push(textInput.value);
    textInput.value = "";
}

function sendMessage() {
    let text = messagesQueue.shift() || "";
    if (text.trim() === "") return;
    
    let when = new Date();
    console.log(username);
    let message = new Message(userName, receiverUserName, text);
    console.log(message);
    sendMessageToHub(message);
}

function addMessageToChat(message) {
    let isCurrentUserMessage = message.senderId === userId;

    console.log(message);

    if (message.receiverUserName == username) {
        createIncomingMessage(message);

    }else{
        createOutgoingMessage(message);
    }
}


function createOutgoingMessage(message) {
    let mainDiv = document.createElement('div');
    mainDiv.className = "outgoing_msg";

    let usernamehead = document.createElement('div');
    usernamehead.className = "username_head";

    let sender = document.createElement('p');
    let senderBold = document.createElement('b');
    senderBold.innerHTML = message.senderUserName;

    let sentmsg = document.createElement('div');
    sentmsg.className = "sent_msg";

    let text = document.createElement('p');
    text.innerHTML = message.text;
    let time = document.createElement('span');
    //time.innerHTML = message.when;
    var currentdate = new Date();
    time.innerHTML =
        (currentdate.getDate() + "."
            + (currentdate.getMonth() + 1) + "."
            + currentdate.getFullYear() + ". "
            + currentdate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false }));

    time.className = "time_date";

    sender.appendChild(senderBold);
    usernamehead.appendChild(sender);
    sentmsg.appendChild(text);
    sentmsg.appendChild(time);
    mainDiv.appendChild(usernamehead);
    mainDiv.appendChild(sentmsg);
    chat.appendChild(mainDiv);
    console.log(message.receiverUserName);
    var name = 'message-' + message.receiverUserName
    console.log(name);
    document.getElementById(name).innerHTML = message.text;
    //scroll to the bottom of the div
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
}

function createIncomingMessage(message) {
    let mainDiv = document.createElement('div');
    mainDiv.className = "incoming_msg";

    let recievedmsg = document.createElement('div');
    recievedmsg.className = "received_msg";

    let recievedmsgChild = document.createElement('div');
    recievedmsgChild.className = "received_withd_msg";

    let usernamehead = document.createElement('div');
    usernamehead.className = "username_head_inc";

    let sender = document.createElement('p');
    let senderBold = document.createElement('b');
    senderBold.innerHTML = message.senderUserName;
    let text = document.createElement('p');
    text.innerHTML = message.text;

    let time = document.createElement('span');
    time.innerHTML = message.when;
    time.className = "time_date";

    sender.appendChild(senderBold)
    usernamehead.appendChild(sender);
    recievedmsgChild.appendChild(text);
    recievedmsgChild.appendChild(time);
    recievedmsg.appendChild(usernamehead);
    recievedmsg.appendChild(recievedmsgChild);

    mainDiv.appendChild(recievedmsg);
    chat.appendChild(mainDiv);

    //scroll to the bottom of the div
    $("#chat").scrollTop($("#chat")[0].scrollHeight);
}