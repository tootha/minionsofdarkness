﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEx.Models;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;


namespace GameEx.Paypal
{
    public class CreateOrderGameEx
    {
        private static OrderRequest BuildRequestBody(List<(InventoryItem, int)> itemAndQuantity, string currencyName)
        {
            List<Item> items = new List<Item>();
            decimal total = 0;
            for (int i = 0; i < itemAndQuantity.Count(); i++)
            {
                Item item = new Item
                {
                    Name = itemAndQuantity[i].Item1.Game.Name,
                    Description = "Game from: " + itemAndQuantity[i].Item1.Renter.CompanyName,
                    Sku = "sku01",
                    UnitAmount = new Money
                    {
                        CurrencyCode = currencyName,
                        Value = itemAndQuantity[i].Item1.Price.ToString()
                    },
                    Tax = new Money
                    {
                        CurrencyCode = currencyName,
                        Value = "0.00"
                    },
                    Quantity = itemAndQuantity[i].Item2.ToString(),
                    Category = "PHYSICAL_GOODS"
                };
                items.Add(item);
                decimal cost = itemAndQuantity[i].Item1.Price * itemAndQuantity[i].Item2;
                total += cost;
            }
            Console.WriteLine(total.ToString("0.00"));
            OrderRequest orderRequest = new OrderRequest()
            {
                CheckoutPaymentIntent = "CAPTURE",

                ApplicationContext = new ApplicationContext
                {
                    BrandName = "GameEx INC",
                    LandingPage = "BILLING",
                    CancelUrl = "https://localhost:5001/Cart/Index",
                    ReturnUrl = "https://localhost:5001/Cart/Index",
                    UserAction = "CONTINUE",
                    ShippingPreference = "SET_PROVIDED_ADDRESS"
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                    new PurchaseUnitRequest{
                        ReferenceId =  "PUHF",
                        Description = "Games pack",
                        CustomId = "Some quality games",
                        SoftDescriptor = "HighQualityGames",
                        AmountWithBreakdown = new AmountWithBreakdown
                        {
                            CurrencyCode = currencyName,
                            Value = total.ToString("0.00"),
                            AmountBreakdown = new AmountBreakdown
                            {
                                ItemTotal = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = total.ToString("0.00")
                                },
                                Shipping = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                Handling = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                TaxTotal = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                },
                                ShippingDiscount = new Money
                                {
                                    CurrencyCode = currencyName,
                                    Value = "0.00"
                                }
                            }
                        },
                        Items = items,
                        ShippingDetail = new ShippingDetail
                        {
                            Name = new Name
                            {
                                FullName = "John Doe"
                            },
                            AddressPortable = new AddressPortable
                            {
                                AddressLine1 = "123 Townsend St",
                                AddressLine2 = "Floor 6",
                                AdminArea2 = "San Francisco",
                                AdminArea1 = "CA",
                                PostalCode = "94107",
                                CountryCode = "US"
                            }
                        }
                    }
                }
            };

            return orderRequest;
        }

    }
}
