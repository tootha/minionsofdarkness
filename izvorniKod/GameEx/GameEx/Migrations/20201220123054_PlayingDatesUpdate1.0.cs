﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class PlayingDatesUpdate10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_GroupMember_PlayerId_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.AlterColumn<DateTime>(
                name: "End",
                schema: "public",
                table: "PlayingDates",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<string>(
                name: "GroupId",
                schema: "public",
                table: "PlayingDates",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "PlayingDates",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "Id",
                schema: "public",
                table: "PlayingDates",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "GroupMemberGroupId",
                schema: "public",
                table: "PlayingDates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupMemberPlayerId",
                schema: "public",
                table: "PlayingDates",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_PlayingDates_PlayerId",
                schema: "public",
                table: "PlayingDates",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayingDates_GroupMemberPlayerId_GroupMemberGroupId",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "GroupMemberPlayerId", "GroupMemberGroupId" });

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates",
                column: "GroupId",
                principalSchema: "public",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_GroupMember_GroupMemberPlayerId_GroupMemberGro~",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "GroupMemberPlayerId", "GroupMemberGroupId" },
                principalSchema: "public",
                principalTable: "GroupMember",
                principalColumns: new[] { "PlayerId", "GroupId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_GroupMember_GroupMemberPlayerId_GroupMemberGro~",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropIndex(
                name: "IX_PlayingDates_PlayerId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropIndex(
                name: "IX_PlayingDates_GroupMemberPlayerId_GroupMemberGroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropColumn(
                name: "Id",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropColumn(
                name: "GroupMemberGroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropColumn(
                name: "GroupMemberPlayerId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.AlterColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "PlayingDates",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GroupId",
                schema: "public",
                table: "PlayingDates",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "End",
                schema: "public",
                table: "PlayingDates",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "PlayerId", "GroupId", "Start", "End" });

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates",
                column: "GroupId",
                principalSchema: "public",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_GroupMember_PlayerId_GroupId",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "PlayerId", "GroupId" },
                principalSchema: "public",
                principalTable: "GroupMember",
                principalColumns: new[] { "PlayerId", "GroupId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
