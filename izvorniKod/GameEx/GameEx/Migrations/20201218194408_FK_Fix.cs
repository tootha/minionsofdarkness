﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class FK_Fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rental_InventoryItem_GameId_RenterId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropIndex(
                name: "IX_Rental_GameId_RenterId",
                schema: "public",
                table: "Rental");

            migrationBuilder.CreateIndex(
                name: "IX_Rental_RenterId_GameId",
                schema: "public",
                table: "Rental",
                columns: new[] { "RenterId", "GameId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental",
                columns: new[] { "RenterId", "GameId" },
                principalSchema: "public",
                principalTable: "InventoryItem",
                principalColumns: new[] { "RenterId", "GameId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropIndex(
                name: "IX_Rental_RenterId_GameId",
                schema: "public",
                table: "Rental");

            migrationBuilder.CreateIndex(
                name: "IX_Rental_GameId_RenterId",
                schema: "public",
                table: "Rental",
                columns: new[] { "GameId", "RenterId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_InventoryItem_GameId_RenterId",
                schema: "public",
                table: "Rental",
                columns: new[] { "GameId", "RenterId" },
                principalSchema: "public",
                principalTable: "InventoryItem",
                principalColumns: new[] { "RenterId", "GameId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
