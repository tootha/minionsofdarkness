﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class WillingToTrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "WillingToTrade",
                schema: "public",
                table: "PlayerGame",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WillingToTrade",
                schema: "public",
                table: "PlayerGame");
        }
    }
}
