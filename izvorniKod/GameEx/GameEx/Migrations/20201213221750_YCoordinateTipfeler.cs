﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class YCoordinateTipfeler : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationYCorrdinate",
                schema: "public",
                table: "Group");

            migrationBuilder.AddColumn<double>(
                name: "LocationYCoordinate",
                schema: "public",
                table: "Group",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationYCoordinate",
                schema: "public",
                table: "Group");

            migrationBuilder.AddColumn<double>(
                name: "LocationYCorrdinate",
                schema: "public",
                table: "Group",
                type: "double precision",
                nullable: true);
        }
    }
}
