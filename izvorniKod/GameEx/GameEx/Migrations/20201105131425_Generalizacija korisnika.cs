﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class Generalizacijakorisnika : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "public",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Igrac",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ProfileImagePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Igrac", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Igrac_AspNetUsers_Id",
                        column: x => x.Id,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Iznajmljivac",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ProfilePicture = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    WebAddress = table.Column<string>(nullable: true),
                    OIB = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Iznajmljivac", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Iznajmljivac_AspNetUsers_Id",
                        column: x => x.Id,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Igrac",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Iznajmljivac",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "public",
                table: "AspNetUsers");
        }
    }
}
