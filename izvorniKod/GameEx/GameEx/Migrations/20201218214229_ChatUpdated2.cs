﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class ChatUpdated2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Group_GroupId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                schema: "public",
                table: "GroupMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SenderId",
                schema: "public",
                table: "GroupMessage",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "GroupId",
                schema: "public",
                table: "GroupMessage",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMessage_GroupId",
                schema: "public",
                table: "GroupMessage",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Group_GroupId",
                schema: "public",
                table: "GroupMessage",
                column: "GroupId",
                principalSchema: "public",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Group_GroupId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropIndex(
                name: "IX_GroupMessage_GroupId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.AlterColumn<string>(
                name: "SenderId",
                schema: "public",
                table: "GroupMessage",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GroupId",
                schema: "public",
                table: "GroupMessage",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                schema: "public",
                table: "GroupMessage",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage",
                columns: new[] { "GroupId", "SenderId", "When" });

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Group_GroupId",
                schema: "public",
                table: "GroupMessage",
                column: "GroupId",
                principalSchema: "public",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
