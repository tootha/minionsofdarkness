﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class OwnerUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.DropForeignKey(
                name: "FK_Group_Player_PlayerId",
                schema: "public",
                table: "Group");
            /*
            migrationBuilder.DropIndex(
                name: "IX_Group_PlayerId",
                schema: "public",
                table: "Group");
            /*
            migrationBuilder.DropColumn(
                name: "PlayerId",
                schema: "public",
                table: "Group");*/
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            /*
            migrationBuilder.AddColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "Group",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Group_PlayerId",
                schema: "public",
                table: "Group",
                column: "PlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Player_PlayerId",
                schema: "public",
                table: "Group",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);*/
        }
    }
}
