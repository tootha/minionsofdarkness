﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class ChatUpdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_AspNetUsers_SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropForeignKey(
                name: "FK_Message_AspNetUsers_UserId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_UserId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.AlterColumn<string>(
                name: "SenderUserName",
                schema: "public",
                table: "Message",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "ReceiverUserName",
                schema: "public",
                table: "Message",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                schema: "public",
                table: "AspNetUsers",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(256)",
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AspNetUsers_UserName",
                schema: "public",
                table: "AspNetUsers",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_Message_ReceiverUserName",
                schema: "public",
                table: "Message",
                column: "ReceiverUserName");

            migrationBuilder.CreateIndex(
                name: "IX_Message_SenderUserName",
                schema: "public",
                table: "Message",
                column: "SenderUserName");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_AspNetUsers_ReceiverUserName",
                schema: "public",
                table: "Message",
                column: "ReceiverUserName",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "UserName",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Message_AspNetUsers_SenderUserName",
                schema: "public",
                table: "Message",
                column: "SenderUserName",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "UserName",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_AspNetUsers_ReceiverUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.DropForeignKey(
                name: "FK_Message_AspNetUsers_SenderUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_ReceiverUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_SenderUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_AspNetUsers_UserName",
                schema: "public",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "SenderUserName",
                schema: "public",
                table: "Message",
                type: "text",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ReceiverUserName",
                schema: "public",
                table: "Message",
                type: "text",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "SenderId",
                schema: "public",
                table: "Message",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                schema: "public",
                table: "AspNetUsers",
                type: "character varying(256)",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 256);

            migrationBuilder.CreateIndex(
                name: "IX_Message_SenderId",
                schema: "public",
                table: "Message",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Message_UserId",
                schema: "public",
                table: "Message",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_AspNetUsers_SenderId",
                schema: "public",
                table: "Message",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Message_AspNetUsers_UserId",
                schema: "public",
                table: "Message",
                column: "UserId",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
