﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GameSuggestionsUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameSuggestion_Player_PlayerId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropIndex(
                name: "IX_GameSuggestion_PlayerId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropColumn(
                name: "PlayerId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                schema: "public",
                table: "GameSuggestion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameSuggestion_UserId",
                schema: "public",
                table: "GameSuggestion",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameSuggestion_AspNetUsers_UserId",
                schema: "public",
                table: "GameSuggestion",
                column: "UserId",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameSuggestion_AspNetUsers_UserId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropIndex(
                name: "IX_GameSuggestion_UserId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.AddColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "GameSuggestion",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameSuggestion_PlayerId",
                schema: "public",
                table: "GameSuggestion",
                column: "PlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameSuggestion_Player_PlayerId",
                schema: "public",
                table: "GameSuggestion",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
