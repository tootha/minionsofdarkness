﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class Database_Final_update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GroupInvitation_GroupInvitationStatus_GroupInvitationStatus~",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.DropTable(
                name: "GroupInvitationStatus",
                schema: "public");

            migrationBuilder.DropIndex(
                name: "IX_GroupInvitation_GroupInvitationStatusId",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "GroupInvitationStatusId",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.DropColumn(
                name: "InvitationStatusId",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "Review",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                schema: "public",
                table: "GroupAdvert",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "GroupAdvert",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                schema: "public",
                table: "GameSuggestion",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "GameSuggestion",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhotoUrl",
                schema: "public",
                table: "Game",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "public",
                table: "Game",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Link",
                schema: "public",
                table: "Game",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "public",
                table: "Game",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "Review",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                schema: "public",
                table: "Message",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupInvitationStatusId",
                schema: "public",
                table: "GroupInvitation",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvitationStatusId",
                schema: "public",
                table: "GroupInvitation",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                schema: "public",
                table: "GroupAdvert",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "GroupAdvert",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                schema: "public",
                table: "GameSuggestion",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "public",
                table: "GameSuggestion",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PhotoUrl",
                schema: "public",
                table: "Game",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "public",
                table: "Game",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Link",
                schema: "public",
                table: "Game",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                schema: "public",
                table: "Game",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "GroupInvitationStatus",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupInvitationStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupInvitation_GroupInvitationStatusId",
                schema: "public",
                table: "GroupInvitation",
                column: "GroupInvitationStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_GroupInvitation_GroupInvitationStatus_GroupInvitationStatus~",
                schema: "public",
                table: "GroupInvitation",
                column: "GroupInvitationStatusId",
                principalSchema: "public",
                principalTable: "GroupInvitationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
