﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class RemoveImagePathDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfileImagePath",
                schema: "public",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProfileImagePath",
                schema: "public",
                table: "AspNetUsers",
                type: "text",
                nullable: true);
        }
    }
}
