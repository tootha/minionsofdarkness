﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class PhotoPathRemovedFromGameTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoPath",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<byte[]>(
                name: "Photo",
                schema: "public",
                table: "Game",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Photo",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<string>(
                name: "PhotoPath",
                schema: "public",
                table: "Game",
                type: "text",
                nullable: true);
        }
    }
}
