﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class Adverts_and_join_requests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupAdvert",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    GroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupAdvert", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupAdvert_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GroupJoinRequest",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    PlayerId = table.Column<string>(nullable: true),
                    GroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupJoinRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupJoinRequest_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupJoinRequest_Player_PlayerId",
                        column: x => x.PlayerId,
                        principalSchema: "public",
                        principalTable: "Player",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupAdvert_GroupId",
                schema: "public",
                table: "GroupAdvert",
                column: "GroupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GroupJoinRequest_GroupId",
                schema: "public",
                table: "GroupJoinRequest",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupJoinRequest_PlayerId",
                schema: "public",
                table: "GroupJoinRequest",
                column: "PlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupAdvert",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GroupJoinRequest",
                schema: "public");
        }
    }
}
