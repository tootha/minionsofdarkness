﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class BazaPodataka_V10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ApprovedByAdmin",
                schema: "public",
                table: "Iznajmljivac",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ExchangeStatus",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Descrption = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameCategory",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameSuggestion",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    PlayerId = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameSuggestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GameSuggestion_Igrac_PlayerId",
                        column: x => x.PlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Group",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OwnerId = table.Column<string>(nullable: true),
                    TimeOfPlaying = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Group_Igrac_OwnerId",
                        column: x => x.OwnerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GroupInvitationStatus",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupInvitationStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Game",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PhotoPath = table.Column<string>(nullable: true),
                    CategoryId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    NumberOfPlayers = table.Column<int>(nullable: false),
                    AvgPlayerTime = table.Column<TimeSpan>(nullable: false),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Game", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Game_GameCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "public",
                        principalTable: "GameCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GroupMember",
                schema: "public",
                columns: table => new
                {
                    GroupId = table.Column<string>(nullable: false),
                    PlayerId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMember", x => new { x.PlayerId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_GroupMember_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupMember_Igrac_PlayerId",
                        column: x => x.PlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupMessage",
                schema: "public",
                columns: table => new
                {
                    SenderId = table.Column<string>(nullable: false),
                    GroupId = table.Column<string>(nullable: false),
                    TimeSent = table.Column<DateTime>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMessage", x => new { x.GroupId, x.SenderId, x.TimeSent });
                    table.ForeignKey(
                        name: "FK_GroupMessage_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupMessage_Igrac_SenderId",
                        column: x => x.SenderId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Review",
                schema: "public",
                columns: table => new
                {
                    GroupId = table.Column<string>(nullable: false),
                    ReviewerId = table.Column<string>(nullable: false),
                    ReviewedPlayerId = table.Column<string>(nullable: false),
                    TimeWritten = table.Column<DateTime>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Review", x => new { x.GroupId, x.ReviewedPlayerId, x.ReviewerId, x.TimeWritten });
                    table.ForeignKey(
                        name: "FK_Review_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Review_Igrac_ReviewedPlayerId",
                        column: x => x.ReviewedPlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Review_Igrac_ReviewerId",
                        column: x => x.ReviewerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupInvitation",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    GroupId = table.Column<string>(nullable: true),
                    InvitedPlayerId = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    InvitationStatusId = table.Column<string>(nullable: true),
                    GroupInvitationStatusId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupInvitation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupInvitation_Group_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "public",
                        principalTable: "Group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupInvitation_GroupInvitationStatus_GroupInvitationStatus~",
                        column: x => x.GroupInvitationStatusId,
                        principalSchema: "public",
                        principalTable: "GroupInvitationStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupInvitation_Igrac_InvitedPlayerId",
                        column: x => x.InvitedPlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Exchange",
                schema: "public",
                columns: table => new
                {
                    InitiatorId = table.Column<string>(nullable: false),
                    TargetPlayerId = table.Column<string>(nullable: false),
                    WantedGameId = table.Column<string>(nullable: false),
                    OfferedGameId = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    ExchangeStatusId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exchange", x => new { x.InitiatorId, x.TargetPlayerId, x.OfferedGameId, x.WantedGameId, x.Created });
                    table.ForeignKey(
                        name: "FK_Exchange_ExchangeStatus_ExchangeStatusId",
                        column: x => x.ExchangeStatusId,
                        principalSchema: "public",
                        principalTable: "ExchangeStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Exchange_Igrac_InitiatorId",
                        column: x => x.InitiatorId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exchange_Game_OfferedGameId",
                        column: x => x.OfferedGameId,
                        principalSchema: "public",
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exchange_Igrac_TargetPlayerId",
                        column: x => x.TargetPlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exchange_Game_WantedGameId",
                        column: x => x.WantedGameId,
                        principalSchema: "public",
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InventoryItem",
                schema: "public",
                columns: table => new
                {
                    GameId = table.Column<string>(nullable: false),
                    RenterId = table.Column<string>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryItem", x => new { x.RenterId, x.GameId });
                    table.ForeignKey(
                        name: "FK_InventoryItem_Game_GameId",
                        column: x => x.GameId,
                        principalSchema: "public",
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InventoryItem_Iznajmljivac_RenterId",
                        column: x => x.RenterId,
                        principalSchema: "public",
                        principalTable: "Iznajmljivac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerGame",
                schema: "public",
                columns: table => new
                {
                    GameId = table.Column<string>(nullable: false),
                    PlayerId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerGame", x => new { x.GameId, x.PlayerId });
                    table.ForeignKey(
                        name: "FK_PlayerGame_Game_GameId",
                        column: x => x.GameId,
                        principalSchema: "public",
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerGame_Igrac_PlayerId",
                        column: x => x.PlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerPlayingDates",
                schema: "public",
                columns: table => new
                {
                    GroupId = table.Column<string>(nullable: false),
                    PlayerId = table.Column<string>(nullable: false),
                    PlayingDate = table.Column<DateTime>(nullable: false),
                    GroupMemberPlayerId = table.Column<string>(nullable: true),
                    GroupMemberGroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerPlayingDates", x => new { x.PlayerId, x.GroupId, x.PlayingDate });
                    table.ForeignKey(
                        name: "FK_PlayerPlayingDates_GroupMember_GroupMemberPlayerId_GroupMem~",
                        columns: x => new { x.GroupMemberPlayerId, x.GroupMemberGroupId },
                        principalSchema: "public",
                        principalTable: "GroupMember",
                        principalColumns: new[] { "PlayerId", "GroupId" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rental",
                schema: "public",
                columns: table => new
                {
                    PlayerId = table.Column<string>(nullable: false),
                    RenterId = table.Column<string>(nullable: false),
                    GameId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rental", x => new { x.RenterId, x.PlayerId, x.GameId });
                    table.ForeignKey(
                        name: "FK_Rental_Igrac_PlayerId",
                        column: x => x.PlayerId,
                        principalSchema: "public",
                        principalTable: "Igrac",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rental_InventoryItem_GameId_RenterId",
                        columns: x => new { x.GameId, x.RenterId },
                        principalSchema: "public",
                        principalTable: "InventoryItem",
                        principalColumns: new[] { "RenterId", "GameId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Exchange_ExchangeStatusId",
                schema: "public",
                table: "Exchange",
                column: "ExchangeStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Exchange_OfferedGameId",
                schema: "public",
                table: "Exchange",
                column: "OfferedGameId");

            migrationBuilder.CreateIndex(
                name: "IX_Exchange_TargetPlayerId",
                schema: "public",
                table: "Exchange",
                column: "TargetPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Exchange_WantedGameId",
                schema: "public",
                table: "Exchange",
                column: "WantedGameId");

            migrationBuilder.CreateIndex(
                name: "IX_Game_CategoryId",
                schema: "public",
                table: "Game",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GameSuggestion_PlayerId",
                schema: "public",
                table: "GameSuggestion",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_OwnerId",
                schema: "public",
                table: "Group",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupInvitation_GroupId",
                schema: "public",
                table: "GroupInvitation",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupInvitation_GroupInvitationStatusId",
                schema: "public",
                table: "GroupInvitation",
                column: "GroupInvitationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupInvitation_InvitedPlayerId",
                schema: "public",
                table: "GroupInvitation",
                column: "InvitedPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMember_GroupId",
                schema: "public",
                table: "GroupMember",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMessage_SenderId",
                schema: "public",
                table: "GroupMessage",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItem_GameId",
                schema: "public",
                table: "InventoryItem",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerGame_PlayerId",
                schema: "public",
                table: "PlayerGame",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerPlayingDates_GroupMemberPlayerId_GroupMemberGroupId",
                schema: "public",
                table: "PlayerPlayingDates",
                columns: new[] { "GroupMemberPlayerId", "GroupMemberGroupId" });

            migrationBuilder.CreateIndex(
                name: "IX_Rental_PlayerId",
                schema: "public",
                table: "Rental",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Rental_GameId_RenterId",
                schema: "public",
                table: "Rental",
                columns: new[] { "GameId", "RenterId" });

            migrationBuilder.CreateIndex(
                name: "IX_Review_ReviewedPlayerId",
                schema: "public",
                table: "Review",
                column: "ReviewedPlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_ReviewerId",
                schema: "public",
                table: "Review",
                column: "ReviewerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Exchange",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GameSuggestion",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GroupInvitation",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GroupMessage",
                schema: "public");

            migrationBuilder.DropTable(
                name: "PlayerGame",
                schema: "public");

            migrationBuilder.DropTable(
                name: "PlayerPlayingDates",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Rental",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Review",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ExchangeStatus",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GroupInvitationStatus",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GroupMember",
                schema: "public");

            migrationBuilder.DropTable(
                name: "InventoryItem",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Group",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Game",
                schema: "public");

            migrationBuilder.DropTable(
                name: "GameCategory",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "ApprovedByAdmin",
                schema: "public",
                table: "Iznajmljivac");
        }
    }
}
