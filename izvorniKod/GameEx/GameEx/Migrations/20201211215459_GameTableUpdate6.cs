﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GameTableUpdate6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Game_GameCategory_CategoryId",
                schema: "public",
                table: "Game");

            migrationBuilder.DropIndex(
                name: "IX_Game_CategoryId",
                schema: "public",
                table: "Game");

            migrationBuilder.CreateTable(
                name: "GameCategoryGame",
                schema: "public",
                columns: table => new
                {
                    GameId = table.Column<string>(nullable: false),
                    CategoryId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCategoryGame", x => new { x.GameId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_GameCategoryGame_GameCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "public",
                        principalTable: "GameCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameCategoryGame_Game_GameId",
                        column: x => x.GameId,
                        principalSchema: "public",
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameCategoryGame_CategoryId",
                schema: "public",
                table: "GameCategoryGame",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameCategoryGame",
                schema: "public");

            migrationBuilder.CreateIndex(
                name: "IX_Game_CategoryId",
                schema: "public",
                table: "Game",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Game_GameCategory_CategoryId",
                schema: "public",
                table: "Game",
                column: "CategoryId",
                principalSchema: "public",
                principalTable: "GameCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
