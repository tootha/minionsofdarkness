﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class BazaPodataka_V101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exchange_Igrac_InitiatorId",
                schema: "public",
                table: "Exchange");

            migrationBuilder.DropForeignKey(
                name: "FK_Exchange_Igrac_TargetPlayerId",
                schema: "public",
                table: "Exchange");

            migrationBuilder.DropForeignKey(
                name: "FK_GameSuggestion_Igrac_PlayerId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_Igrac_OwnerId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupInvitation_Igrac_InvitedPlayerId",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMember_Igrac_PlayerId",
                schema: "public",
                table: "GroupMember");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Igrac_SenderId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_Igrac_AspNetUsers_Id",
                schema: "public",
                table: "Igrac");

            migrationBuilder.DropForeignKey(
                name: "FK_InventoryItem_Iznajmljivac_RenterId",
                schema: "public",
                table: "InventoryItem");

            migrationBuilder.DropForeignKey(
                name: "FK_Iznajmljivac_AspNetUsers_Id",
                schema: "public",
                table: "Iznajmljivac");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayerGame_Igrac_PlayerId",
                schema: "public",
                table: "PlayerGame");

            migrationBuilder.DropForeignKey(
                name: "FK_Rental_Igrac_PlayerId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Igrac_ReviewedPlayerId",
                schema: "public",
                table: "Review");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Igrac_ReviewerId",
                schema: "public",
                table: "Review");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Iznajmljivac",
                schema: "public",
                table: "Iznajmljivac");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Igrac",
                schema: "public",
                table: "Igrac");

            migrationBuilder.RenameTable(
                name: "Iznajmljivac",
                schema: "public",
                newName: "Renter",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Igrac",
                schema: "public",
                newName: "Player",
                newSchema: "public");

            migrationBuilder.AlterColumn<bool>(
                name: "ApprovedByAdmin",
                schema: "public",
                table: "Renter",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Renter",
                schema: "public",
                table: "Renter",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Player",
                schema: "public",
                table: "Player",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Exchange_Player_InitiatorId",
                schema: "public",
                table: "Exchange",
                column: "InitiatorId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Exchange_Player_TargetPlayerId",
                schema: "public",
                table: "Exchange",
                column: "TargetPlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameSuggestion_Player_PlayerId",
                schema: "public",
                table: "GameSuggestion",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Player_OwnerId",
                schema: "public",
                table: "Group",
                column: "OwnerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupInvitation_Player_InvitedPlayerId",
                schema: "public",
                table: "GroupInvitation",
                column: "InvitedPlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMember_Player_PlayerId",
                schema: "public",
                table: "GroupMember",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryItem_Renter_RenterId",
                schema: "public",
                table: "InventoryItem",
                column: "RenterId",
                principalSchema: "public",
                principalTable: "Renter",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Player_AspNetUsers_Id",
                schema: "public",
                table: "Player",
                column: "Id",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerGame_Player_PlayerId",
                schema: "public",
                table: "PlayerGame",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Renter_AspNetUsers_Id",
                schema: "public",
                table: "Renter",
                column: "Id",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Player_ReviewedPlayerId",
                schema: "public",
                table: "Review",
                column: "ReviewedPlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Player_ReviewerId",
                schema: "public",
                table: "Review",
                column: "ReviewerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exchange_Player_InitiatorId",
                schema: "public",
                table: "Exchange");

            migrationBuilder.DropForeignKey(
                name: "FK_Exchange_Player_TargetPlayerId",
                schema: "public",
                table: "Exchange");

            migrationBuilder.DropForeignKey(
                name: "FK_GameSuggestion_Player_PlayerId",
                schema: "public",
                table: "GameSuggestion");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_Player_OwnerId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupInvitation_Player_InvitedPlayerId",
                schema: "public",
                table: "GroupInvitation");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMember_Player_PlayerId",
                schema: "public",
                table: "GroupMember");

            migrationBuilder.DropForeignKey(
                name: "FK_GroupMessage_Player_SenderId",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_InventoryItem_Renter_RenterId",
                schema: "public",
                table: "InventoryItem");

            migrationBuilder.DropForeignKey(
                name: "FK_Player_AspNetUsers_Id",
                schema: "public",
                table: "Player");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayerGame_Player_PlayerId",
                schema: "public",
                table: "PlayerGame");

            migrationBuilder.DropForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropForeignKey(
                name: "FK_Renter_AspNetUsers_Id",
                schema: "public",
                table: "Renter");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Player_ReviewedPlayerId",
                schema: "public",
                table: "Review");

            migrationBuilder.DropForeignKey(
                name: "FK_Review_Player_ReviewerId",
                schema: "public",
                table: "Review");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Renter",
                schema: "public",
                table: "Renter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Player",
                schema: "public",
                table: "Player");

            migrationBuilder.RenameTable(
                name: "Renter",
                schema: "public",
                newName: "Iznajmljivac",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Player",
                schema: "public",
                newName: "Igrac",
                newSchema: "public");

            migrationBuilder.AlterColumn<bool>(
                name: "ApprovedByAdmin",
                schema: "public",
                table: "Iznajmljivac",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Iznajmljivac",
                schema: "public",
                table: "Iznajmljivac",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Igrac",
                schema: "public",
                table: "Igrac",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Exchange_Igrac_InitiatorId",
                schema: "public",
                table: "Exchange",
                column: "InitiatorId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Exchange_Igrac_TargetPlayerId",
                schema: "public",
                table: "Exchange",
                column: "TargetPlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameSuggestion_Igrac_PlayerId",
                schema: "public",
                table: "GameSuggestion",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Igrac_OwnerId",
                schema: "public",
                table: "Group",
                column: "OwnerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupInvitation_Igrac_InvitedPlayerId",
                schema: "public",
                table: "GroupInvitation",
                column: "InvitedPlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMember_Igrac_PlayerId",
                schema: "public",
                table: "GroupMember",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GroupMessage_Igrac_SenderId",
                schema: "public",
                table: "GroupMessage",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Igrac_AspNetUsers_Id",
                schema: "public",
                table: "Igrac",
                column: "Id",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InventoryItem_Iznajmljivac_RenterId",
                schema: "public",
                table: "InventoryItem",
                column: "RenterId",
                principalSchema: "public",
                principalTable: "Iznajmljivac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Iznajmljivac_AspNetUsers_Id",
                schema: "public",
                table: "Iznajmljivac",
                column: "Id",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerGame_Igrac_PlayerId",
                schema: "public",
                table: "PlayerGame",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_Igrac_PlayerId",
                schema: "public",
                table: "Rental",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Igrac_ReviewedPlayerId",
                schema: "public",
                table: "Review",
                column: "ReviewedPlayerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Review_Igrac_ReviewerId",
                schema: "public",
                table: "Review",
                column: "ReviewerId",
                principalSchema: "public",
                principalTable: "Igrac",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
