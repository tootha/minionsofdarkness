﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class ChatUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                schema: "public",
                table: "Message");

            migrationBuilder.AddColumn<string>(
                name: "ReceiverUserName",
                schema: "public",
                table: "Message",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SenderId",
                schema: "public",
                table: "Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SenderUserName",
                schema: "public",
                table: "Message",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Message_SenderId",
                schema: "public",
                table: "Message",
                column: "SenderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_AspNetUsers_SenderId",
                schema: "public",
                table: "Message",
                column: "SenderId",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_AspNetUsers_SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "ReceiverUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "SenderId",
                schema: "public",
                table: "Message");

            migrationBuilder.DropColumn(
                name: "SenderUserName",
                schema: "public",
                table: "Message");

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                schema: "public",
                table: "Message",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
