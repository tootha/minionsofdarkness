﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GroupAddedGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GameId",
                schema: "public",
                table: "Group",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Group_GameId",
                schema: "public",
                table: "Group",
                column: "GameId");

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Game_GameId",
                schema: "public",
                table: "Group",
                column: "GameId",
                principalSchema: "public",
                principalTable: "Game",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Group_Game_GameId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Group_GameId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "GameId",
                schema: "public",
                table: "Group");
        }
    }
}
