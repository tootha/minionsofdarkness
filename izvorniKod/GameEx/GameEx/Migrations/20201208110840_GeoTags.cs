﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GeoTags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "LocationRadius",
                schema: "public",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LocationXCoordinate",
                schema: "public",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "LocationYCorrdinate",
                schema: "public",
                table: "Group",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocationRadius",
                schema: "public",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "LocationXCoordinate",
                schema: "public",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "LocationYCorrdinate",
                schema: "public",
                table: "Group");
        }
    }
}
