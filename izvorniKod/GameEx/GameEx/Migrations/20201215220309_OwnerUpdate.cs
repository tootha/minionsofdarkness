﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class OwnerUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Group_Player_OwnerId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Group_OwnerId",
                schema: "public",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "LocationYCoordinate",
                schema: "public",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                schema: "public",
                table: "Group");

            migrationBuilder.AddColumn<bool>(
                name: "Owner",
                schema: "public",
                table: "GroupMember",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "LocationYCoordinate",
                schema: "public",
                table: "Group",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Owner",
                schema: "public",
                table: "GroupMember");

            migrationBuilder.DropColumn(
                name: "LocationYCoordinate",
                schema: "public",
                table: "Group");


            migrationBuilder.AddColumn<double>(
                name: "LocationYCorrdinate",
                schema: "public",
                table: "Group",
                type: "double precision",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerId",
                schema: "public",
                table: "Group",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Group_OwnerId",
                schema: "public",
                table: "Group",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Player_OwnerId",
                schema: "public",
                table: "Group",
                column: "OwnerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
