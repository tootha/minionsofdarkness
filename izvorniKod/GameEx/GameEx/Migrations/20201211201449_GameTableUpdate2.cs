﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GameTableUpdate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Photo",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<string>(
                name: "PhotoUrl",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ThumbnailUrl",
                schema: "public",
                table: "Game",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoUrl",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "ThumbnailUrl",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<byte[]>(
                name: "Photo",
                schema: "public",
                table: "Game",
                type: "bytea",
                nullable: true);
        }
    }
}
