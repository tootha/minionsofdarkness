﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class Rental_Dates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfRent",
                schema: "public",
                table: "Rental",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfReturn",
                schema: "public",
                table: "Rental",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsReturned",
                schema: "public",
                table: "Rental",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfRent",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "DateOfReturn",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "IsReturned",
                schema: "public",
                table: "Rental");
        }
    }
}
