﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class GameTableUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvgPlayerTime",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "NumberOfPlayers",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<string>(
                name: "Designer",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxPlayers",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxPlayingTime",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinPlayers",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinPlayingTime",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Publisher",
                schema: "public",
                table: "Game",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Rating",
                schema: "public",
                table: "Game",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Designer",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "MaxPlayers",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "MaxPlayingTime",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "MinPlayers",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "MinPlayingTime",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "Publisher",
                schema: "public",
                table: "Game");

            migrationBuilder.DropColumn(
                name: "Rating",
                schema: "public",
                table: "Game");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "AvgPlayerTime",
                schema: "public",
                table: "Game",
                type: "interval",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<int>(
                name: "NumberOfPlayers",
                schema: "public",
                table: "Game",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
