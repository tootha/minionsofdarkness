﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class ChatUpdated1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropColumn(
                name: "TimeSent",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.AddColumn<DateTime>(
                name: "When",
                schema: "public",
                table: "GroupMessage",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage",
                columns: new[] { "GroupId", "SenderId", "When" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.DropColumn(
                name: "When",
                schema: "public",
                table: "GroupMessage");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeSent",
                schema: "public",
                table: "GroupMessage",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMessage",
                schema: "public",
                table: "GroupMessage",
                columns: new[] { "GroupId", "SenderId", "TimeSent" });
        }
    }
}
