﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class PlayingDatesUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerPlayingDates_GroupMember_GroupMemberPlayerId_GroupMem~",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerPlayingDates",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.DropIndex(
                name: "IX_PlayerPlayingDates_GroupMemberPlayerId_GroupMemberGroupId",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.DropColumn(
                name: "PlayingDate",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.DropColumn(
                name: "GroupMemberGroupId",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.DropColumn(
                name: "GroupMemberPlayerId",
                schema: "public",
                table: "PlayerPlayingDates");

            migrationBuilder.RenameTable(
                name: "PlayerPlayingDates",
                schema: "public",
                newName: "PlayingDates",
                newSchema: "public");

            migrationBuilder.AddColumn<DateTime>(
                name: "Start",
                schema: "public",
                table: "PlayingDates",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                schema: "public",
                table: "PlayingDates",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "PlayerId", "GroupId", "Start", "End" });

            migrationBuilder.CreateIndex(
                name: "IX_PlayingDates_GroupId",
                schema: "public",
                table: "PlayingDates",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates",
                column: "GroupId",
                principalSchema: "public",
                principalTable: "Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayingDates_GroupMember_PlayerId_GroupId",
                schema: "public",
                table: "PlayingDates",
                columns: new[] { "PlayerId", "GroupId" },
                principalSchema: "public",
                principalTable: "GroupMember",
                principalColumns: new[] { "PlayerId", "GroupId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Group_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_Player_PlayerId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayingDates_GroupMember_PlayerId_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayingDates",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropIndex(
                name: "IX_PlayingDates_GroupId",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropColumn(
                name: "Start",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.DropColumn(
                name: "End",
                schema: "public",
                table: "PlayingDates");

            migrationBuilder.RenameTable(
                name: "PlayingDates",
                schema: "public",
                newName: "PlayerPlayingDates",
                newSchema: "public");

            migrationBuilder.AddColumn<DateTime>(
                name: "PlayingDate",
                schema: "public",
                table: "PlayerPlayingDates",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "GroupMemberGroupId",
                schema: "public",
                table: "PlayerPlayingDates",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupMemberPlayerId",
                schema: "public",
                table: "PlayerPlayingDates",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerPlayingDates",
                schema: "public",
                table: "PlayerPlayingDates",
                columns: new[] { "PlayerId", "GroupId", "PlayingDate" });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerPlayingDates_GroupMemberPlayerId_GroupMemberGroupId",
                schema: "public",
                table: "PlayerPlayingDates",
                columns: new[] { "GroupMemberPlayerId", "GroupMemberGroupId" });

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerPlayingDates_GroupMember_GroupMemberPlayerId_GroupMem~",
                schema: "public",
                table: "PlayerPlayingDates",
                columns: new[] { "GroupMemberPlayerId", "GroupMemberGroupId" },
                principalSchema: "public",
                principalTable: "GroupMember",
                principalColumns: new[] { "PlayerId", "GroupId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
