﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEx.Migrations
{
    public partial class Rental_PK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rental",
                schema: "public",
                table: "Rental");

            migrationBuilder.AlterColumn<string>(
                name: "GameId",
                schema: "public",
                table: "Rental",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "Rental",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "RenterId",
                schema: "public",
                table: "Rental",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "Id",
                schema: "public",
                table: "Rental",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rental",
                schema: "public",
                table: "Rental",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental",
                columns: new[] { "RenterId", "GameId" },
                principalSchema: "public",
                principalTable: "InventoryItem",
                principalColumns: new[] { "RenterId", "GameId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rental",
                schema: "public",
                table: "Rental");

            migrationBuilder.DropColumn(
                name: "Id",
                schema: "public",
                table: "Rental");

            migrationBuilder.AlterColumn<string>(
                name: "RenterId",
                schema: "public",
                table: "Rental",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PlayerId",
                schema: "public",
                table: "Rental",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "GameId",
                schema: "public",
                table: "Rental",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rental",
                schema: "public",
                table: "Rental",
                columns: new[] { "RenterId", "PlayerId", "GameId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_Player_PlayerId",
                schema: "public",
                table: "Rental",
                column: "PlayerId",
                principalSchema: "public",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rental_InventoryItem_RenterId_GameId",
                schema: "public",
                table: "Rental",
                columns: new[] { "RenterId", "GameId" },
                principalSchema: "public",
                principalTable: "InventoryItem",
                principalColumns: new[] { "RenterId", "GameId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
