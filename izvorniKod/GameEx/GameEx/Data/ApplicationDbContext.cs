﻿using GameEx.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEx.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public ApplicationDbContext()
        {
        }
        public DbSet<PlayerPlayingDates> PlayingDates { get; set; }
        public DbSet<Player> Player { get; set; }
        public DbSet<Renter> Renter { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<GroupMember> GroupMember { get; set; }
        public DbSet<GroupInvitation> GroupInvitation { get; set; }
        public DbSet<Rental> Rental { get; set; }
        public DbSet<PlayerGame> PlayerGame { get; set; }
        public DbSet<InventoryItem> InventoryItem { get; set; }
        public DbSet<GameCategory> GameCategory { get; set; }
        public DbSet<GameCategoryGame> GameCategoryGame { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<GroupMessage> GroupMessage { get; set; }
        public DbSet<Exchange> Exchange { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<GroupJoinRequest> GroupJoinRequest { get; set; }
        //TODO dodaj ostale DbSetove

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { 
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // PostgreSQL uses the public schema by default - not dbo.
            modelBuilder.HasDefaultSchema("public");

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasKey(p => p.Id);
                entity.HasOne(i => i.User).WithOne(k => k.Player)
                    .HasForeignKey<Player>(i => i.Id);
            });

            modelBuilder.Entity<Renter>(entity =>
            {
                entity.HasKey(r => r.Id);
                entity.HasOne(i => i.User).WithOne(k => k.Renter)
                    .HasForeignKey<Renter>(i => i.Id);
                entity.Property(r => r.ApprovedByAdmin).HasDefaultValue(false);
            });

            modelBuilder.Entity<Exchange>(entity =>
            {
                entity.HasKey(e => new { e.InitiatorId,e.TargetPlayerId,e.OfferedGameId,e.WantedGameId,e.Created});
                entity.HasOne(e => e.Initiator).WithMany(i => i.InitiatedExchanges)
                    .HasForeignKey(e => e.InitiatorId);
                entity.HasOne(e => e.TargetPlayer).WithMany(i => i.TargetedExchanges)
                    .HasForeignKey(e => e.TargetPlayerId);
                entity.HasOne(e => e.OfferedGame).WithMany(g => g.OfferedGames)
                    .HasForeignKey(e => e.OfferedGameId);
                entity.HasOne(e => e.WantedGame).WithMany(g => g.WantedGames)
                    .HasForeignKey(e => e.WantedGameId);
                entity.HasOne(e => e.ExchangeStatus).WithMany(es => es.Exchnages)
                    .HasForeignKey(e => e.ExchangeStatusId);
            });

            modelBuilder.Entity<ExchangeStatus>(entity =>
            {
                entity.HasKey(e => e.Id);
                
            });


            modelBuilder.Entity<Game>(entity =>
            {
                entity.HasKey(g => g.Id);
            });

            modelBuilder.Entity<GameCategory>(entity =>
            {
                entity.HasKey(g => g.Id);
            });

            modelBuilder.Entity<GameCategoryGame>(entity => 
            {
                entity.HasKey(gcg => new { gcg.GameId, gcg.CategoryId });
                entity.HasOne(gcg => gcg.Game).WithMany(g => g.GameCategoryGames)
                    .HasForeignKey(gcg => gcg.GameId);
                entity.HasOne(gcg => gcg.GameCategory).WithMany(gc => gc.GameCategoryGames)
                    .HasForeignKey(gcg => gcg.CategoryId);
            });

            modelBuilder.Entity<GameSuggestion>(entity =>
            {
                entity.HasKey(g => g.Id);
                entity.HasOne(e => e.User).WithMany(p => p.GameSuggestions)
                    .HasForeignKey(e => e.UserId);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.HasKey(g => g.Id);
                entity.HasOne(g => g.Game).WithMany(g => g.Groups)
                    .HasForeignKey(g => g.GameId);
            });

            modelBuilder.Entity<GroupInvitation>(entity =>
            {
                entity.HasKey(g => g.Id);
                entity.HasOne(e => e.Group).WithMany(g => g.GroupInvitations)
                    .HasForeignKey(e => e.GroupId);
                entity.HasOne(e => e.InvitedPlayer).WithMany(p => p.GroupInvitations)
                    .HasForeignKey(e => e.InvitedPlayerId);
            });

            
            modelBuilder.Entity<GroupMember>(entity =>
            {
                entity.HasKey(g => new { g.PlayerId, g.GroupId});
                entity.HasOne(e => e.Group).WithMany(g => g.GroupMembers)
                    .HasForeignKey(e => e.GroupId);
                entity.HasOne(e => e.Player).WithMany(g => g.GroupMemberships)
                    .HasForeignKey(e => e.PlayerId);
            });

            modelBuilder.Entity<GroupMessage>(entity =>
            {
                entity.HasKey(g => new { g.Id });
                entity.HasOne(e => e.Group).WithMany(g => g.GroupMessages)
                    .HasForeignKey(e => e.GroupId);
                entity.HasOne(e => e.Sender).WithMany(p => p.GroupMessages)
                    .HasForeignKey(e => e.SenderId);
            });

            modelBuilder.Entity<InventoryItem>(entity =>
            {
                entity.HasKey(i => new { i.RenterId, i.GameId});
                entity.HasOne(i => i.Game).WithMany(g => g.InventoryItems)
                    .HasForeignKey(e => e.GameId);
                entity.HasOne(e => e.Renter).WithMany(g => g.InventoryItems)
                    .HasForeignKey(e => e.RenterId);
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasKey(p => p.Id);
            });

            modelBuilder.Entity<PlayerGame>(entity =>
            {
                entity.HasKey(p => new { p.GameId, p.PlayerId});
                entity.HasOne(e => e.Game).WithMany(g => g.PlayerGames)
                    .HasForeignKey(e => e.GameId);
                entity.HasOne(e => e.Player).WithMany(g => g.PlayerGames)
                    .HasForeignKey(e => e.PlayerId);
            });

            modelBuilder.Entity<PlayerPlayingDates>(entity =>
            {
                entity.HasKey(p => new { p.Id});
                entity.HasOne(p => p.Player).WithMany(p => p.PlayingDates).HasForeignKey(p => p.PlayerId);
                entity.HasOne(p => p.Group).WithMany(p => p.PlayingDates).HasForeignKey(p => p.GroupId);

            });

            modelBuilder.Entity<Rental>(entity =>
            {
                //entity.HasKey(r => new { r.RenterId,r.PlayerId, r.GameId});
                entity.HasKey(r => new { r.Id });
                entity.HasOne(e => e.Player).WithMany(g => g.Rentals)
                    .HasForeignKey(e => e.PlayerId);
                entity.HasOne(e => e.InventoryItem).WithMany(g => g.Rentals)
                    .HasForeignKey(e => new { e.RenterId, e.GameId });

            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.HasKey(r => new { r.GroupId, r.ReviewedPlayerId, r.ReviewerId, r.TimeWritten });
                entity.HasOne(e => e.Reviewer).WithMany(g => g.WrittenReviews)
                    .HasForeignKey(e => e.ReviewerId);
                entity.HasOne(e => e.ReviewedPlayer).WithMany(g => g.ReceivedReviews)
                    .HasForeignKey(e => e.ReviewedPlayerId);

            });

            modelBuilder.Entity<GroupJoinRequest>(entity =>
            {
                entity.HasKey(r => new { r.Id });
                entity.HasOne(e => e.Group).WithMany(g => g.GroupJoinRequests)
                    .HasForeignKey(e => e.GroupId);
                entity.HasOne(e => e.Player).WithMany(g => g.GroupJoinRequests)
                    .HasForeignKey(e => e.PlayerId);
            });

            modelBuilder.Entity<GroupAdvert>(entity =>
            {
                entity.HasKey(r => new { r.Id });
                entity.HasOne(e => e.Group).WithOne(g => g.GroupAdvert)
                    .HasForeignKey<GroupAdvert>(e => e.GroupId);

            });

            modelBuilder.Entity<Message>().HasOne(m => m.Sender)
                .WithMany(u => u.SentMessages).HasForeignKey(u => u.SenderUserName).HasPrincipalKey(u => u.UserName);
            modelBuilder.Entity<Message>().HasOne(m => m.Receiver)
                .WithMany(u => u.ReceivedMessages).HasForeignKey(u => u.ReceiverUserName).HasPrincipalKey(u => u.UserName);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<GameEx.Models.Review> Review { get; set; }

        public DbSet<GameEx.Models.GameSuggestion> GameSuggestion { get; set; }

        public DbSet<GameEx.Models.GroupAdvert> GroupAdvert { get; set; }


    }
}
